package com.stox.core.repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.ui.model.Link;
import com.stox.util.Constant;
import com.stox.util.FileUtil;
import com.stox.util.StringUtil;

public class LinkStateRepository {
	private static final String PATH = Constant.HOME + "workbench" + File.separator + "state.link.csv";
	
	public void persistAll() throws IOException{
		final List<String> lines = Arrays.stream(Link.values()).map(link -> format(link.getState())).collect(Collectors.toList());
		Files.write(FileUtil.safeGet(PATH), lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
	}
	
	private String format(Link.State state){
		return null == state ? "" : state.getTo() + "," + state.getScrip().getIsin() + "," + (null == state.getBarSpan() ? "NULL" : state.getBarSpan().getShortName());
	}
	
	public void loadAll() throws IOException{
		final List<String> lines = Files.readAllLines(FileUtil.safeGet(PATH));
		for(int index = 0; index < lines.size(); index++){
			final String line = lines.get(index);
			final Link.State state = parse(line);
			if(null != state){
				final Link link = Link.values()[index];
				link.setState(state);
			}
		}
	}
	
	private Link.State parse(String line){
		if(StringUtil.hasText(line)){
			final String[] tokens = line.split(",");
			final long to = Long.parseLong(tokens[0]);
			final Scrip scrip = ScripRepository.getInstance().find(tokens[1]);
			final BarSpan barSpan = BarSpan.getByShortName(tokens[2]);
			return new Link.State(to, scrip, barSpan);
		}
		return null;
	}
	
}

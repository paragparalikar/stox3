package com.stox.data.downloader.scrip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;

public class NseScripMasterDownloader implements ScripMasterDownloader {
	private static final String URL_SCRIP_MASTER = "https://www.nseindia.com/content/equities/EQUITY_L.csv";

	@Override
	public List<Scrip> download(Exchange exchange) throws IOException {
		final InputStream stream = new URL(URL_SCRIP_MASTER).openStream();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		return reader.lines().map(line -> parse(line)).filter(scrip -> null != scrip).collect(Collectors.toList());
	}

	private Scrip parse(String line) {
		final String[] tokens = line.split(",");
		if ("SYMBOL".equals(tokens[0])) {
			return null;
		}
		final Scrip scrip = new Scrip();
		scrip.setExchange(Exchange.NSE);
		scrip.setCode(tokens[0]);
		scrip.setName(tokens[1]);
		scrip.setIsin(tokens[6]);
		return scrip;
	}
	

}

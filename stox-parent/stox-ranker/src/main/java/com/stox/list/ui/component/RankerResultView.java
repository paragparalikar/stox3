package com.stox.list.ui.component;

import java.util.Collections;
import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.repository.BarRepository;
import com.stox.core.ui.model.Link;
import com.stox.list.ranker.Ranker;
import com.stox.list.ranker.RankerResult;
import com.stox.list.ui.RankerModuleView;
import com.stox.list.ui.RankerState;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentProgressBar;
import com.stox.ui.fx.fluent.scene.control.FluentTableColumn;
import com.stox.ui.fx.fluent.scene.control.FluentTableView;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.util.Async;
import com.stox.util.Constant;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.sun.javafx.binding.ObjectConstant;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;

public class RankerResultView implements Runnable{
	
	private final RankerModuleView rankerModuleView;
	
	private final FluentTableColumn<RankerResult, String> nameColumn = new FluentTableColumn<RankerResult, String>("Instrument")
			.cellValueFactory(features -> ObjectConstant.valueOf(features.getValue().getScrip().getName()));
	private final FluentTableColumn<RankerResult, Double> rankColumn = new FluentTableColumn<RankerResult, Double>("Rank")
			.cellValueFactory(features -> ObjectConstant.valueOf(features.getValue().getRank()))
			.cellFactory(column -> rankColumnCell())
			.prefWidth(70).maxWidth(70).minWidth(70);
	private final FluentTableView<RankerResult> tableView = new FluentTableView<RankerResult>()
			.classes("primary")
			.columns(nameColumn, rankColumn)
			.columnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY)
			.onItemSelected((o, old, value) -> onTableItemSelected(value));
	private final FluentProgressBar progressBar = new FluentProgressBar().fullWidth().classes("success", "inverted");
	private final FluentButton previousButton = new FluentButton(Icon.LONG_ARROW_LEFT).classes("primary", "icon").onAction(e -> previous());
	private final FluentHBox buttonBar = new FluentHBox(previousButton, progressBar).classes("padded", "spaced").alignment(Pos.CENTER).fullWidth();

	public RankerResultView(RankerModuleView rankerModuleView) {
		this.rankerModuleView = rankerModuleView;
	}
	
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void run() {
		Ui.fx(() -> {
			progressBar.setProgress(0);
			tableView.getItems().clear();
			tableView.getSortOrder().setAll(Collections.singleton(rankColumn));
		});
		final RankerState state = rankerModuleView.getState();
		final Ranker ranker = state.getRanker();
		final int offset = state.getOffset();
		final BarSpan barSpan = state.getBarSpan();
		final Object config = state.getConfig();
		final int count = ranker.getMinBarCount(config) + offset;
		final BarRepository barRepository = BarRepository.getInstance();
		for(int index = 0; index < state.getScrips().size(); index++){
			final Scrip scrip = state.getScrips().get(index);
			if(state.isStopped()){
				break;
			}
			try{
				final List<Bar> bars = barRepository.find(scrip.getIsin(), barSpan, count);
				final RankerResult result = ranker.rank(scrip, bars, config);
				final double progress =  ((double) index) / ((double) state.getScrips().size() - 1) ;
				Ui.fx(() -> {
					tableView.getItems().add(result);
					progressBar.setProgress(Math.max(progressBar.getProgress(), progress));
				});
			}catch(Exception e){
				//ignored
			}
		}
	}
	
	private TableCell<RankerResult, Double> rankColumnCell() {
		return new TableCell<RankerResult, Double>() {
			@Override
			protected void updateItem(Double item, boolean empty) {
				super.updateItem(item, empty);
				setText(null == item ? null : Constant.currencyFormat.format(item));
			}
		};
	}

	private void onTableItemSelected(RankerResult item) {
		final Link link = rankerModuleView.getLinkButton().getLink();
		final Link.State state = new Link.State(0, item.getScrip(), rankerModuleView.getState().getBarSpan());
		link.setState(state);
	}
	
	public void start(){
		rankerModuleView.content(tableView).bottom(buttonBar);
		rankerModuleView.getState().setStopped(false);
		Async.execute(this);
	}
	
	private void stop(){
		rankerModuleView.getState().setStopped(true);
		rankerModuleView.content(null).bottom(null);
	}
	
	private void previous(){
		stop();
		final ScripSelectionView scripSelectionView = new ScripSelectionView(rankerModuleView);
		scripSelectionView.start();
	}
	
}

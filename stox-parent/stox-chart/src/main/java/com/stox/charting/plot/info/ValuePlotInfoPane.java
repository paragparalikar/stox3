package com.stox.charting.plot.info;


import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class ValuePlotInfoPane implements PlotInfoPane {

	private final PlotInfoPane delegate;
	private final Label valueLabel = new Label();

	public ValuePlotInfoPane(final PlotInfoPane delegate) {
		this.delegate = delegate;
		valueLabel.getStyleClass().add("value-label");
		delegate.getNode().getChildren().add(valueLabel);
	}

	public void setValue(final String text) {
		valueLabel.setText(text);
	}

	public void bind(final ObjectProperty<Color> colorProperty) {
		valueLabel.textFillProperty().bind(colorProperty);
		delegate.bind(colorProperty);
	}

	@Override
	public HBox getNode() {
		return delegate.getNode();
	}

	@Override
	public void setName(String name) {
		delegate.setName(name);
	}

	@Override
	public void addRemoveEventHandler(EventHandler<ActionEvent> eventHandler) {
		delegate.addRemoveEventHandler(eventHandler);
	}

	@Override
	public void addVisibilityEventHandler(EventHandler<ActionEvent> eventHandler) {
		delegate.addVisibilityEventHandler(eventHandler);
	}

}

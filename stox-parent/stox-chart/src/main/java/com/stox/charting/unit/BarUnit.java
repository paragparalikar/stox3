package com.stox.charting.unit;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.ui.fx.fluent.scene.layout.FluentRegion;
import com.stox.widget.Ui;
import com.stox.widget.parent.Parent;

import javafx.scene.Node;
import javafx.scene.layout.Region;

public class BarUnit implements Unit<Double> {

	private final Parent<Node> parent;
	private final Region region = new FluentRegion().classes("bar");

	public BarUnit(final Parent<Node> parent) {
		this.parent = parent;
	}

	@Override
	public void attach() {
		parent.add(region);
	}

	@Override
	public void detach() {
		parent.remove(region);
	}

	@Override
	public void update(final int index, final Double model, final Double previousModel, final XAxis xAxis,
			final YAxis yAxis) {
		final double y = Ui.px(yAxis.getY(model));
		region.resizeRelocate(xAxis.getX(index), y, xAxis.getUnitWidth(), yAxis.getHeight() - y);
	}

	public void setOpacity(double value) {
		region.setOpacity(value);
	}

}

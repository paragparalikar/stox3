package com.stox.screener.ui.component;

import com.stox.auto.AutoUiBuilder;
import com.stox.auto.AutoVBox;
import com.stox.core.model.BarSpan;
import com.stox.screener.ui.ScreenerModuleView;
import com.stox.screener.ui.ScreenerState;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.control.FluentTextField;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;

import javafx.geometry.Pos;

public class ScreenConfigView {
	
	private AutoVBox autoVBox;
	private final ScreenerModuleView screenerModuleView;
	
	private final FluentLabel barSpanLabel = new FluentLabel("Bar Span");
	private final FluentComboBox<BarSpan> barSpanComboBox = new FluentComboBox<BarSpan>().items(BarSpan.values()).classes("primary", "inverted", "auto-widget");
	private final FluentHBox barSpanContainer = new FluentHBox(barSpanLabel, new FluentHBox(barSpanComboBox).alignment(Pos.CENTER_RIGHT).fullWidth()).fullWidth().classes("spaced", "padded");
	
	private final FluentLabel spanLabel = new FluentLabel("Span");
	private final FluentTextField spanTextField = new FluentTextField().classes("primary", "inverted", "auto-widget");
	private final FluentHBox spanContainer = new FluentHBox(spanLabel, new FluentHBox(spanTextField).alignment(Pos.CENTER_RIGHT).fullWidth()).fullWidth().classes("spaced", "padded");
	
	private final FluentLabel offsetLabel = new FluentLabel("Offset");
	private final FluentTextField offsetTextField = new FluentTextField().classes("primary", "inverted", "auto-widget");
	private final FluentHBox offsetContainer = new FluentHBox(offsetLabel, new FluentHBox(offsetTextField).alignment(Pos.CENTER_RIGHT).fullWidth()).fullWidth().classes("spaced", "padded");
	
	private final FluentButton previousButton = new FluentButton(Icon.LONG_ARROW_LEFT).classes("primary", "icon").onAction(e -> previous());
	private final FluentButton nextButton = new FluentButton(Icon.LONG_ARROW_RIGHT).classes("primary", "icon").onAction(e -> next());
	private final FluentHBox buttonBar = new FluentHBox(previousButton, new Spacer(),nextButton).classes("padded");
	
	public ScreenConfigView(ScreenerModuleView rankerModuleView) {
		this.screenerModuleView = rankerModuleView;
	}
	
	public void start(){
		final ScreenerState state = screenerModuleView.getState();
		spanTextField.setText(String.valueOf(state.getSpan()));
		offsetTextField.setText(String.valueOf(state.getOffset()));
		
		barSpanComboBox.getSelectionModel().select(state.getBarSpan());
		final AutoUiBuilder builder = new AutoUiBuilder();
		final FluentVBox container = new FluentVBox(spanContainer, offsetContainer, barSpanContainer);
		if(null != state.getConfig()){
			autoVBox = builder.build(state.getConfig());
			autoVBox.populateView();
			container.getChildren().add(autoVBox);
		}
		screenerModuleView.content(container).bottom(buttonBar);
	}
	
	private void stop(){
		screenerModuleView.content(null).bottom(null);
	}
	
	private void next(){
		stop();
		if(null != autoVBox) autoVBox.populateModel();
		final ScreenerState state = screenerModuleView.getState();
		state.setBarSpan(barSpanComboBox.getValue());
		state.setSpan(Integer.parseInt(spanTextField.getText()));
		state.setOffset(Integer.parseInt(offsetTextField.getText()));
		
		final ScripSelectionView scripSelectionView = new ScripSelectionView(screenerModuleView);
		scripSelectionView.start();
	}
	
	private void previous(){
		stop();
		final ScreenSelectionView screenSelectionView = new ScreenSelectionView(screenerModuleView);
		screenSelectionView.start();
	}
	
}

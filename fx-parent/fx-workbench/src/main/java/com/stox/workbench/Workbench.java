package com.stox.workbench;

import java.io.File;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentPane;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;
import com.stox.util.Constant;
import com.stox.widget.decorator.GlassableDecorator;
import com.stox.widget.decorator.MovableDecorator;
import com.stox.widget.decorator.ResizableDecorator;
import com.stox.workbench.delegate.ShutdownDelegate;
import com.stox.workbench.delegate.StartupDelegate;
import com.stox.workbench.event.ModuleViewRegistration;
import com.stox.workbench.layout.DockingLayoutManager;
import com.stox.workbench.layout.LayoutManager;
import com.stox.workbench.modal.ConfirmationModal;
import com.stox.workbench.modal.Modal;
import com.stox.workbench.module.view.ModuleView;
import com.stox.workbench.module.view.ModuleViewFactory;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

@SuppressWarnings("unused")
public class Workbench {
	
	private final Stage stage;
	private final FluentPane contentPane = new FluentPane();
	private final WorkbenchTitleBar titleBar = new WorkbenchTitleBar().exitEventHandler(e -> exit());
	private final FluentBorderPane displayPane = new FluentBorderPane().top(titleBar.getNode()).center(contentPane);
	private final FluentStackPane glassableNode = new FluentStackPane(displayPane);
	private final FluentBorderPane resizableNode = new FluentBorderPane(glassableNode);
	
	private final MovableDecorator movableDecorator;
	private final ResizableDecorator resizableDecorator;
	private final GlassableDecorator glassableDecorator = new GlassableDecorator(glassableNode);
	private final ModuleViewFactory moduleViewFactory = new ModuleViewFactory();
	private final LayoutManager layoutManager = new DockingLayoutManager(titleBar.getMenuBar(), contentPane, moduleViewFactory);
	
	private final Set<WorkbenchInterceptor> interceptors = new HashSet<>();
	private final String stateFilePath = Constant.HOME + "workbench" + File.separator + "state.csv";
	
	
	public Workbench(Stage stage) {
		this.stage = stage;
		titleBar.bind(stage);
		stage.initStyle(StageStyle.UNDECORATED);
		movableDecorator = new MovableDecorator(titleBar.getNode(), stage);
		resizableDecorator = new ResizableDecorator(stage, resizableNode);
		final Scene scene = new Scene(resizableNode);
		Application.setUserAgentStylesheet("stylex/base.css");
		scene.getStylesheets().addAll( "stylex/label.css", "stylex/button.css", "stylex/menu.css", "stylex/scroll-bar.css", "stylex/table-view.css",
				"stylex/combo-box.css", "stylex/choice-box.css", "stylex/check-box.css", "stylex/text-field.css", "stylex/list-view.css", "stylex/progress-indicator.css", 
				"stylex/progress-bar.css", "stylex/tool-bar.css", "stylex/titled-pane.css", "stylex/search-box.css",
				"stylex/override.css");
		scene.getStylesheets().addAll("stylex/workbench.css", "stylex/modal.css");
		stage.setScene(scene);
		ServiceLoader.load(WorkbenchInterceptor.class).forEach(interceptors::add);
	}
		
	public void start() throws Exception{
		for(WorkbenchInterceptor interceptor : interceptors) interceptor.beforeStart(this);
		Event.register(this);
		layoutManager.start();
		stage.addEventHandler(WindowEvent.WINDOW_SHOWN, e -> {
			new StartupDelegate().startup(stateFilePath, stage, moduleViewFactory);
		});
		stage.show();
	}
	
	public void stop() throws Exception{
		Event.unregister(this);
		new ShutdownDelegate().shutdown(stateFilePath, stage, layoutManager);
		for(WorkbenchInterceptor interceptor : interceptors) interceptor.afterStop(this);
	}
	
	private void exit(){
		new ConfirmationModal(
				"Are you sure you want to exit the workbench?", 
				"Exit",
				() -> Platform.exit())
		.show();
	}
	
	public Stage getStage() {
		return stage;
	}
	
	public WorkbenchMenuBar getMenuBar(){
		return titleBar.getMenuBar();
	}
		
	public void register(ModuleViewRegistration registration){
		moduleViewFactory.register(registration);
	}
	
	@EventHandler(Modal.SHOW)
	public void show(Modal modal) {
		glassableDecorator.showGlass(modal.getNode());
		modal.onShowing();
	}

	@EventHandler(Modal.HIDE)
	public void hide(Modal modal) {
		glassableDecorator.hideGlass();
	}
	
	@EventHandler(ModuleView.SHOW)
	public void show(ModuleView view) {
		if(0 >= contentPane.getWidth() || 0 >= contentPane.getHeight()){
			Platform.runLater(() -> {
				layoutManager.show(view);
				view.start();
				view.getNode().toFront();
			});
		}else{
			layoutManager.show(view);
			view.start();
		}
	}

	@EventHandler(ModuleView.HIDE)
	public void hide(ModuleView view) {
		view.stop();
		layoutManager.hide(view);
	}
}

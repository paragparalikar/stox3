package com.stox.charting.drawing.text;

import com.stox.charting.ChartingView;
import com.stox.charting.drawing.AbstractDrawingToggleButton;
import com.stox.widget.Ui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class ChartTextToggleButton extends AbstractDrawingToggleButton
		implements ChangeListener<Boolean> {

	public ChartTextToggleButton(ChartingView chartingView) {
		super(chartingView);
		setText("A");
		classes("icon", "primary").text("A").tooltip(Ui.tooltip("Text"));
		selectedProperty().addListener(this);
	}

	@Override
	public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
		super.changed(observable, oldValue, newValue);
		final ChartingView chartingView = getChartingView();
		if (newValue && null != chartingView) {
			chartingView.setMouseModeHandler(new ChartTextModeMouseHandler(chartingView, () -> setSelected(false)));
		}
	}

}

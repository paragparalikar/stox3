package com.stox.workbench.layout;

import com.stox.event.Event;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.widget.decorator.SnappableDecorator;
import com.stox.workbench.event.ModuleViewRegistration;
import com.stox.workbench.module.view.ModuleView;
import com.stox.workbench.module.view.ModuleViewFactory;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

public class DockingLayoutManager extends AbstractLayoutManager {

	private final MenuBar menuBar;
	private final Pane contentPane;
	private SnappableDecorator snappableDecorator;
	private final Menu modulesMenu = new Menu("Modules");
	
	public DockingLayoutManager(MenuBar menuBar, Pane contentPane, ModuleViewFactory moduleViewFactory) {
		super(moduleViewFactory);
		this.menuBar = menuBar;
		this.contentPane = contentPane;
	}

	@Override
	public void start() {
		snappableDecorator = new SnappableDecorator(contentPane);
		menuBar.getMenus().add(modulesMenu);
		getModuleViewFactory().getRegistrations().stream()
		.sorted((one, two) -> one.getName().compareToIgnoreCase(two.getName()))
		.forEach(registration -> {
			final ModuleMenuItem moduleMenuItem = new ModuleMenuItem(registration.getIcon(),registration.getName()); 
			moduleMenuItem.setOnAction(e -> onMenuItemClicked(registration));
			modulesMenu.getItems().add(moduleMenuItem);
		});
	}

	private void onMenuItemClicked(ModuleViewRegistration registration){
		if(!registration.isMultipleAllowed() && isShowing(registration.getId())){
			final ModuleView moduleView = getShowingModuleView(registration.getId());
			moduleView.getNode().toFront();
		}else{
			final ModuleView moduleView = getModuleViewFactory().create(registration.getId());
			Event.fire(ModuleView.SHOW, moduleView);
		}
	}
	
	@Override
	public void stop() {
		menuBar.getMenus().remove(modulesMenu);
	}

	@Override
	public void show(ModuleView view) {
		super.show(view);
		snappableDecorator.add(view.getTitleBar().getNode(), view.getNode());
	}

	@Override
	public void hide(ModuleView view) {
		super.hide(view);
		snappableDecorator.remove(view.getNode());
	}

}

class ModuleMenuItem extends MenuItem {

	public ModuleMenuItem(String icon, String name) {
		super(name, new FluentLabel(icon).classes("icon", "primary"));
		getStyleClass().add("primary");
	}

}

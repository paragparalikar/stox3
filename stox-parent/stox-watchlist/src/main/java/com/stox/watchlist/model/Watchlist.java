package com.stox.watchlist.model;

import java.util.Comparator;
import java.util.Objects;

import lombok.NonNull;
import lombok.Value;

@Value
public class Watchlist implements Comparable<Watchlist>{
	public static final String CREATED = "WATCHLIST_CREATED";
	public static final String MODIFIED = "WATCHLIST_MODIFIED";
	public static final String CLEARED = "WATCHLIST_CLEARED";
	public static final String DELETED = "WATCHLIST_DELETED";
	public static final Comparator<Watchlist> COMPARATOR = (one, two) -> one.name.compareToIgnoreCase(two.name);  
	
	@NonNull
	private String id;
	
	@NonNull
	private String name;
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Watchlist other) {
		return Objects.compare(this, other,COMPARATOR);
	}

}

package com.stox.util;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Async {

	private static class FinxTreadFactory implements ThreadFactory, UncaughtExceptionHandler {

		private int count;
		private final String name;

		public FinxTreadFactory(final String namePrefix) {
			this.name = namePrefix;
		}

		@Override
		public Thread newThread(Runnable runnable) {
			final Thread thread = new Thread(runnable, "Stox-" + name + "-" + (++count));
			thread.setUncaughtExceptionHandler(this);
			return thread;
		}

		@Override
		public void uncaughtException(Thread t, Throwable e) {
			e.printStackTrace();
		}

	}

	private static final ExecutorService executorService = Executors.newWorkStealingPool();
	private static final ExecutorService syncExecutorService = Executors
			.newSingleThreadExecutor(new FinxTreadFactory("sync"));
	private static final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(0,
			new FinxTreadFactory("scheduled"));

	public synchronized static boolean isShutdown() {
		return executorService.isShutdown() || scheduledExecutorService.isShutdown()
				|| syncExecutorService.isShutdown();
	}

	public synchronized static void execute(final Runnable runnable) {
		if (!executorService.isShutdown()) {
			executorService.execute(runnable);
		}
	}

	public synchronized static void executeSync(final Runnable runnable) {
		if (!syncExecutorService.isShutdown() && !syncExecutorService.isTerminated()) {
			syncExecutorService.execute(runnable);
		}
	}

	public synchronized static <T> void execute(Callable<T> callable, Consumer<T> successCallback,
			Consumer<Throwable> failureCallback, Runnable finallyCallback) {
		if (!executorService.isShutdown() && !executorService.isTerminated()) {
			Objects.requireNonNull(callable, "callable can not be null");
			executorService.execute(() -> {
				try {
					final T result = callable.call();
					Optional.ofNullable(successCallback).ifPresent(consumer -> consumer.accept(result));
				} catch (Throwable throwable) {
					Optional.ofNullable(failureCallback).ifPresent(consumer -> consumer.accept(throwable));
				} finally {
					Optional.ofNullable(finallyCallback).ifPresent(runnable -> runnable.run());
				}
			});
		}
	}

	public synchronized static void shutdown() {
		executorService.shutdown();
		syncExecutorService.shutdown();
		scheduledExecutorService.shutdown();
		try {
			executorService.awaitTermination(1, TimeUnit.SECONDS);
			syncExecutorService.awaitTermination(1, TimeUnit.MILLISECONDS);
			scheduledExecutorService.awaitTermination(1, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized static ScheduledFuture<?> schedule(final Runnable task, long delay, long period) {
		return scheduledExecutorService.scheduleWithFixedDelay(task, delay, period, TimeUnit.MILLISECONDS);
	}

}

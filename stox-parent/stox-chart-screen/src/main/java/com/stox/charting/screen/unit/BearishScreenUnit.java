package com.stox.charting.screen.unit;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.screen.ScreenMatch;
import com.stox.core.model.Bar;
import com.stox.widget.Icon;

import javafx.scene.Group;
import javafx.scene.shape.Polygon;

public class BearishScreenUnit extends AbstractScreenUnit {

	public BearishScreenUnit(final Group parent) {
		super(Icon.ARROW_DOWN, "bearish", parent);
	}

	@Override
	public void update(int index, ScreenMatch model, ScreenMatch previousModel, XAxis xAxis, YAxis yAxis) {
		final Bar bar = model.getBar();
		final Polygon node = getNode();
		final double x = xAxis.getX(model.getIndex());
		final double width = Math.max(10, xAxis.getUnitWidth());
		final double height = 0.7 * width;
		final double y = yAxis.getY(bar.getHigh()) - 5 - height;
		node.getPoints().setAll(x, y, x + width, y, x + width / 2, y + height);
	}

}

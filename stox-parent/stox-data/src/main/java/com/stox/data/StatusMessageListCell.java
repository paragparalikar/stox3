package com.stox.data;

import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.widget.Icon;
import com.stox.widget.Ui;

import javafx.scene.control.ListCell;
import javafx.scene.control.ProgressIndicator;

public class StatusMessageListCell extends ListCell<StatusMessage> implements Runnable {

	private FluentLabel graphic;
	private StatusMessage previousItem;
	private static ProgressIndicator progressIndicator;

	@Override
	protected void updateItem(StatusMessage item, boolean empty) {
		super.updateItem(item, empty);
		if (null != previousItem) {
			previousItem.remove(this);
		}
		if (null == item || empty) {
			setText(null);
			setGraphic(null);
		} else {
			item.add(this);
			setText(item.getMessage());
			run();
		}
		previousItem = item;
	}

	@Override
	public void run() {
		Ui.fx(() -> {
			final Boolean success = getItem().isSuccess();
			if (null == success) {
				if (null == progressIndicator) {
					progressIndicator = new ProgressIndicator();
					progressIndicator.getStyleClass().addAll("tiny","primary","inverted", "transparent-background");
				}
				setGraphic(progressIndicator);
			} else {
				if (null == graphic) {
					graphic = new FluentLabel().classes("icon", "inverted", "transparent-background");
				}
				setGraphic(graphic);
				graphic.text(success ? Icon.CHECK : Icon.EXCLAMATION_TRIANGLE)
						.classes(success, "success")
						.classes(!success, "danger");
			}
		});
	}

}

package com.stox.watchlist.ui;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.util.Async;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.repository.WatchlistEntryRepository;
import com.stox.watchlist.repository.WatchlistRepository;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.workbench.modal.ConfirmationModal;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;

public class WatchlistBox extends FluentHBox implements Supplier<Watchlist>{
	
	private final FluentComboBox<Watchlist> comboBox = new FluentComboBox<Watchlist>().fullArea().classes("primary","inverted", "first");
	private final FluentButton createButton = new FluentButton(Icon.PLUS).onAction(e -> create()).tooltip(Ui.tooltip("Create")).classes("icon","primary","inverted","middle");
	private final FluentButton editButton = new FluentButton(Icon.EDIT).onAction(e -> edit()).disable(true).tooltip(Ui.tooltip("Edit")).classes("icon","primary","inverted","middle");
	private final FluentButton clearButton = new FluentButton(Icon.ERASER).onAction(e -> clear()).disable(true).tooltip(Ui.tooltip("Clear")).classes("icon","primary","inverted","middle");
	private final FluentButton deleteButton = new FluentButton(Icon.TRASH).onAction(e -> delete()).disable(true).tooltip(Ui.tooltip("Delete")).classes("icon","primary","inverted","last");
	
	private final WatchlistRepository watchlistRepository = WatchlistRepository.getInstance();
	
	public WatchlistBox() {
		children(comboBox, createButton, editButton, clearButton, deleteButton).fullArea().classes("box");
		comboBox.selectionModel().selectedIndexProperty().addListener((o,old,value) -> {
			editButton.disable(null == value);
			deleteButton.disable(null == value);
			clearButton.disable(null == value);
		});
	}
	
	public void start(){
		Event.register(this);
		disable(true);
		Async.execute(()->{
			final List<Watchlist> watchlists = watchlistRepository.find();
			Collections.sort(watchlists);
			Ui.fx(() -> {
				comboBox.getItems().setAll(watchlists);
				if(!watchlists.isEmpty()){
					comboBox.getSelectionModel().select(0);
				}
				disable(false);
			});
		});
	}
	
	public void stop(){
		Event.unregister(this);
	}
	
	@Override
	protected void finalize() throws Throwable {
		stop();
		super.finalize();
	}
	
	private void create(){
		final WatchlistEditorModal modal = new WatchlistEditorModal(null);
		modal.show();
	}
	
	private void clear(){
		final Watchlist watchlist = comboBox.getValue();
		final ConfirmationModal modal = new ConfirmationModal("Are you sure you want to clear watchlist \""+watchlist.getName()+"\"","Clear",() -> {
			final WatchlistEntryRepository entryRepository = WatchlistEntryRepository.getInstance();
			entryRepository.delete(watchlist.getId());
			Event.fire(Watchlist.CLEARED, watchlist);
		});
		modal.show();
	}
	
	private void edit(){
		final WatchlistEditorModal modal = new WatchlistEditorModal(comboBox.getValue());
		modal.show();
	}
	
	private void delete(){
		final Watchlist watchlist = comboBox.getValue();
		new ConfirmationModal(
				"Are you sure you want to delete watchlist \""+watchlist.getName()+"\"?", 
				"Delete",
				() -> {
					try{
						watchlistRepository.delete(watchlist);
						Event.fire(Watchlist.DELETED, watchlist);
						// TODO Toast here 
					}catch(Exception e){
						e.printStackTrace();
						// TODO Toast here 
					}
				}).show();
	}
	
	
	@EventHandler(Watchlist.CREATED)
	public void onWatchlistCreated(Watchlist watchlist){
		Ui.fx(() -> {
			comboBox.items().add(watchlist);
			FXCollections.sort(comboBox.items());
			comboBox.select(watchlist);
		});
	}
	
	@EventHandler(Watchlist.MODIFIED)
	public void onWatchlistModified(Watchlist watchlist){
		Ui.fx(() -> {
			final Iterator<Watchlist> iterator = comboBox.items().iterator();
			while(iterator.hasNext()){
				final Watchlist item = iterator.next();
				if(item.getId().equals(watchlist.getId())){
					iterator.remove();
					break;
				}
			}
			comboBox.items().add(watchlist);
			FXCollections.sort(comboBox.items());
			comboBox.setValue(watchlist);
		});
	}
	
	@EventHandler(Watchlist.DELETED)
	public void onWatchlistDeleted(Watchlist watchlist){
		Ui.fx(() -> {
			comboBox.items().remove(watchlist);
			if(!comboBox.items().isEmpty()){
				comboBox.getSelectionModel().select(0);
			}
		});
	}
			
	@Override
	public Watchlist get() {
		return comboBox.getValue();
	}
	
	public void addChangeListener(ChangeListener<? super Watchlist> listener){
		comboBox.getSelectionModel().selectedItemProperty().addListener(listener);
	}

}

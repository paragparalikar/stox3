package com.stox.charting.tools;

import com.stox.charting.ChartingView;

import javafx.scene.Node;

public interface ChartingToolBox {

	Node getNode();
	
	void attach(ChartingView chartingView);
	
}

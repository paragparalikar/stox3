package com.stox.charting.drawing;

import com.stox.charting.drawing.region.ChartRegion;
import com.stox.charting.drawing.segment.horizontal.HorizontalSegment;
import com.stox.charting.drawing.segment.trend.TrendSegment;
import com.stox.charting.drawing.segment.vertical.VerticalSegment;
import com.stox.charting.drawing.text.ChartText;

public class DrawingFactory {

	public Drawing build(final int id) {
		switch (id) {
		case TrendSegment.ID:
			return new TrendSegment();
		case HorizontalSegment.ID:
			return new HorizontalSegment();
		case VerticalSegment.ID:
			return new VerticalSegment();
		case ChartRegion.ID:
			return new ChartRegion();
		case ChartText.ID:
			return new ChartText();
		}
		return null;
	}

}

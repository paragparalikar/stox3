package com.stox.list.ranker;

import com.stox.core.model.Scrip;

import lombok.Value;

@Value
public class RankerResult {

	private Scrip scrip;
	
	private double rank;

}

package com.stox.screen;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.screen.TestScreen.Config;

import lombok.Getter;
import lombok.Setter;

public class TestScreen implements Screen<Config> {

	@Getter
	@Setter
	public static class Config{
		private int span = 5;
		private double multiple = 2;
	}

	@Override
	public String getCode() {
		return "test";
	}

	@Override
	public String getName() {
		return "Test";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan() + 2;
	}

	@Override
	public boolean isMatch(List<Bar> bars, Config config) {
		if(null != bars && bars.size() > config.getSpan() + 1){
			double sum = 0;
			for(int index = 1; index <= config.getSpan(); index++){
				final Bar bar = bars.get(index);
				final Bar previous = bars.get(index + 1);
				sum += Math.abs(bar.getHigh() - previous.getHigh());
			}
			final double average = sum / config.getSpan();
			final Bar bar = bars.get(0);
			final Bar previous = bars.get(1);
			final double delta = bar.getHigh() - previous.getHigh();
			return delta > average*config.getMultiple();
		}
		
		return false;
	}
	
}

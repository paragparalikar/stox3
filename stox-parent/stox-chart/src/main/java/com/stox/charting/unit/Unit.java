package com.stox.charting.unit;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.util.Attachable;

public interface Unit<T> extends Attachable {

	public abstract void update(final int index, final T model, final T previousModel, final XAxis xAxis,
			final YAxis yAxis);

}

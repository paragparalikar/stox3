package com.stox.data.downloader.scrip;

import java.util.Date;
import java.util.List;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;
import com.stox.core.repository.ScripRepository;
import com.stox.data.DownloadContext;
import com.stox.data.StatusMessage;
import com.stox.data.downloader.bar.eod.EodBarDownloadAction;
import com.stox.util.Action;
import com.stox.util.DateUtil;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ScripMasterDownloadAction implements Action {

	private StatusMessage message;
	private final DownloadContext context;
	private final ScripRepository scripRepository = ScripRepository.getInstance();

	@Override
	public boolean validate() {
		final Date date = scripRepository.getLastModifiedDate(context.getExchange());
		return date.before(DateUtil.trim(new Date())) && !context.isCancelled();
	}

	@Override
	public void before() {
		message = new StatusMessage("Download scrip master");
		context.getMessageCallback().accept(message);
	}

	@Override
	public void execute() throws Throwable {
		final Exchange exchange = context.getExchange();
		final ScripMasterDownloaderFactory factory = new ScripMasterDownloaderFactory();
		final ScripMasterDownloader downloader = factory.create(exchange);
		final List<Scrip> scrips = downloader.download(exchange);
		scripRepository.save(exchange, scrips);
	}

	@Override
	public void success() {
		message.setSuccess(true);
	}

	@Override
	public void failure(Throwable throwable) {
		message.setSuccess(false);
	}

	@Override
	public void after() {
		context.getAfterCallback().run();
		if (context.isCancelled()) {
			context.getTerminationCallback().run();
		}else{
			final EodBarDownloadAction action = new EodBarDownloadAction(context);
			context.getExecutorService().submit(action);
		}
	}

}

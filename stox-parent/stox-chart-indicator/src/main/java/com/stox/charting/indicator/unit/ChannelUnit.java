package com.stox.charting.indicator.unit;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.unit.Unit;
import com.stox.indicator.Channel;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ChannelUnit implements Unit<Channel>{

	private final Parent<Point2D> parent;

	@Override
	public void attach() {
		
	}

	@Override
	public void detach() {
		
	}

	@Override
	public void update(int index, Channel model, Channel previousModel, XAxis xAxis, YAxis yAxis) {
		final double x = xAxis.getX(index) + xAxis.getUnitWidth();
		final double upper = yAxis.getY(model.getUpper());
		final double middle = yAxis.getY(model.getMiddle());
		final double lower = yAxis.getY(model.getLower());
		parent.addAll(new Point2D(x, upper), new Point2D(x, middle), new Point2D(x, lower));
	}
	
}

package com.stox.workbench.modal;

import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;
import com.stox.widget.Ui;

import javafx.event.Event;
import javafx.event.EventType;

public class ModalTitleBar extends FluentHBox {
	
	public static class ModalCloseRequestedEvent extends Event {
		private static final long serialVersionUID = -6481289093163602968L;
		public static final EventType<ModalCloseRequestedEvent> TYPE = new EventType<>("ModalTitleBar.CloseRequestedEvent");
		public ModalCloseRequestedEvent() {
			super(TYPE);
		}
	}

	private final FluentLabel titleLabel = new FluentLabel().classes("title-text");
	private final FluentButton closeButton = new FluentButton(Icon.TIMES)
			.classes("icon", "hover-danger")
			.tooltip(Ui.tooltip("Close"))
			.onAction(e -> onClose());
	private final FluentHBox left = new FluentHBox(titleLabel);
	private final FluentHBox right = new FluentHBox(closeButton);
	
	public ModalTitleBar() {
		classes("primary-background", "title-bar").children(left, new Spacer(), right).fullWidth();
	}
	
	protected void onClose(){
		fireEvent(new ModalCloseRequestedEvent());
	}

	public FluentLabel getTitleLabel() {
		return titleLabel;
	}

	protected FluentButton getCloseButton() {
		return closeButton;
	}

	protected FluentHBox getLeft() {
		return left;
	}

	protected FluentHBox getRight() {
		return right;
	}
	
	
	
}

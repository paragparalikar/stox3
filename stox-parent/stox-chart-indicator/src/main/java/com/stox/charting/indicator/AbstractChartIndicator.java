package com.stox.charting.indicator;

import java.util.List;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.UnitParent;

public abstract class AbstractChartIndicator<T, V, P> implements ChartIndicator<T, V, P> {

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis, List<V> models, List<Unit<V>> units, UnitParent<P> parent,
			IndicatorPlot<T, V, P> plot) {
		plot.doLayoutChartChildren(xAxis, yAxis);
	}
	
	@Override
	public String toString() {
		return getName();
	}

}

package com.stox.list.ui.component;


import com.stox.list.ranker.Ranker;
import com.stox.list.ranker.RankerFactory;
import com.stox.list.ui.RankerModuleView;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;

import javafx.geometry.Pos;

public class RankerSelectionView {
	
	private final RankerModuleView rankerModuleView;
	private final FluentListView<Ranker<?>> listView = new FluentListView<Ranker<?>>();
	private final FluentButton nextButton = new FluentButton(Icon.LONG_ARROW_RIGHT).classes("primary", "icon").onAction(e -> next());
	private final FluentHBox buttonBar = new FluentHBox(nextButton).classes("padded").alignment(Pos.CENTER_RIGHT).fullWidth();
	
	public RankerSelectionView(RankerModuleView rankerModuleView) {
		this.rankerModuleView = rankerModuleView;
		nextButton.disableProperty().bind(listView.getSelectionModel().selectedItemProperty().isNull());
	}
	
	public void start(){
		final RankerFactory rankerFactory = RankerFactory.instance();
		listView.items(rankerFactory.getAll());
		rankerModuleView.content(listView).bottom(buttonBar);
	}
	
	private void next(){
		final Ranker<?> ranker = listView.getSelectionModel().getSelectedItem();
		rankerModuleView.getState().setRanker(ranker);
		rankerModuleView.getState().setConfig(ranker.buildDefaultConfig());
		rankerModuleView.content(null).bottom(null);
		
		final RankerConfigView rankerConfigView = new RankerConfigView(rankerModuleView);
		rankerConfigView.start();
	}
}

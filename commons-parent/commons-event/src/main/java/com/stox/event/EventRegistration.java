package com.stox.event;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;

public final class EventRegistration {

	private final int hashCode;
	private final Method method;
	private final WeakReference<Object> targetReference;

	public EventRegistration(Object target, Method method) {
		super();
		this.method = method;
		this.targetReference = new WeakReference<>(target);
		this.hashCode = 31 * method.hashCode() * target.hashCode();
	}

	public void invoke(final Object payload) {
		try {
			final Object target = targetReference.get();
			if(null != target){
				switch (method.getParameterCount()) {
				case 0:
					method.invoke(target);
					break;
				case 1:
					if (method.getParameterTypes()[0].isAssignableFrom(payload.getClass())) {
						method.invoke(target, payload);
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Object getTarget() {
		return targetReference.get();
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventRegistration other = (EventRegistration) obj;
		return method.equals(other.method) && hashCode == other.hashCode;
	}

}

package com.stox.list.ui;

import com.stox.core.ui.LinkButton;
import com.stox.list.ui.component.RankerSelectionView;
import com.stox.widget.Icon;
import com.stox.workbench.module.view.ModuleView;

import javafx.geometry.Side;
import javafx.scene.Node;

public class RankerModuleView extends ModuleView {
	public static final String ID = "ranker";
	public static final String ICON = Icon.SORT_NUMERIC_DESC;
	
	private final RankerState state = new RankerState();
	private final LinkButton linkButton = new LinkButton();
	
	public RankerModuleView() {
		getTitleBar().add(Side.RIGHT, linkButton);
	}

	@Override
	public void start() {
		super.start();
		final RankerSelectionView rankerSelectionView = new RankerSelectionView(this);
		rankerSelectionView.start();
	}
	
	@Override
	public void stop() {
		state.setStopped(true);
		super.stop();
	}

	@Override
	public String getId() {
		return ID;
	}

	public RankerState getState() {
		return state;
	}
	
	public LinkButton getLinkButton() {
		return linkButton;
	}
	
	@Override
	public RankerModuleView content(Node node) {
		super.content(node);
		return this;
	}
	
	@Override
	public RankerModuleView bottom(Node node) {
		super.bottom(node);
		return this;
	}
	
}

package com.stox.workbench.layout;

import java.util.Collection;

import com.stox.workbench.module.view.ModuleView;

public interface LayoutManager {
	
	ModuleView show(String id);

	void show(ModuleView view);
	
	boolean isShowing(String id);

	ModuleView getShowingModuleView(String id);
	
	void hide(ModuleView view);

	Collection<ModuleView> getModuleViews();

	void start();

	void stop();

}

package com.stox.workbench.modal;

import com.stox.event.Event;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;
import com.stox.widget.decorator.GlassableDecorator;
import com.stox.widget.decorator.MovableDecorator;
import com.stox.widget.decorator.ResizableDecorator;
import com.stox.workbench.modal.ModalTitleBar.ModalCloseRequestedEvent;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Screen;

@SuppressWarnings("unused")
public class Modal{
	public static final String SHOW = "MODAL_SHOW";
	public static final String HIDE = "MODAL_HIDE";

	private final ModalTitleBar titleBar = new ModalTitleBar();
	private final FluentBorderPane container = new FluentBorderPane().top(titleBar);
	private final FluentStackPane stackPane = new FluentStackPane(container);
	private final GlassableDecorator glassableDecorator = new GlassableDecorator(stackPane);
	private final ProgressIndicator progressIndicator = new ProgressIndicator();
	private final FluentBorderPane wrapper = new FluentBorderPane(stackPane).classes("modal").managed(false);
	private final ResizableDecorator resizableDecorator = new ResizableDecorator(wrapper);
	private final MovableDecorator movableDecorator = new MovableDecorator(titleBar, wrapper);

	public Modal() {
		this(null);
	}
	
	public Modal(String text){
		this(text, null);
	}
	
	public Modal(final String title, final Node content) {
		container.setCenter(content);
		titleBar.getTitleLabel().setText(title);
		titleBar.addEventHandler(ModalCloseRequestedEvent.TYPE, e -> hide());
	}
		
	public Node getNode() {
		return wrapper;
	}

	public void show() {
		Event.fire(SHOW, this);
	}
	
	protected void showGlass(){
		glassableDecorator.showGlass(progressIndicator);
	}

	public void onShowing() {
		resizeRelocate();
	}

	protected void resizeRelocate() {
		final Rectangle2D rectangle = Screen.getPrimary().getVisualBounds();
		final double height = rectangle.getHeight() / 2;
		final double width = rectangle.getWidth() / 2;
		wrapper.resizeRelocate(width / 2, height / 2, width, height);
	}

	public void hide() {
		Event.fire(HIDE, this);
	}
	
	public void hideGlass(){
		glassableDecorator.hideGlass();
	}

	public ModalTitleBar getTitleBar() {
		return titleBar;
	}

	public FluentBorderPane getContainer() {
		return container;
	}

}

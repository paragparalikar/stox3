package com.stox.charting.screen;

import java.util.Comparator;
import java.util.Objects;

import com.stox.core.model.Bar;

import lombok.NonNull;
import lombok.Value;

@Value
public class ScreenMatch implements Comparable<ScreenMatch> {
	private static final Comparator<ScreenMatch> COMPARATOR = (one, two) -> one.getIndex().compareTo(two.getIndex());

	@NonNull
	private Bar bar;

	@NonNull
	private Integer index;

	@Override
	public int compareTo(ScreenMatch other) {
		return Objects.compare(this, other, COMPARATOR);
	}

}

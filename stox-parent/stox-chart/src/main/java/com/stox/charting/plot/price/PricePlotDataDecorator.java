package com.stox.charting.plot.price;

import java.util.List;
import java.util.Objects;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.event.DataChangedEvent;
import com.stox.charting.event.DataLoadingEvent;
import com.stox.charting.event.DataRequestEvent;
import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.repository.BarRepository;
import com.stox.util.Async;
import com.stox.widget.Ui;

public class PricePlotDataDecorator{

	private volatile long to;
	private volatile Scrip scrip;
	private volatile BarSpan barSpan = BarSpan.D;
	private final PricePlot pricePlot;
	private volatile boolean loading, fullyLoaded;

	public PricePlotDataDecorator(PricePlot pricePlot) {
		this.pricePlot = pricePlot;
		pricePlot.addEventHandler(DataRequestEvent.TYPE, this::onDataRequested);
	}
	
	private boolean equals(Scrip scrip, final BarSpan barSpan, final long to){
		return Objects.equals(scrip, pricePlot.getScrip()) && Objects.equals(barSpan, this.barSpan) && to == this.to;
	}
	
	private void reset(){
		loading = false;
		fullyLoaded = false;
		pricePlot.getModels().clear();
	}
	
	private void onDataRequested(DataRequestEvent event){
		if(!equals(scrip, event.getBarSpan(), event.getTo())){
			reset();
			this.scrip = pricePlot.getScrip();
			this.barSpan = null == event.getBarSpan() ? barSpan : event.getBarSpan();
			this.to = event.getTo();
		}
		load(scrip, barSpan, to, event.getXAxis());
	}
	
	private void load(Scrip scrip, BarSpan barSpan, long to, XAxis xAxis){
		if(shouldLoad(xAxis.getEndIndex()) && equals(scrip, barSpan, to)){
			loading = true;
			Ui.fx(() -> pricePlot.fireEvent(new DataLoadingEvent(loading)));
			Async.execute(() -> {
				try{
					final List<Bar> bars = load(scrip, barSpan, to);
					if(equals(scrip, barSpan, to)){
						fullyLoaded = bars.isEmpty();
						if(!fullyLoaded){
							pricePlot.getModels().addAll(bars);
							pricePlot.fireEvent(new DataChangedEvent(pricePlot.getModels()));
						}
					}
				}catch(Exception e){
					e.printStackTrace();
					if(equals(scrip, barSpan, to)) fullyLoaded = true;
					// TODO Toast or error message here
				}finally{
					if(equals(scrip, barSpan, to)){
						loading = false;
						Ui.fx(() -> pricePlot.fireEvent(new DataLoadingEvent(loading)));
						load(scrip, barSpan, to, xAxis);
					}
				}
			});
		}
	}
	
	private List<Bar> load(Scrip scrip, final BarSpan barSpan, final long to){
		final List<Bar> models = pricePlot.getModels();
		final long effectiveTo = models.isEmpty() ? (0 >= to ? System.currentTimeMillis() : to) : models.get(models.size() - 1).getDate() - barSpan.getMillis();
		final long effectiveFrom = effectiveTo - 400 * barSpan.getMillis();
		return BarRepository.getInstance().find(scrip.getIsin(), barSpan, effectiveFrom, effectiveTo);
	}
	
	private boolean shouldLoad(int endIndex){
		return 	null != pricePlot.getScrip() && 
				null != barSpan && 
				!loading && 
				!fullyLoaded && 
				endIndex > pricePlot.getModels().size();
	}

}

package com.stox.charting.plot.info;


import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;

import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class SimplePlotInfoPane implements PlotInfoPane {

	private final Label nameLabel = new Label();
	private final Button removeButton = new FluentButton(Icon.TIMES).classes("icon", "primary", "inverted");
	private final Button visibilityButton = new FluentButton(Icon.EYE).classes("icon", "primary", "inverted");
	private final HBox container = new FluentHBox(removeButton, visibilityButton, nameLabel).classes("plot-info-pane");

	@Override
	public HBox getNode() {
		return container;
	}

	@Override
	public void setName(String name) {
		nameLabel.setText(name);
	}

	@Override
	public void bind(ObjectProperty<Color> colorProperty) {
		nameLabel.textFillProperty().bind(colorProperty);
	}

	@Override
	public void addRemoveEventHandler(EventHandler<ActionEvent> eventHandler) {
		removeButton.addEventHandler(ActionEvent.ACTION, eventHandler);
	}

	@Override
	public void addVisibilityEventHandler(EventHandler<ActionEvent> eventHandler) {
		visibilityButton.addEventHandler(ActionEvent.ACTION, eventHandler);
	}

}

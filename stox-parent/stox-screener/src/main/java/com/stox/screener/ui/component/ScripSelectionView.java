package com.stox.screener.ui.component;

import java.util.List;

import com.stox.core.model.Scrip;
import com.stox.core.ui.filter.scrip.ScripFilter;
import com.stox.screener.ui.ScreenerModuleView;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;

public class ScripSelectionView {

	private final ScreenerModuleView screenerModuleView;
	private final ScripFilter scripFilter = new ScripFilter();
	private final FluentButton previousButton = new FluentButton(Icon.LONG_ARROW_LEFT).classes("primary", "icon").onAction(e -> previous());
	private final FluentButton nextButton = new FluentButton(Icon.LONG_ARROW_RIGHT).classes("primary", "icon").onAction(e -> next());
	private final FluentHBox buttonBar = new FluentHBox(previousButton, new Spacer(),nextButton).classes("padded");
	
	public ScripSelectionView(ScreenerModuleView screenerModuleView) {
		this.screenerModuleView = screenerModuleView;
	}
	
	public void start(){
		screenerModuleView.content(scripFilter).bottom(buttonBar);
	}
	
	private void stop(){
		screenerModuleView.content(null).bottom(null);
	}
	
	private void next(){
		stop();
		final List<Scrip> scrips = screenerModuleView.getState().getScrips(); 
		scrips.clear();
		scrips.addAll(scripFilter.get());
		
		final ScreenerResultView rankerResultView = new ScreenerResultView(screenerModuleView);
		rankerResultView.start();
	}
	
	private void previous(){
		stop();
		final ScreenConfigView rankerConfigView = new ScreenConfigView(screenerModuleView);
		rankerConfigView.start();
	}
	
}

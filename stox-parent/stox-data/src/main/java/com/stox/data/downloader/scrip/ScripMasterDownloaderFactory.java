package com.stox.data.downloader.scrip;

import com.stox.core.model.Exchange;

public class ScripMasterDownloaderFactory {

	public ScripMasterDownloader create(Exchange exchange) {
		switch (exchange) {
		case NSE:
			return new NseScripMasterDownloader();
		default:
			throw new UnsupportedOperationException("Exchange \"" + exchange.getName() + "\" is not suppoerted yet");
		}
	}

}

package com.stox.widget.decorator;

import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentPane;
import com.stox.widget.Ui;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class GlassableDecorator {

	private FluentBorderPane node;
	private Pane glass;
	private final StackPane target;
	

	public GlassableDecorator(final StackPane target) {
		this.target = target;
	}

	public synchronized boolean isGlassShowing() {
		return null != glass;
	}

	public synchronized void showGlass(Node node) {
		this.node = new FluentBorderPane(node);
		glass = new FluentPane().classes("glass");
		target.getChildren().addAll(glass, this.node);
		Ui.fade(this.node, 0, 1, null);
	}

	public synchronized void hideGlass() {
		if (null != node) {
			Ui.fade(node, 1, 0, () -> {
				if(null != node){
					node.getChildren().clear();
				}
				target.getChildren().removeAll(glass, node);
				glass = null;
				node = null;
			});
		}
	}

}

package com.stox.watchlist.ui;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.ui.LinkButton;
import com.stox.core.ui.model.Link;
import com.stox.core.ui.model.Link.State;
import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentToggleButton;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.util.Async;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.model.WatchlistEntry;
import com.stox.watchlist.repository.WatchlistEntryRepository;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.widget.decorator.GlassableDecorator;
import com.stox.widget.search.SearchBox;
import com.stox.widget.search.SearchableListView;
import com.stox.workbench.module.view.ModuleView;

import javafx.collections.FXCollections;
import javafx.geometry.Side;
import javafx.scene.control.ProgressIndicator;

public class WatchlistView extends ModuleView {
	public static final String ID = "watchlist";

	private final LinkButton linkButton = new LinkButton();
	
	private final FluentToggleButton filterButton = new FluentToggleButton(Icon.FILTER).classes("primary","icon").onAction(e -> toggleFilter()).tooltip(Ui.tooltip("Filter"));
	private final WatchlistBox watchlistBox = new WatchlistBox();
	private final FluentComboBox<BarSpan> barSpanComboBox = new FluentComboBox<BarSpan>().items(BarSpan.values()).onSelect((o,old,value) -> refresh()).fullArea().classes("primary","inverted");
	private final FluentVBox filterContainer = new FluentVBox(watchlistBox, barSpanComboBox).classes("primary-background","spaced");
	
	private final SearchableListView<WatchlistEntry> listView = new SearchableListView<>();
	private final FluentStackPane root = new FluentStackPane(listView);
	private final GlassableDecorator glassableDecorator = new GlassableDecorator(root);
	private final ProgressIndicator progressIndicator = new ProgressIndicator();
	private final SearchBox<WatchlistEntry> searchBox = new SearchBox<WatchlistEntry>(listView, this::test);
	private final FluentToggleButton searchButton = new FluentToggleButton(Icon.SEARCH).classes("primary","icon").onAction(e -> toggleSearch());
	
	private volatile BarSpan currentBarSpan;
	private volatile Watchlist currentWatchlist;
	private final WatchlistEntryRepository watchlistEntryRepository = WatchlistEntryRepository.getInstance();
	
	public WatchlistView() {
		content(listView);
		searchBox.classes("primary").fullWidth();
		getTitleBar().add(Side.RIGHT, filterButton);
		getTitleBar().add(Side.RIGHT, searchButton);
		getTitleBar().add(Side.RIGHT, linkButton);
		watchlistBox.addChangeListener((o,old,watchlist) -> refresh());
		
		listView.setCellFactory(param -> new WatchlistEntryListCell(watchlistBox));
		listView.getSelectionModel().selectedItemProperty().addListener((o, old, entry) -> updateLinkState(entry));
		progressIndicator.getStyleClass().addAll("primary","inverted","transparent-background", "large");
	}
	
	private boolean test(WatchlistEntry entry, String text){
		final Scrip scrip = entry.getScrip();
		text = text.trim().toLowerCase();
		return null != scrip && (
				scrip.getName().trim().toLowerCase().contains(text) || 
				scrip.getCode().trim().toLowerCase().contains(text) ||
				scrip.getIsin().trim().toLowerCase().contains(text));
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public void start() {
		super.start();
		watchlistBox.start();
		barSpanComboBox.select(BarSpan.D);
		Event.register(this);
	}
	
	@Override
	public void stop() {
		Event.unregister(this);
		watchlistBox.stop();
		super.stop();
	}
	
	@EventHandler(WatchlistEntry.CREATED)
	public void onWatchlistEntryCreated(WatchlistEntry entry){
		if(null != currentWatchlist 
				&& null != currentBarSpan
				&& currentWatchlist.getId().equals(entry.getWatchlistId())
				&& currentBarSpan.equals(entry.getBarSpan())){
			Ui.fx(() -> {
				listView.getItems().add(entry);
				FXCollections.sort(listView.getItems());
			});
		}
	}
	
	@EventHandler(Watchlist.CLEARED)
	public void onWatchlistCleared(Watchlist watchlist){
		if(null != currentWatchlist 
				&& currentWatchlist.equals(watchlist)){
			Ui.fx(() -> {
				listView.getItems().clear();
			});
		}
	}
	
	@EventHandler(WatchlistEntry.DELETED)
	public void onWatchlistEntryDeleted(WatchlistEntry entry){
		Ui.fx(() -> {
			listView.getItems().remove(entry);
		});
	}
	
	private void updateLinkState(WatchlistEntry entry){
		if(null != entry){
			final Link link = linkButton.getLink();
			link.setState(new State(0, entry.getScrip(), entry.getBarSpan()));
		}
	}
	
	private void toggleFilter(){
		getTitleBar().add(filterButton.isSelected(), Side.BOTTOM, filterContainer);
	}
	
	private void toggleSearch(){
		getTitleBar().add(searchButton.isSelected(), Side.BOTTOM, searchBox);
		if(searchButton.isSelected()){
			searchBox.getTextField().requestFocus();
		}
	}
	
	private void refresh(){
		final Watchlist watchlist = watchlistBox.get();
		final BarSpan barSpan = barSpanComboBox.getSelectionModel().getSelectedItem();
		if(null != watchlist && null != barSpan && (!Objects.equals(currentWatchlist, watchlist) || !Objects.equals(currentBarSpan, barSpan))){
			filterContainer.disable(true);
			glassableDecorator.showGlass(progressIndicator);
			Async.execute(() -> {
				final List<WatchlistEntry> entries = watchlistEntryRepository.find(watchlist.getId(), barSpan);
				Collections.sort(entries);
				Ui.fx(() -> {
					listView.getItems().setAll(entries);
					currentWatchlist = watchlist;
					currentBarSpan = barSpan;
					filterContainer.disable(false);
					glassableDecorator.hideGlass();
				});
			});
		}
	}

}

package com.stox.watchlist.ui;

import java.util.UUID;

import com.stox.event.Event;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.util.StringUtil;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.repository.WatchlistRepository;
import com.stox.widget.Icon;
import com.stox.widget.form.TextFormField;
import com.stox.workbench.modal.Modal;

import javafx.scene.Scene;

public class WatchlistEditorModal extends Modal {

	private final Watchlist watchlist;
	private final TextFormField nameFormField = new TextFormField("Name");
	private final FluentVBox content = new FluentVBox(nameFormField).classes("padded");
	private final FluentButton cancelButton = new FluentButton("Cancel").cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton().classes("success").defaultButton(true).onAction(e -> action());
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");
	private final FluentLabel graphic = new FluentLabel(Icon.BOOKMARK).classes("icon", "primary");
	
	private final WatchlistRepository watchlistRepository = WatchlistRepository.getInstance();

	public WatchlistEditorModal(Watchlist watchlist) {
		this.watchlist = watchlist;
		getContainer().center(content).bottom(buttonBar);
		getTitleBar().getTitleLabel().graphic(graphic);
		nameFormField.getTextField().getStyleClass().add("bordered");
	}
	
	@Override
	public void show() {
		populate();
		super.show();
		nameFormField.getTextField().requestFocus();
	}
	
	@Override
	protected void resizeRelocate() {
		final double height = 160;
		final double width = 500;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}
	
	private void populate(){
		if(null == watchlist){
			getTitleBar().getTitleLabel().setText("Create Watchlist");
			nameFormField.getTextField().clear();
			actionButton.setText("Create");
		}else{
			getTitleBar().getTitleLabel().setText("Edit Watchlist - " + watchlist.getName());
			nameFormField.getTextField().setText(watchlist.getName());
			actionButton.setText("Edit");
		}
	}

	private void action() {
		final String name = nameFormField.getTextField().getText();
		if(StringUtil.hasText(name)){
			try{
				final String id = null == this.watchlist ? UUID.randomUUID().toString() : watchlist.getId();
				final Watchlist watchlist = watchlistRepository.save(new Watchlist(id, name));
				hide();
				Event.fire(null == this.watchlist ? Watchlist.CREATED : Watchlist.MODIFIED, watchlist);
			}catch(Exception e){
				nameFormField.setError(e.getMessage());
			}
		}else{
			nameFormField.setError("Please provide a name for watchlist");
		}
	}
	
	private void cancel() {
		hide();
	}

}

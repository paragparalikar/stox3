package com.stox.example.ui.menu;

import java.util.Date;
import java.util.Iterator;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.HasBarSpan;
import com.stox.core.model.intf.HasDate;
import com.stox.core.model.intf.HasScrip;
import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.example.model.Example;
import com.stox.example.model.ExampleGroup;
import com.stox.example.repository.ExampleGroupRepository;
import com.stox.example.repository.ExampleRepository;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.widget.Icon;

import javafx.collections.FXCollections;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

public class SaveAsExampleMenu extends Menu {
	
	private final Object target;
	private final ExampleGroupRepository exampleGroupRepository = ExampleGroupRepository.getInstance();
	private final ExampleRepository exampleRepository = ExampleRepository.getInstance();
	
	public SaveAsExampleMenu(final Object target) {
		super("Save as Example", new FluentLabel(Icon.BOOKMARK).classes("icon", "primary", "inverted", "transparent-background"));
		this.target = target;
		exampleGroupRepository.find().forEach(group -> {
			final MenuItem item = new MenuItem(group.getName());
			getItems().add(item);
			item.setUserData(group);
			item.setOnAction(e -> add(group));
		});
		FXCollections.sort(getItems(), (one, two) -> one.getText().compareToIgnoreCase(two.getText()));
		Event.register(this);
	}
	
	@Override
	protected void finalize() throws Throwable {
		Event.unregister(this);
		super.finalize();
	}
	
	@EventHandler(ExampleGroup.CREATED)
	public void onExampleGroupCreated(ExampleGroup group){
		final MenuItem item = new MenuItem(group.getName());
		getItems().add(item);
		item.setUserData(group);
		item.setOnAction(e -> add(group));
		FXCollections.sort(getItems(), (one, two) -> one.getText().compareToIgnoreCase(two.getText()));
	}
	
	@EventHandler(ExampleGroup.MODIFIED)
	public void onExampleGroupModified(ExampleGroup group){
		for(MenuItem item : getItems()){
			final ExampleGroup value = (ExampleGroup)item.getUserData();
			if(value.getId().equals(group.getId())){
				item.setUserData(group);
				item.setText(group.getName());
			}
		}
	}
	
	@EventHandler(ExampleGroup.DELETED)
	public void onExampleGroupDeleted(ExampleGroup group){
		final Iterator<MenuItem> iterator = getItems().iterator();
		while(iterator.hasNext()){
			final MenuItem item = iterator.next();
			final ExampleGroup value = (ExampleGroup)item.getUserData();
			if(value.getId().equals(group.getId())){
				iterator.remove();
				break;
			}
		}
	}
	
	private void add(ExampleGroup group){
		final Scrip scrip = ((HasScrip)target).getScrip();
		final BarSpan barSpan = ((HasBarSpan)target).getBarSpan();
		final Date date = ((HasDate)target).getDate();
		final Example entry = new Example(group.getId(), scrip, barSpan, date.getTime());
		final Example savedEntry = exampleRepository.save(group.getId(), entry);
		if(null != savedEntry){
			Event.fire(Example.CREATED, entry);
		}
	}

}

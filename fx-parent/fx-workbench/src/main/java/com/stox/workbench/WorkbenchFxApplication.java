package com.stox.workbench;

import com.stox.util.Async;
import com.stox.widget.Icon;
import com.sun.javafx.application.LauncherImpl;

import javafx.application.Application;
import javafx.application.Preloader.PreloaderNotification;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class WorkbenchFxApplication extends Application {

	private Workbench workbench;

	@Override
	public void init() throws Exception {
		super.init();
		Font.loadFont(Icon.class.getClassLoader().getResource(Icon.PATH).toExternalForm(), 10);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.addEventHandler(WindowEvent.WINDOW_SHOWN, e -> notifyPreloader(new PreloaderNotification() {}));
		workbench = new Workbench(stage);
		workbench.start();
	}

	@Override
	public void stop() throws Exception {
		workbench.stop();
		super.stop();
		Async.shutdown();
	}

	public static void main(String[] args) {
		LauncherImpl.launchApplication(WorkbenchFxApplication.class, WorkbenchPreloader.class, args);
	}

}

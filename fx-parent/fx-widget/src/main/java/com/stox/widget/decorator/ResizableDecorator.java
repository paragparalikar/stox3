package com.stox.widget.decorator;

import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentRegion;
import com.stox.widget.area.Area;
import com.stox.widget.area.RegionAreaAdapter;
import com.stox.widget.area.StageAreaAdapter;

import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ResizableDecorator {

	protected enum ResizeMode {
		TOP, TOP_RIGHT, TOP_LEFT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT;
	}

	private ResizeMode mode;
	private final Area area;
	private final BorderPane wrapper;
	private final FluentRegion top = new FluentRegion().classes("border", "top").fullWidth();
	private final FluentRegion topRight = new FluentRegion().classes("border", "top", "right");
	private final FluentRegion right = new FluentRegion().classes("border", "right");
	private final FluentRegion bottomRight = new FluentRegion().classes("border", "bottom", "right");
	private final FluentRegion bottom = new FluentRegion().classes("border", "bottom").fullWidth();
	private final FluentRegion bottomLeft = new FluentRegion().classes("border", "bottom", "left");
	private final FluentRegion left = new FluentRegion().classes("border", "left");
	private final FluentRegion topLeft = new FluentRegion().classes("border", "top", "left");
	

	public ResizableDecorator(BorderPane wrapper) {
		this(new RegionAreaAdapter(wrapper), wrapper);
	}
	
	public ResizableDecorator(Stage stage, BorderPane wrapper) {
		this(new StageAreaAdapter(stage), wrapper);
	}
	
	public ResizableDecorator(Area area, BorderPane wrapper) {
		this.wrapper = wrapper;
		this.area = area;
		build();
		setCursors();
		bind();
	}

	private void bind() {
		top.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.TOP, event));
		topRight.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.TOP_RIGHT, event));
		right.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.RIGHT, event));
		bottomRight.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.BOTTOM_RIGHT, event));
		bottom.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.BOTTOM, event));
		bottomLeft.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.BOTTOM_LEFT, event));
		left.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.LEFT, event));
		topLeft.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> pressed(ResizeMode.TOP_LEFT, event));
		wrapper.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> dragged(event));
		wrapper.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> released());
	}
	
	public void addMouseEventHandler(final EventType<MouseEvent> type, final EventHandler<MouseEvent> eventHandler){
		top.addEventHandler(type, eventHandler);
		topRight.addEventHandler(type, eventHandler);
		right.addEventHandler(type, eventHandler);
		bottomRight.addEventHandler(type, eventHandler);
		bottom.addEventHandler(type, eventHandler);
		bottomLeft.addEventHandler(type, eventHandler);
		left.addEventHandler(type, eventHandler);
		topLeft.addEventHandler(type, eventHandler);
	}
	
	public void removeMouseEventHandler(final EventType<MouseEvent> type, final EventHandler<MouseEvent> eventHandler){
		top.removeEventHandler(type, eventHandler);
		topRight.removeEventHandler(type, eventHandler);
		right.removeEventHandler(type, eventHandler);
		bottomRight.removeEventHandler(type, eventHandler);
		bottom.removeEventHandler(type, eventHandler);
		bottomLeft.removeEventHandler(type, eventHandler);
		left.removeEventHandler(type, eventHandler);
		topLeft.removeEventHandler(type, eventHandler);
	}

	protected void pressed(final ResizeMode mode, final MouseEvent event) {
		this.mode = mode;
	}

	protected void released() {
		this.mode = null;
	}

	protected void dragged(final MouseEvent event) {
		if (null != mode) {
			switch (mode) {
			case BOTTOM:
				area.setBounds(area.getX(), area.getY(), area.getWidth(), event.getY());
				break;
			case BOTTOM_LEFT:
				area.setBounds(area.getX() + event.getX(), area.getY(), area.getWidth() - event.getX(), event.getY());
				break;
			case BOTTOM_RIGHT:
				area.setBounds(area.getX(), area.getY(), event.getX(), event.getY());
				break;
			case LEFT:
				area.setBounds(area.getX() + event.getX(), area.getY(), area.getWidth() - event.getX(),
						area.getHeight());
				break;
			case RIGHT:
				area.setBounds(area.getX(), area.getY(), event.getX(), area.getHeight());
				break;
			case TOP:
				area.setBounds(area.getX(), area.getY() + event.getY(), area.getWidth(),
						area.getHeight() - event.getY());
				break;
			case TOP_LEFT:
				area.setBounds(area.getX() + event.getX(), area.getY() + event.getY(), area.getWidth() - event.getX(),
						area.getHeight() - event.getY());
				break;
			case TOP_RIGHT:
				area.setBounds(area.getX(), area.getY() + event.getY(), event.getX(), area.getHeight() - event.getY());
				break;
			default:
				break;
			}
		}
	}

	private void build() {
		wrapper.setLeft(left);
		wrapper.setRight(right);
		wrapper.setTop(new FluentHBox(topLeft, top, topRight));
		wrapper.setBottom(new FluentHBox(bottomLeft, bottom, bottomRight));
	}

	private void setCursors() {
		top.setCursor(Cursor.N_RESIZE);
		topRight.setCursor(Cursor.NE_RESIZE);
		right.setCursor(Cursor.E_RESIZE);
		bottomRight.setCursor(Cursor.SE_RESIZE);
		bottom.setCursor(Cursor.S_RESIZE);
		bottomLeft.setCursor(Cursor.SW_RESIZE);
		left.setCursor(Cursor.W_RESIZE);
	}

}

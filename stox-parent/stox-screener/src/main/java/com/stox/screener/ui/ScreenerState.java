package com.stox.screener.ui;

import java.util.ArrayList;
import java.util.List;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.screen.Screen;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScreenerState {

	private Object config;
	
	private Screen<?> screen;
	
	private int span = 1;
	
	private int offset = 0;
	
	private volatile boolean stopped;
	
	private BarSpan barSpan = BarSpan.D;
	
	private final List<Scrip> scrips = new ArrayList<>();
	
	
}

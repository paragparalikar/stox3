package com.stox.auto;

public interface AutoWidget {

	void populateView();
	
	void populateModel();
	
}

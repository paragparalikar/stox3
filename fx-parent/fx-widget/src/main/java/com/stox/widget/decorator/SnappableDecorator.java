package com.stox.widget.decorator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.stox.widget.Ui;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class SnappableDecorator{

	private Pane container;
	private double xSize = 20;
	private double ySize = 20;
	private boolean initialized;
	private final double boxSize = 20;
	private final Group group = new Group();
	private final Rectangle rectangle = new Rectangle();
	private final List<Binding> bindings = new ArrayList<>();
	
	
	public SnappableDecorator(Pane container) {
		this.container = container;
		group.setVisible(false);
		group.setManaged(false);
		group.getChildren().add(rectangle);
		container.getChildren().add(group);
		rectangle.getStyleClass().add("snap-to-grid-rectangle");
		container.layoutBoundsProperty().addListener((o, old, bounds) -> rebuild());
		rebuild();
	}
	
	public void add(final Node knob, final BorderPane target){
		container.getChildren().add(target);
		Ui.fade(target, 0, 1, null);
		final EventHandler<MouseEvent> mousePressedHandler = e -> mousePressed(target);
		final EventHandler<MouseEvent> mouseReleasedHandler = e -> mouseReleased(target);
		final DockableDecorator dockableDecorator = new DockableDecorator(knob, target, container);
		final Binding binding = new Binding(dockableDecorator, mousePressedHandler, mouseReleasedHandler);
		binding.bind();
		bindings.add(binding);
		snap(target);
	}
	
	public void remove(final BorderPane target){
		Ui.fade(target, 1, 0, ()->{
			container.getChildren().remove(target);
		});
		final Iterator<Binding> iterator = bindings.iterator();
		while(iterator.hasNext()){
			final Binding binding = iterator.next();
			if(target == binding.getDockableDecorator().getTarget()){
				binding.unbind();
				iterator.remove();
				break;
			}
		}
	}
	
	
	private void mousePressed(Region region){
		if (!initialized) rebuild();
		group.setVisible(true);
		rectangle.setLayoutX(Math.max(0, snap(region.getLayoutX(), xSize)));
		rectangle.setLayoutY(Math.max(0, snap(region.getLayoutY(), ySize)));
		rectangle.setHeight(Math.min(container.getHeight(), snap(region.getLayoutY() + region.getHeight(), ySize) - rectangle.getLayoutY()));
		rectangle.setWidth(Math.min(container.getWidth(), snap(region.getLayoutX() + region.getWidth(), xSize) - rectangle.getLayoutX()));
	}
		
	private void mouseReleased(Region region){
		region.resizeRelocate(rectangle.getLayoutX(), rectangle.getLayoutY(), rectangle.getWidth(), rectangle.getHeight());
		group.setVisible(false);
	}

	private void rebuild() {
		if (0 == container.getHeight() || 0 == container.getWidth()) {
			return;
		}
		group.getChildren().clear();
		group.getChildren().add(rectangle);

		xSize = container.getWidth() / new Double(container.getWidth() / boxSize).intValue();
		ySize = container.getHeight() / new Double(container.getHeight() / boxSize).intValue();

		double pointer = 0;
		while (pointer < container.getWidth()) {
			pointer += xSize;
			final Line line = new Line(pointer, 0, pointer, container.getHeight());
			line.getStyleClass().add("snap-to-grid-line");
			group.getChildren().add(line);
		}

		pointer = 0;
		while (pointer < container.getHeight()) {
			pointer += ySize;
			final Line line = new Line(0, pointer, container.getWidth(), pointer);
			line.getStyleClass().add("snap-to-grid-line");
			group.getChildren().add(line);
		}
		initialized = true;
	}
	
	private void snap(final Region region) {
		final double x = Math.max(0, snap(region.getLayoutX(), xSize));
		region.setLayoutX(x < xSize ? 0 : x);
		
		final double y = Math.max(0, snap(region.getLayoutY(), ySize));
		region.setLayoutY(y < ySize ? 0 : y);
		
		final double currentWidth = Math.max(Math.max(region.getWidth(), region.getPrefWidth()),
				Math.max(region.getMinWidth(), region.getMaxWidth()));
		double width = Math.min(container.getWidth(), snap(currentWidth, xSize));
		width = xSize > (container.getWidth() - (x+width)) ? container.getWidth() - x : width;
		region.setMinWidth(width);
		region.setPrefWidth(width);
		region.setMaxWidth(width);
		
		final double currentHeight = Math.max(Math.max(region.getHeight(), region.getPrefHeight()),
				Math.max(region.getMinHeight(), region.getMaxHeight()));
		double height = Math.min(container.getHeight(), snap(currentHeight, ySize));
		height = ySize > (container.getHeight() - (y+height)) ? container.getHeight() - y : height;
		region.setMinHeight(height);
		region.setPrefHeight(height);
		region.setMaxHeight(height);
	}
		
	private double snap(double value, double size) {
		double rem = value % size;
		return ((int)(rem < size / 2 ? value - rem : value - rem + size));
	}
	
}


class Binding{
	
	private final DockableDecorator dockableDecorator;
	private final EventHandler<MouseEvent> mousePressedHandler;
	private final EventHandler<MouseEvent> mouseReleasedHandler;
		
	public Binding(DockableDecorator dockableDecorator, 
			EventHandler<MouseEvent> mousePressedHandler,
			EventHandler<MouseEvent> mouseReleasedHandler) {
		super();
		this.dockableDecorator = dockableDecorator;
		this.mousePressedHandler = mousePressedHandler;
		this.mouseReleasedHandler = mouseReleasedHandler;
	}
	
	public void bind(){
		dockableDecorator.getKnob().addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
		dockableDecorator.getKnob().addEventFilter(MouseEvent.MOUSE_DRAGGED, mousePressedHandler);
		dockableDecorator.getKnob().addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
		final ResizableDecorator resizableDecorator = dockableDecorator.getResizableDecorator();
		resizableDecorator.addMouseEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
		resizableDecorator.addMouseEventHandler(MouseEvent.MOUSE_DRAGGED, mousePressedHandler);
		resizableDecorator.addMouseEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
		
	}
	
	public void unbind(){
		dockableDecorator.getKnob().removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
		dockableDecorator.getKnob().removeEventHandler(MouseEvent.MOUSE_DRAGGED, mousePressedHandler);
		dockableDecorator.getKnob().removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
		final ResizableDecorator resizableDecorator = dockableDecorator.getResizableDecorator();
		resizableDecorator.removeMouseEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
		resizableDecorator.removeMouseEventHandler(MouseEvent.MOUSE_DRAGGED, mousePressedHandler);
		resizableDecorator.removeMouseEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
	}

	protected DockableDecorator getDockableDecorator() {
		return dockableDecorator;
	}

	protected EventHandler<MouseEvent> getMousePressedHandler() {
		return mousePressedHandler;
	}

	protected EventHandler<MouseEvent> getMouseReleasedHandler() {
		return mouseReleasedHandler;
	}
	
}

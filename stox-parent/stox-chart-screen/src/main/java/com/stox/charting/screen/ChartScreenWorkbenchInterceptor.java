package com.stox.charting.screen;

import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;

public class ChartScreenWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		workbench.getStage().getScene().getStylesheets().add("style/charting-screen.css");
	}
	
}

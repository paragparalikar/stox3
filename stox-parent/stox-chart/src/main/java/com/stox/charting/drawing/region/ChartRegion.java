package com.stox.charting.drawing.region;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.drawing.AbstractDrawing;
import com.stox.charting.drawing.ControlPoint;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

public class ChartRegion extends AbstractDrawing {
	public static final int ID = 4;

	private final Rectangle rectangle = new Rectangle();
	private final ControlPoint one = new ControlPoint();
	private final ControlPoint two = new ControlPoint();
	private final Group node = new Group(rectangle, one, two);

	public ChartRegion() {
		rectangle.getStyleClass().add("chart-region");
		bind();
	}
	
	protected void bind() {
		super.bind();
		rectangle.xProperty().bind(one.centerXProperty());
		rectangle.yProperty().bind(one.centerYProperty());
		rectangle.widthProperty().bind(two.centerXProperty().subtract(one.centerXProperty()));
		rectangle.heightProperty().bind(two.centerYProperty().subtract(one.centerYProperty()));

		final ChartRegionMouseEventHandler chartRegionMouseEventHandler = new ChartRegionMouseEventHandler(this);
		rectangle.addEventHandler(MouseEvent.MOUSE_PRESSED, chartRegionMouseEventHandler);
		rectangle.addEventHandler(MouseEvent.MOUSE_DRAGGED, chartRegionMouseEventHandler);
	}

	@Override
	public Node getNode() {
		return node;
	}

	@Override
	public int getDrawingId() {
		return ID;
	}

	@Override
	public void update(XAxis xAxis, YAxis yAxis) {
		one.update(xAxis, yAxis);
		two.update(xAxis, yAxis);
	}
	
	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis) {
		one.layoutChartChildren(xAxis, yAxis);
		two.layoutChartChildren(xAxis, yAxis);
	}

	@Override
	public String format() {
		return one.format() + ";" + two.format();
	}

	@Override
	public void parse(String text) {
		final String[] tokens = text.split(";");
		one.parse(tokens[0]);
		two.parse(tokens[1]);
	}

	public void move(final double xDelta, final double yDelta) {
		move(one.getCenterX() + xDelta, one.getCenterY() + yDelta, two.getCenterX() + xDelta,
				two.getCenterY() + yDelta);
	}

	public void move(final double startX, final double startY, final double endX, final double endY) {
		one.setCenterX(startX);
		one.setCenterY(startY);
		two.setCenterX(endX);
		two.setCenterY(endY);
	}
}

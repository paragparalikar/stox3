package com.stox.watchlist.ui.menu;

import java.util.Iterator;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.HasBarSpan;
import com.stox.core.model.intf.HasScrip;
import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.model.WatchlistEntry;
import com.stox.watchlist.repository.WatchlistEntryRepository;
import com.stox.watchlist.repository.WatchlistRepository;
import com.stox.widget.Icon;

import javafx.collections.FXCollections;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

public class AddToWatchlistMenu extends Menu {
	
	private final Object target;
	private final WatchlistRepository watchlistRepository = WatchlistRepository.getInstance();
	private final WatchlistEntryRepository watchlistEntryRepository = WatchlistEntryRepository.getInstance();
	
	public AddToWatchlistMenu(final Object target) {
		super("Add to Watchlist", new FluentLabel(Icon.BOOKMARK).classes("icon", "primary", "inverted", "transparent-background"));
		this.target = target;
		watchlistRepository.find().forEach(watchlist -> {
			final MenuItem item = new MenuItem(watchlist.getName());
			getItems().add(item);
			item.setUserData(watchlist);
			item.setOnAction(e -> add(watchlist));
		});
		FXCollections.sort(getItems(), (one, two) -> one.getText().compareToIgnoreCase(two.getText()));
		Event.register(this);
	}
	
	@Override
	protected void finalize() throws Throwable {
		Event.unregister(this);
		super.finalize();
	}
	
	@EventHandler(Watchlist.CREATED)
	public void onWatchlistCreated(Watchlist watchlist){
		final MenuItem item = new MenuItem(watchlist.getName());
		getItems().add(item);
		item.setUserData(watchlist);
		item.setOnAction(e -> add(watchlist));
		FXCollections.sort(getItems(), (one, two) -> one.getText().compareToIgnoreCase(two.getText()));
	}
	
	@EventHandler(Watchlist.MODIFIED)
	public void onWatchlistModified(Watchlist watchlist){
		for(MenuItem item : getItems()){
			final Watchlist value = (Watchlist)item.getUserData();
			if(value.getId().equals(watchlist.getId())){
				item.setUserData(watchlist);
				item.setText(watchlist.getName());
			}
		}
	}
	
	@EventHandler(Watchlist.DELETED)
	public void onWatchlistDeleted(Watchlist watchlist){
		final Iterator<MenuItem> iterator = getItems().iterator();
		while(iterator.hasNext()){
			final MenuItem item = iterator.next();
			final Watchlist value = (Watchlist)item.getUserData();
			if(value.getId().equals(watchlist.getId())){
				iterator.remove();
				break;
			}
		}
	}
	
	private void add(Watchlist watchlist){
		final Scrip scrip = ((HasScrip)target).getScrip();
		final BarSpan barSpan = ((HasBarSpan)target).getBarSpan();
		final WatchlistEntry entry = new WatchlistEntry(watchlist.getId(), scrip, barSpan);
		final WatchlistEntry savedEntry = watchlistEntryRepository.save(watchlist.getId(), entry);
		if(null != savedEntry){
			Event.fire(WatchlistEntry.CREATED, entry);
		}
	}

}

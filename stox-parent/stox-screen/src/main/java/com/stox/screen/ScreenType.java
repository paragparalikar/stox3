package com.stox.screen;

public enum ScreenType {

	BULLISH("Bullish"), BEARISH("Bearish"), NEUTRAL("Neutral");

	private final String name;

	private ScreenType(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return getName();
	}
}

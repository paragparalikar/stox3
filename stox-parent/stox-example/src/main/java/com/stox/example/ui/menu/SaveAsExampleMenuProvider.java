package com.stox.example.ui.menu;

import com.stox.core.model.intf.HasBarSpan;
import com.stox.core.model.intf.HasDate;
import com.stox.core.model.intf.HasScrip;
import com.stox.widget.menu.MenuProvider;

import javafx.scene.control.ContextMenu;

public class SaveAsExampleMenuProvider implements MenuProvider {

	@Override
	public void accept(ContextMenu contextMenu, Object target) {
		if(		   target instanceof HasScrip 
				&& target instanceof HasBarSpan 
				&& target instanceof HasDate){
			contextMenu.getItems().add(new SaveAsExampleMenu(target));
		}
	}

}

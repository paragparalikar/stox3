package com.stox.screen;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.util.BarUtil;

public class BullishPinBarScreen implements Screen<Void> {

	@Override
	public String getCode() {
		return "screen-bullish-bar-pin";
	}

	@Override
	public String getName() {
		return "Bullish Pin Bar";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Void buildDefaultConfig() {
		return null;
	}

	@Override
	public int getMinBarCount(Void config) {
		return 1;
	}

	@Override
	public boolean isMatch(List<Bar> bars, Void config) {
		return BarUtil.isBullishPinBar(bars.get(0));
	}

}

package com.stox.charting;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import com.stox.charting.axis.horizontal.MutableXAxis;
import com.stox.charting.axis.vertical.TransformerYAxis;
import com.stox.charting.chart.Chart;
import com.stox.charting.chart.PrimaryChart;
import com.stox.charting.chart.SecondaryChart;
import com.stox.charting.event.ConfigChangedEvent;
import com.stox.charting.event.DataChangedEvent;
import com.stox.charting.event.DataLoadingEvent;
import com.stox.charting.event.PanRequestEvent;
import com.stox.charting.event.PlotRemovedEvent;
import com.stox.charting.event.UnderlayChangedEvent;
import com.stox.charting.event.ZoomRequestEvent;
import com.stox.charting.grid.VerticalGrid;
import com.stox.charting.plot.DerivativePlot;
import com.stox.charting.plot.Underlay;
import com.stox.charting.plot.VolumePlot;
import com.stox.charting.tools.ChartingToolBar;
import com.stox.charting.unit.PriceUnitType;
import com.stox.charting.widget.BarInfoPanel;
import com.stox.charting.widget.Crosshair;
import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.HasBarSpan;
import com.stox.core.model.intf.HasDate;
import com.stox.core.model.intf.HasScrip;
import com.stox.core.ui.LinkButton;
import com.stox.core.ui.model.Link;
import com.stox.core.ui.model.Link.State;
import com.stox.ui.fx.fluent.scene.control.FluentSplitPane;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;
import com.stox.widget.Ui;
import com.stox.widget.decorator.GlassableDecorator;
import com.stox.widget.menu.TargetAwareContextMenu;
import com.stox.workbench.module.view.ModuleView;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Side;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.stage.Window;

public class ChartingView extends ModuleView implements HasScrip, HasBarSpan, HasDate{
	public static final String ID = "chart";
	private static final int RIGHT_GAP = 5;
	
	private long to;
	private BarSpan barSpan = BarSpan.D;
	private ModeMouseHandler mouseModeHandler;
	
	private final Configuration configuration = new Configuration();
	private final LinkButton linkButton = new LinkButton();
	private final List<Chart> charts = new ArrayList<>();
	private final MutableXAxis xAxis = new MutableXAxis();
	private final TransformerYAxis volumeYAxis = new TransformerYAxis();
	private final VerticalGrid verticalGrid = new VerticalGrid();
	private final BarInfoPanel barInfoPanel = new BarInfoPanel();
	private final VolumePlot volumePlot = new VolumePlot(configuration, volumeYAxis); 
	private final PrimaryChart primaryChart = new PrimaryChart(configuration, xAxis, volumeYAxis, verticalGrid, barInfoPanel);
	private final Consumer<State> stateConsumer = this::linkStateChanged;
	private final FluentSplitPane splitPane = new FluentSplitPane(primaryChart).orientation(Orientation.VERTICAL).classes("charting-split-pane");
	private final Crosshair crosshair = new Crosshair(this);
	private final TargetAwareContextMenu contextMenu = new TargetAwareContextMenu(this);
	private final FluentStackPane root = new FluentStackPane(verticalGrid, splitPane, crosshair).classes("charting-root");
	private final GlassableDecorator glassableDecorator = new GlassableDecorator(root);
	private final PanAndZoomMouseHandler panAndZoomMouseHandler = new PanAndZoomMouseHandler(splitPane);
	private final ProgressIndicator progressIndicator = new ProgressIndicator();
	
	public ChartingView() {
		content(root);
		getTitleBar().add(Side.RIGHT, linkButton);
		linkButton.getLinkProperty().addListener((o, old, link) -> linkChanged(old, link));
		primaryChart.add(volumePlot);
		setMouseModeHandler(panAndZoomMouseHandler);
		progressIndicator.getStyleClass().addAll("primary","inverted","transparent-background", "large");
		bind();
	}
	
	@Override
	public void start() {
		super.start();
		bottom(new ChartingToolBar(this));
		Platform.runLater(() -> linkChanged(null, linkButton.getLink()));
	}
	
	private void bind() {
		final EventHandler<MouseEvent> indexInfoHandler = event -> showIndexInfo(event.getX());
		splitPane.addEventFilter(MouseEvent.MOUSE_MOVED, indexInfoHandler);
		splitPane.addEventFilter(MouseEvent.MOUSE_DRAGGED, indexInfoHandler);
		splitPane.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> showContextMenu(event));
		splitPane.addEventHandler(UnderlayChangedEvent.TYPE, event -> add(remove(event.getPlot())));
		splitPane.addEventHandler(ConfigChangedEvent.TYPE, event -> configChanged(event.getPlot()));
		splitPane.addEventHandler(ZoomRequestEvent.TYPE, event -> zoom(event.getX(), event.getPercentage()));
		splitPane.addEventHandler(PanRequestEvent.TYPE, event -> pan(event.getDeltaX()));
		splitPane.addEventHandler(PlotRemovedEvent.TYPE, event -> removeEmptyCharts());
		splitPane.addEventHandler(DataLoadingEvent.TYPE, e -> barDataLoading(e.isLoading()));
		
		primaryChart.addPrimaryPlotDataChangedEventHandler(event -> barDataChanged(event.getBars()));
	}
	
	private void barDataLoading(boolean loading){
		synchronized(glassableDecorator){
			if(loading && !glassableDecorator.isGlassShowing()){
				glassableDecorator.showGlass(progressIndicator);
			}else{
				glassableDecorator.hideGlass();
			}
		}
	}
	
	public void setMouseModeHandler(final ModeMouseHandler mouseModeHandler) {
		Optional.ofNullable(this.mouseModeHandler).ifPresent(handler -> handler.detach());
		this.mouseModeHandler = null == mouseModeHandler ? panAndZoomMouseHandler : mouseModeHandler;
		this.mouseModeHandler.attach();
	}
	
	private void showIndexInfo(final double x) {
		final int index = xAxis.getIndex(x);
		primaryChart.showIndexInfo(index);
		charts.forEach(chart -> chart.showIndexInfo(index));
	}
	
	private void showContextMenu(final MouseEvent event) {
		if (MouseButton.SECONDARY.equals(event.getButton()) && !event.isConsumed()) {
			final Window window = getNode().getScene().getWindow();
			contextMenu.show(window, event.getScreenX(), event.getScreenY());
		}
	}
	
	private void configChanged(final DerivativePlot<?> plot) {
		plot.load(primaryChart.getBars());
		plot.updateValueBounds(xAxis.getClippedStartIndex(), xAxis.getClippedEndIndex());
		Optional.ofNullable(getChart(plot)).ifPresent(c -> c.layoutChartChildren());
	}
	
	private void linkChanged(Link old, Link link){
		if(null != old){
			old.remove(stateConsumer);
		}
		if(null != link){
			link.add(stateConsumer);
			linkStateChanged(link.getState());
		}
	}
	
	private void linkStateChanged(State state){
		if(null != state){
			unload();
			reset();
			this.to = state.getTo();
			primaryChart.setScrip(state.getScrip());
			barSpan = null == state.getBarSpan() ? barSpan : state.getBarSpan();
			updateTitleText();
			primaryChart.load(to, this.barSpan, xAxis);
		}
	}
	
	private void relayoutCharts() {
		final int size = splitPane.getItems().size();
		for (int index = 0; index < size - 1; index++) {
			splitPane.setDividerPosition(index, 1 - ((size - index - 1) * 0.2));
		}
	}
	
	private DerivativePlot<?> remove(final DerivativePlot<?> plot) {
		primaryChart.remove(plot);
		charts.forEach(chart -> chart.remove(plot));
		removeEmptyCharts();
		return plot;
	}
	
	private void removeEmptyCharts() {
		final Iterator<Chart> chartIterator = charts.iterator();
		while (chartIterator.hasNext()) {
			final Chart chart = chartIterator.next();
			if (chart.isEmpty()) {
				chartIterator.remove();
				splitPane.getItems().remove(chart);
				relayoutCharts();
			}
		}
	}
	
	public DerivativePlot<?> add(final DerivativePlot<?> plot) {
		if (Underlay.PRICE.equals(plot.getUnderlay())) {
			add(primaryChart, plot);
		} else if (Underlay.VOLUME.equals(plot.getUnderlay())) {
			if (primaryChart.contains(volumePlot)) {
				add(primaryChart, plot);
			} else {
				charts.stream().filter(chart -> chart.contains(volumePlot)).findFirst()
						.ifPresent(chart -> add(chart, plot));
			}
		} else {
			final Chart secondaryChart = new SecondaryChart(configuration, xAxis, volumeYAxis);
			charts.add(secondaryChart);
			splitPane.getItems().add(secondaryChart);
			add(secondaryChart, plot);
			relayoutCharts();
		}
		return plot;
	}
	
	private void add(Chart chart, DerivativePlot<?> plot) {
		chart.add(plot);
		plot.load(primaryChart.getBars());
		chart.updateValueBounds();
		chart.layoutChartChildren();
	}

	public void load(final DerivativePlot<?> plot) {
		plot.load(primaryChart.getBars());
	}

	public void setBarSpan(final BarSpan barSpan) {
		if (null != barSpan && !barSpan.equals(this.barSpan)) {
			unload();
			reset();
			this.barSpan = barSpan;
			updateTitleText();
			primaryChart.load(to, barSpan, xAxis);
		}
	}

	private void updateTitleText() {
		final Scrip scrip = primaryChart.getScrip();
		getTitleBar().titleText(null == scrip ? null : scrip.getName() + " - " + barSpan.getName());
	}

	public void reset() {
		primaryChart.reset();
		charts.forEach(Chart::reset);
		xAxis.setPivotX(primaryChart.getContentWidth() - xAxis.getUnitWidth() * RIGHT_GAP);
	}
	
	public void barDataChanged(final List<Bar> bars) {
		final Scrip scrip = primaryChart.getScrip();
		xAxis.setBars(bars);
		primaryChart.load(scrip, bars);
		charts.forEach(chart -> chart.load(scrip, bars));
		updateValueBounds();
		Ui.fx(() -> layoutChartChildren());
	}

	private void updateValueBounds() {
		primaryChart.updateValueBounds();
		charts.forEach(chart -> chart.updateValueBounds());
	}

	public void layoutChartChildren() {
		primaryChart.layoutChartChildren();
		charts.forEach(chart -> chart.layoutChartChildren());
	}

	public void unload() {
		final Scrip scrip = primaryChart.getScrip();
		primaryChart.unload(scrip);
		charts.forEach(chart -> chart.unload(scrip));
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	protected void initializeDefaultBounds(){
		final BorderPane wrapper = getNode();
		final Region parent = (Region) wrapper.getParent();
		wrapper.setLayoutX(parent.getWidth()/5);
		wrapper.setPrefWidth(parent.getWidth()*4/5);
		wrapper.setPrefHeight(parent.getHeight());
	}

	public void clearDrawings() {
		primaryChart.clearDrawings();
		charts.forEach(Chart::clearDrawings);
	}
	
	public Chart getChart(final double screenX, final double screenY) {
		final Point2D point = new Point2D(screenX, screenY);
		return primaryChart.contains(primaryChart.screenToLocal(point)) ? primaryChart
				: charts.stream().filter(chart -> chart.contains(chart.screenToLocal(point))).findFirst().orElse(null);
	}

	private Chart getChart(DerivativePlot<?> plot) {
		if (primaryChart.contains(plot)) {
			return primaryChart;
		}
		return charts.stream().filter(chart -> chart.contains(plot)).findFirst().orElse(null);
	}

	@Override
	public Date getDate() {
		final double x = contextMenu.getAnchorX();
		final Point2D point = primaryChart.screenToLocal(x, 0);
		final int index = xAxis.getIndex(point.getX());
		final List<Bar> bars = primaryChart.getBars();
		if (index >= 0 && index < bars.size()) {
			final Bar bar = bars.get(index);
			if (null != bar) {
				return new Date(bar.getDate());
			}
		}
		return null;
	}

	@Override
	public Scrip getScrip() {
		return primaryChart.getScrip();
	}
	
	@Override
	public BarSpan getBarSpan() {
		return barSpan;
	}
	
	private void zoom(final double x, final int percentage) {
		xAxis.zoom(0 == x ? xAxis.getUnitWidth() : x, percentage);
		updateValueBounds();
		layoutChartChildren();
		primaryChart.load(to, barSpan, xAxis);
	}

	private void pan(final double deltaX) {
		xAxis.pan(deltaX);
		updateValueBounds();
		layoutChartChildren();
		primaryChart.load(to, barSpan, xAxis);
	}
	
	public Region getContainer() {
		return splitPane;
	}
	
	public PriceUnitType getUnitType() {
		return primaryChart.getUnitType();
	}

	public void setUnitType(final PriceUnitType unitType) {
		primaryChart.setUnitType(unitType);
	}
	
	public LinkButton getLinkButton() {
		return linkButton;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}


}

package com.stox.workbench.module.view;

import com.stox.event.Event;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;

public abstract class ModuleView {
	public static final String SHOW = "MODULE_VIEW_SHOW";
	public static final String HIDE = "MODULE_VIEW_HIDE";

	private final ModuleViewTitleBar titleBar = new ModuleViewTitleBar()
			.closeEventHandler(e -> Event.fire(HIDE, ModuleView.this));
	private final FluentBorderPane container = new FluentBorderPane().top(titleBar.getNode());
	private final FluentStackPane root = new FluentStackPane(container);
	private final FluentBorderPane wrapper = new FluentBorderPane(root).managed(false).classes("module-view");
	
	public abstract String getId();

	public BorderPane getNode() {
		return wrapper;
	}

	protected ModuleView content(Node node) {
		container.center(node);
		return this;
	}
	
	protected ModuleView bottom(Node node){
		container.bottom(node);
		return this;
	}

	public ModuleViewTitleBar getTitleBar() {
		return titleBar;
	}
	
	public void start() {
		initializeBounds();
	}
	
	protected void initializeBounds(){
		if(0 == wrapper.getPrefHeight() || 0 == wrapper.getPrefWidth()){
			initializeDefaultBounds();
		}
		wrapper.resizeRelocate(
				wrapper.getLayoutX(), 
				wrapper.getLayoutY(), 
				wrapper.getPrefWidth(),
				wrapper.getPrefHeight());
	}
	
	protected void initializeDefaultBounds(){
		final Region parent = (Region) wrapper.getParent();
		wrapper.setPrefWidth(parent.getWidth()/5);
		wrapper.setPrefHeight(parent.getHeight());
	}

	public void stop() {

	}

}

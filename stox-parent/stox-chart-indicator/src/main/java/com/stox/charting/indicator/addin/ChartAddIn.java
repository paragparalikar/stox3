package com.stox.charting.indicator.addin;

import com.stox.auto.AutoWidget;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.unit.Unit;

import javafx.scene.Node;

public interface ChartAddIn<MODEL>  {

	void update();
	
	Node getNode();
	
	AutoWidget getEditor();

	void preLayoutChartChildren(final XAxis xAxis, final YAxis yAxis);

	void layoutUnit(final MODEL model, final Unit<MODEL> unit, final XAxis xAxis, final YAxis yAxis);

	void postLayoutChartChildren(final XAxis xAxis, final YAxis yAxis);

}

package com.stox.widget.form;

import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;

public class FormField extends FluentVBox {
	
	private FluentLabel errorLabel;
	private final FluentLabel nameLabel = new FluentLabel();
	private final FluentHBox hbox = new FluentHBox(nameLabel).classes("spaced");
	
	public FormField(String name) {
		nameLabel.setText(name);
		children(hbox).classes("form-field", "spaced").fullArea();
	}
		
	public void setError(String error){
		if(null == errorLabel){
			errorLabel = new FluentLabel().classes("danger", "inverted");
		}
		if(!hbox.getChildren().contains(errorLabel)){
			hbox.getChildren().add(1, errorLabel);
		}
		errorLabel.setText(error);
	}
}

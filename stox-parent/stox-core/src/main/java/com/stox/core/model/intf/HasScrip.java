package com.stox.core.model.intf;

import com.stox.core.model.Scrip;

public interface HasScrip {

	public Scrip getScrip();

}

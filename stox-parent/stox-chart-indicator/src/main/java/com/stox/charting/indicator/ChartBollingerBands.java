package com.stox.charting.indicator;

import java.util.Collections;
import java.util.List;

import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.indicator.unit.ChannelUnit;
import com.stox.charting.indicator.unit.parent.ChannelUnitParent;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.indicator.BollingerBands;
import com.stox.indicator.BollingerBands.Config;
import com.stox.indicator.Channel;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import lombok.experimental.Delegate;

public class ChartBollingerBands extends AbstractChartIndicator<Config, Channel, Point2D>{

	@Delegate
	private final Indicator<Config, Channel> bollingerBandsIndicator = IndicatorFactory.get(BollingerBands.class);
	
	@Override
	public String getName() {
		return "Bollinger Bands";
	}

	@Override
	public UnitParent<Point2D> buildParent(Group group) {
		return new ChannelUnitParent(group);
	}

	@Override
	public Unit<Channel> buildUnit(Parent<Point2D> parent) {
		return new ChannelUnit(parent);
	}

	@Override
	public boolean isGroupable() {
		return false;
	}

	@Override
	public Underlay getUnderlay(Config config) {
		return Underlay.PRICE;
	}

	@Override
	public List<ChartAddIn<Channel>> buildAddIns(Config config, UnitParent<Point2D> parent) {
		return Collections.emptyList();
	}

	@Override
	public double getMin(Channel value) {
		return null == value ? Double.MAX_VALUE : value.getLower();
	}

	@Override
	public double getMax(Channel value) {
		return null == value ? Double.MIN_VALUE : value.getUpper();
	}
	
}

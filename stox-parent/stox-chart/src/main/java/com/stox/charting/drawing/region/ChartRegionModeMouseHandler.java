package com.stox.charting.drawing.region;

import com.stox.charting.ChartingView;
import com.stox.charting.ModeMouseHandler;

import javafx.scene.input.MouseEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ChartRegionModeMouseHandler implements ModeMouseHandler {

	private final ChartingView chartingView;
	private final ChartRegionModeMouseEventHandler handler;

	@Override
	public void attach() {
		chartingView.getContainer().addEventFilter(MouseEvent.MOUSE_PRESSED, handler);
		chartingView.getContainer().addEventFilter(MouseEvent.MOUSE_DRAGGED, handler);
		chartingView.getContainer().addEventFilter(MouseEvent.MOUSE_RELEASED, handler);
	}

	@Override
	public void detach() {
		chartingView.getContainer().removeEventFilter(MouseEvent.MOUSE_PRESSED, handler);
		chartingView.getContainer().removeEventFilter(MouseEvent.MOUSE_DRAGGED, handler);
		chartingView.getContainer().removeEventFilter(MouseEvent.MOUSE_RELEASED, handler);
	}
}

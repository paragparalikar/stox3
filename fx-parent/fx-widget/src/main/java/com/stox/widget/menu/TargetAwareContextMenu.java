package com.stox.widget.menu;

import java.util.ServiceLoader;

import javafx.scene.control.ContextMenu;

public class TargetAwareContextMenu extends ContextMenu {

	private final Object target;

	public TargetAwareContextMenu(final Object target) {
		this.target = target;
		setAutoHide(true);
		getStyleClass().add("target-aware-context-menu");
		build();
	}

	private void build() {
		final ServiceLoader<MenuProvider> serviceLoader = ServiceLoader.load(MenuProvider.class);
		serviceLoader.forEach(menuProvider -> menuProvider.accept(this, target));
	}

}

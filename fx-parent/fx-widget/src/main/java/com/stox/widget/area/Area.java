package com.stox.widget.area;


import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;

public interface Area{
	
	Node getNode();

	double getX();

	double getY();

	double getHeight();

	double getWidth();

	Cursor getCursor();

	void setCursor(final Cursor cursor);

	Rectangle2D getMaxBounds();

	void setBounds(double x, double y, double width, double height);

	<T extends Event> void addEventHandler(final EventType<T> eventType, final EventHandler<? super T> eventHandler);

	<T extends Event> void removeEventHandler(EventType<T> eventType, EventHandler<T> eventHandler);

	<T extends Event> void addEventFilter(final EventType<T> eventType, final EventHandler<? super T> eventHandler);

	<T extends Event> void removeEventFilter(EventType<T> eventType, EventHandler<T> eventHandler);

}

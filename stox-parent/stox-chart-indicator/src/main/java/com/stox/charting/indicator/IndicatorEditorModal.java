package com.stox.charting.indicator;

import com.stox.auto.AutoUiBuilder;
import com.stox.auto.AutoVBox;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.workbench.modal.Modal;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class IndicatorEditorModal extends Modal{

	private final AutoVBox autoVBox;
	private final FluentButton cancelButton = new FluentButton("Cancel").cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton("Edit").classes("primary").defaultButton(true);
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");
	private final FluentBorderPane content = new FluentBorderPane().classes("padded","spaced"); 

	@SuppressWarnings("rawtypes")
	public IndicatorEditorModal(Object configuration, ChartIndicator chartIndicator) {
		super("Edit "+chartIndicator);

		final AutoUiBuilder uiBuilder = new AutoUiBuilder();
		autoVBox = uiBuilder.build(configuration);
		content.center(autoVBox);
		autoVBox.populateView();

		getContainer().center(content).setBottom(buttonBar);
		getNode().getStyleClass().add("primary");
		getTitleBar().getTitleLabel().setGraphic(new FluentLabel(Icon.LINE_CHART).focusTraversable(false).classes("primary","icon"));
	}
	
	@Override
	protected void resizeRelocate() {
		final double height = 300;
		final double width = 500;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}
	
	private void cancel(){
		hide();
	}
	
	public void setOnAction(EventHandler<ActionEvent> value){
		actionButton.setOnAction(e -> {
			autoVBox.populateModel();
			value.handle(e);
			hide();
		});
	}
	
}

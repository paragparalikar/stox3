package com.stox.watchlist.ui;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.ui.filter.scrip.ScripFilterVBox;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.model.WatchlistEntry;
import com.stox.watchlist.repository.WatchlistEntryRepository;
import com.stox.watchlist.repository.WatchlistRepository;

public class WatchlistScripFilterVBox extends ScripFilterVBox<Watchlist> {

	public WatchlistScripFilterVBox() {
		super("Watchlists");
		final WatchlistRepository watchlistRepository = WatchlistRepository.getInstance();
		items(watchlistRepository.find());
	}

	@Override
	public void filter(Iterable<Scrip> values) {
		final List<Watchlist> watchlists = selectedItems();
		if(!watchlists.isEmpty()){
			final Set<String> isins = getIsins(watchlists);
			final Iterator<Scrip> iterator = values.iterator();
			while(iterator.hasNext()){
				final Scrip scrip = iterator.next();
				if(!isins.contains(scrip.getIsin())){
					iterator.remove();
				}
			}
		}
	}
	
	private Set<String> getIsins(List<Watchlist> watchlists){
		final Set<String> isins = new HashSet<>();
		final WatchlistEntryRepository entryRepository = WatchlistEntryRepository.getInstance();
		for(Watchlist watchlist : watchlists){
			for(BarSpan barSpan : BarSpan.values()){
				final List<WatchlistEntry> entries = entryRepository.find(watchlist.getId(), barSpan);
				for(WatchlistEntry entry : entries){
					isins.add(entry.getScrip().getIsin());
				}
			}
		}
		return isins;
	}

}

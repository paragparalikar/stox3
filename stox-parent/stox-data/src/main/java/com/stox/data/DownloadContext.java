package com.stox.data;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

import com.stox.core.model.Exchange;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DownloadContext {
	
	private boolean cancelled;

	private final Exchange exchange;
	
	private final Runnable afterCallback;
	
	private final Calendar lastDownloadDate;
	
	private final long initialLastDownloadDate;
	
	private final Runnable terminationCallback;
	
	private final ExecutorService executorService;
	
	private final Consumer<StatusMessage> messageCallback;
	
}

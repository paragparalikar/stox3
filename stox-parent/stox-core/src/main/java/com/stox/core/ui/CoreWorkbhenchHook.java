package com.stox.core.ui;

import com.stox.core.repository.LinkStateRepository;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;

public class CoreWorkbhenchHook implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		workbench.getStage().getScene().getStylesheets().add("style/link.css");
		new LinkStateRepository().loadAll();
	}

	@Override
	public void afterStop(Workbench workbench) throws Exception {
		new LinkStateRepository().persistAll();
	}

}

package com.stox.watchlist.ui;

import com.stox.widget.Icon;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class WatchlistWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(WatchlistView.ID, Icon.BOOKMARK, "Watchlist", true, () -> new WatchlistView());
		workbench.register(registration);
	}

}

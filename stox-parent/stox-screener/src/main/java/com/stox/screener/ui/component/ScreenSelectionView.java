package com.stox.screener.ui.component;

import com.stox.screen.Screen;
import com.stox.screen.ScreenFactory;
import com.stox.screener.ui.ScreenerModuleView;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;

import javafx.geometry.Pos;
import javafx.scene.control.ListCell;

public class ScreenSelectionView {

	private final ScreenerModuleView screenerModuleView;
	private final FluentListView<Screen<?>> listView = new FluentListView<Screen<?>>().cellFactory(lv -> new ScreenListCell());
	private final FluentButton nextButton = new FluentButton(Icon.LONG_ARROW_RIGHT).classes("primary", "icon").onAction(e -> next());
	private final FluentHBox buttonBar = new FluentHBox(nextButton).classes("padded").alignment(Pos.CENTER_RIGHT).fullWidth();
	
	public ScreenSelectionView(ScreenerModuleView rankerModuleView) {
		this.screenerModuleView = rankerModuleView;
		nextButton.disableProperty().bind(listView.getSelectionModel().selectedItemProperty().isNull());
	}
	
	public void start(){
		listView.items(ScreenFactory.find());
		screenerModuleView.content(listView).bottom(buttonBar);
	}
	
	private void next(){
		final Screen<?> screen = listView.getSelectionModel().getSelectedItem();
		screenerModuleView.getState().setScreen(screen);
		screenerModuleView.getState().setConfig(screen.buildDefaultConfig());
		screenerModuleView.content(null).bottom(null);
		
		final ScreenConfigView rankerConfigView = new ScreenConfigView(screenerModuleView);
		rankerConfigView.start();
	}
	
}


class ScreenListCell extends ListCell<Screen<?>>{
	
	@Override
	protected void updateItem(Screen<?> item, boolean empty) {
		super.updateItem(item, empty);
		if(null == item || empty){
			setText(null);
		}else{
			setText(item.getName());
		}
	}
	
}
package com.stox.screener.ui;

import com.stox.core.ui.LinkButton;
import com.stox.screener.ui.component.ScreenSelectionView;
import com.stox.widget.Icon;
import com.stox.workbench.module.view.ModuleView;

import javafx.geometry.Side;
import javafx.scene.Node;

public class ScreenerModuleView extends ModuleView {
	public static final String ID = "screener";
	public static final String ICON = Icon.FILTER;

	private final ScreenerState state = new ScreenerState();
	private final LinkButton linkButton = new LinkButton();

	public ScreenerModuleView() {
		getTitleBar().add(Side.RIGHT, linkButton);
	}
	
	@Override
	public void start() {
		super.start();
		final ScreenSelectionView screenSelectionView = new ScreenSelectionView(this);
		screenSelectionView.start();
	}

	@Override
	public String getId() {
		return ID;
	}

	public LinkButton getLinkButton() {
		return linkButton;
	}

	@Override
	public ScreenerModuleView content(Node node) {
		super.content(node);
		return this;
	}

	@Override
	public ScreenerModuleView bottom(Node node) {
		super.bottom(node);
		return this;
	}

	public ScreenerState getState() {
		return state;
	}
}

package com.stox.indicator;

import java.util.List;
import java.util.stream.Collectors;

import com.stox.core.model.Bar;
import com.stox.core.model.BarValue;
import com.stox.indicator.Decision.Config;

public class Decision implements Indicator<Config, Double> {

	public static class Config {

	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	private Double value(final Bar bar) {
		return Math.log(
				BarValue.SPREAD.get(bar) * BarValue.BODY.get(bar) * Math.abs(bar.getClose() - BarValue.MID.get(bar)));
	}

	@Override
	public Double compute(List<Double> values, List<Bar> bars, Config config) {
		return null != bars && !bars.isEmpty() ? value(bars.get(0)) : null;
	}

	@Override
	public List<Double> computeAll(List<Double> values, List<Bar> bars, Config config) {
		return bars.stream().map(bar -> value(bar)).collect(Collectors.toList());
	}

}

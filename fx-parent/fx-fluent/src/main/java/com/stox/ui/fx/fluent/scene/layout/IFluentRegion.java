package com.stox.ui.fx.fluent.scene.layout;

import com.stox.ui.fx.fluent.scene.IFluentNode;

import javafx.geometry.Insets;
import javafx.scene.layout.Region;

public interface IFluentRegion<T extends Region & IFluentRegion<T>> extends IFluentNode<T> {

	default T height(double value) {
		getThis().setMinHeight(value);
		getThis().setPrefHeight(value);
		getThis().setMaxHeight(value);
		return getThis();
	}

	default T width(double value) {
		getThis().setMinWidth(value);
		getThis().setPrefWidth(value);
		getThis().setMaxWidth(value);
		return getThis();
	}

	@Override
	default T fullHeight() {
		getThis().setMaxHeight(Double.MAX_VALUE);
		return IFluentNode.super.fullHeight();
	}

	@Override
	default T fullWidth() {
		getThis().setMaxWidth(Double.MAX_VALUE);
		return IFluentNode.super.fullWidth();
	}

	default T padding(Insets value) {
		getThis().setPadding(value);
		return getThis();
	}

	default T padding(double value) {
		return padding(new Insets(value));
	}
	
	default T snapToPixed(boolean value){
		getThis().setSnapToPixel(value);
		return getThis();
	}

}

package com.stox.example.ui;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.ui.LinkButton;
import com.stox.core.ui.model.Link;
import com.stox.core.ui.model.Link.State;
import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.example.model.Example;
import com.stox.example.model.ExampleGroup;
import com.stox.example.repository.ExampleRepository;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentToggleButton;
import com.stox.ui.fx.fluent.scene.layout.FluentStackPane;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.util.Async;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.widget.decorator.GlassableDecorator;
import com.stox.widget.search.SearchBox;
import com.stox.widget.search.SearchableListView;
import com.stox.workbench.module.view.ModuleView;

import javafx.collections.FXCollections;
import javafx.geometry.Side;
import javafx.scene.control.ProgressIndicator;

public class ExampleModuleView extends ModuleView {
	public static final String ID = "example";

	private final LinkButton linkButton = new LinkButton();
	
	private final FluentToggleButton filterButton = new FluentToggleButton(Icon.FILTER).classes("primary","icon").onAction(e -> toggleFilter()).tooltip(Ui.tooltip("Filter"));
	private final ExampleGroupBox exampleGroup = new ExampleGroupBox();
	private final FluentComboBox<BarSpan> barSpanComboBox = new FluentComboBox<BarSpan>().items(BarSpan.values()).onSelect((o,old,value) -> refresh()).fullArea().classes("primary","inverted");
	private final FluentVBox filterContainer = new FluentVBox(exampleGroup, barSpanComboBox).classes("primary-background","spaced");
	
	private final SearchableListView<Example> listView = new SearchableListView<>();
	private final FluentStackPane root = new FluentStackPane(listView);
	private final GlassableDecorator glassableDecorator = new GlassableDecorator(root);
	private final ProgressIndicator progressIndicator = new ProgressIndicator();
	private final SearchBox<Example> searchBox = new SearchBox<Example>(listView, this::test);
	private final FluentToggleButton searchButton = new FluentToggleButton(Icon.SEARCH).classes("primary","icon").onAction(e -> toggleSearch());
	
	private volatile BarSpan currentBarSpan;
	private volatile ExampleGroup currentExampleGroup;
	private final ExampleRepository exampleRepository = ExampleRepository.getInstance();
	
	public ExampleModuleView() {
		content(listView);
		searchBox.classes("primary").fullWidth();
		getTitleBar().add(Side.RIGHT, filterButton);
		getTitleBar().add(Side.RIGHT, searchButton);
		getTitleBar().add(Side.RIGHT, linkButton);
		exampleGroup.addChangeListener((o,old,watchlist) -> refresh());
		
		listView.setCellFactory(param -> new ExampleListCell(exampleGroup));
		listView.getSelectionModel().selectedItemProperty().addListener((o, old, entry) -> updateLinkState(entry));
		progressIndicator.getStyleClass().addAll("primary","inverted","transparent-background", "large");
	}
	
	private boolean test(Example entry, String text){
		final Scrip scrip = entry.getScrip();
		text = text.trim().toLowerCase();
		return null != scrip && (
				scrip.getName().trim().toLowerCase().contains(text) || 
				scrip.getCode().trim().toLowerCase().contains(text) ||
				scrip.getIsin().trim().toLowerCase().contains(text));
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public void start() {
		super.start();
		exampleGroup.start();
		barSpanComboBox.select(BarSpan.D);
		Event.register(this);
	}
	
	@Override
	public void stop() {
		Event.unregister(this);
		exampleGroup.stop();
		super.stop();
	}
	
	@EventHandler(Example.CREATED)
	public void onExampleCreated(Example entry){
		if(null != currentExampleGroup 
				&& null != currentBarSpan
				&& currentExampleGroup.getId().equals(entry.getGroupId())
				&& currentBarSpan.equals(entry.getBarSpan())){
			Ui.fx(() -> {
				listView.getItems().add(entry);
				FXCollections.sort(listView.getItems());
			});
		}
	}
	
	@EventHandler(Example.DELETED)
	public void onExampleDeleted(Example entry){
		Ui.fx(() -> listView.getItems().remove(entry));
	}
	
	private void updateLinkState(Example example){
		if(null != example){
			final Link link = linkButton.getLink();
			link.setState(new State(example.getDate(), example.getScrip(), example.getBarSpan()));
		}
	}
	
	private void toggleFilter(){
		getTitleBar().add(filterButton.isSelected(), Side.BOTTOM, filterContainer);
	}
	
	private void toggleSearch(){
		getTitleBar().add(searchButton.isSelected(), Side.BOTTOM, searchBox);
		if(searchButton.isSelected()){
			searchBox.getTextField().requestFocus();
		}
	}
	
	private void refresh(){
		final ExampleGroup group = exampleGroup.get();
		final BarSpan barSpan = barSpanComboBox.getSelectionModel().getSelectedItem();
		if(null != group && null != barSpan && (!Objects.equals(currentExampleGroup, group) || !Objects.equals(currentBarSpan, barSpan))){
			filterContainer.disable(true);
			glassableDecorator.showGlass(progressIndicator);
			Async.execute(() -> {
				final List<Example> entries = exampleRepository.find(group.getId(), barSpan);
				Collections.sort(entries);
				Ui.fx(() -> {
					listView.getItems().setAll(entries);
					currentExampleGroup = group;
					currentBarSpan = barSpan;
					filterContainer.disable(false);
					glassableDecorator.hideGlass();
				});
			});
		}
	}

}

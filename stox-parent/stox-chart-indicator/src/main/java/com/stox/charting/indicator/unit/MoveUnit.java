package com.stox.charting.indicator.unit;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.unit.Unit;
import com.stox.core.model.Bar;
import com.stox.core.model.Move;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MoveUnit implements Unit<Move> {

	@NonNull
	private final Parent<Point2D> parent;

	@Override
	public void update(int index, Move model, Move previousModel, XAxis xAxis, YAxis yAxis) {
		if (null != model) {
			if (parent.isEmpty()) {
				final Bar start = model.getStart();
				parent.add(new Point2D(getX(model.getStartIndex(), xAxis), yAxis.getY(model.getBarValue().get(start))));
			}
			final Bar end = model.getEnd();
			parent.add(new Point2D(getX(model.getEndIndex(), xAxis), yAxis.getY(model.getBarValue().get(end))));
		}
	}

	private double getX(final int index, final XAxis xAxis) {
		return xAxis.getX(index) + xAxis.getUnitWidth() / 2;
	}

	@Override
	public void attach() {

	}

	@Override
	public void detach() {

	}

}

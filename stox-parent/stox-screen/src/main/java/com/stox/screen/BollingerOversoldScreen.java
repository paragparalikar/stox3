package com.stox.screen;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.BarValue;
import com.stox.screen.BollingerOversoldScreen.Config;

import lombok.Getter;
import lombok.Setter;

public class BollingerOversoldScreen implements Screen<Config>{

	@Getter
	@Setter
	public static class Config{
		private int span = 14;
		private double multiple = 2;
		private BarValue barValue = BarValue.CLOSE;
	}

	@Override
	public String getCode() {
		return "bollinger-oversold";
	}

	@Override
	public String getName() {
		return "Bollinger Oversold";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan();
	}

	@Override
	public boolean isMatch(List<Bar> bars, Config config) {
		double sum = 0;
		final BarValue barValue = config.getBarValue();
		for (int i = 0; i < config.getSpan(); i++) {
			sum += barValue.get(bars.get(i));
		}
		double deviationSum = 0;
		final double average = sum / config.getSpan();
		for (int i = 0; i < config.getSpan(); i++) {
			double deviation = average - barValue.get(bars.get(i));
			deviationSum += Math.pow(deviation, 2);
		}
		final double stdDev = Math.sqrt(deviationSum / config.getSpan());
		final double lowerBollingerBand = average - config.getMultiple()*stdDev;
		return barValue.get(bars.get(1)) <= lowerBollingerBand && barValue.get(bars.get(0)) >= lowerBollingerBand;
	}
	
	
	
}

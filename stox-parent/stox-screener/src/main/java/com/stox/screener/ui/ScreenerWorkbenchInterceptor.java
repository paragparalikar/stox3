package com.stox.screener.ui;

import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class ScreenerWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(ScreenerModuleView.ID,
				ScreenerModuleView.ICON, "Screener", true, () -> new ScreenerModuleView());
		workbench.register(registration);
	}

}

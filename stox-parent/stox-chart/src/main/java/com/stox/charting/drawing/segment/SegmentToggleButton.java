package com.stox.charting.drawing.segment;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.stox.charting.ChartingView;
import com.stox.charting.drawing.AbstractDrawingToggleButton;
import com.stox.charting.drawing.event.UpdatableRequestEvent;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.MouseEvent;

public abstract class SegmentToggleButton<T extends Segment> extends AbstractDrawingToggleButton {

	public SegmentToggleButton(ChartingView chartingView) {
		super(chartingView);
		getStyleClass().addAll("icon", "primary");
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		selectedProperty().addListener(this);
	}

	@Override
	public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
		super.changed(observable, oldValue, newValue);
		final ChartingView chartingView = getChartingView();
		if (newValue && null != chartingView) {
			final SegmentModeMouseHandler modeMouseHandler = new SegmentModeMouseHandler(chartingView,
					buildSegmentModeMouseEventHandler((segment, event) -> {
						add(segment, event.getScreenX(), event.getScreenY());
						final Point2D point = segment.getNode().getParent().screenToLocal(event.getScreenX(),
								event.getScreenY());
						move(segment, point);
						segment.getNode().fireEvent(new UpdatableRequestEvent(segment));
					}, segment -> {
						setSelected(false);
						chartingView.setMouseModeHandler(null);
					}));
			chartingView.setMouseModeHandler(modeMouseHandler);
		}
	}

	protected abstract SegmentModeMouseEventHandler<T> buildSegmentModeMouseEventHandler(
			final BiConsumer<T, MouseEvent> startCallback, final Consumer<T> endCallback);

	protected abstract void move(final T segment, final Point2D point);

}

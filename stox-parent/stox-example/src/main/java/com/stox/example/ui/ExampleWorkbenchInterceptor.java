package com.stox.example.ui;

import com.stox.widget.Icon;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class ExampleWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(ExampleModuleView.ID, Icon.BOOKMARK, "Examples", true, () -> new ExampleModuleView());
		workbench.register(registration);
	}

}

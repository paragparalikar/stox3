package com.stox.watchlist.model;

import java.util.Comparator;
import java.util.Objects;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.HasScrip;

import lombok.NonNull;
import lombok.Value;

@Value
public class WatchlistEntry implements HasScrip, Comparable<WatchlistEntry>{
	public static final String CREATED = "WATCHLIST_ENTRY_CREATED";
	public static final String DELETED = "WATCHLIST_ENTRY_DELETED";
	public static final Comparator<WatchlistEntry> COMPARATOR = (one, two) -> one.scrip.getName().compareToIgnoreCase(two.getScrip().getName());

	@NonNull
	private String watchlistId;
	
	@NonNull
	private Scrip scrip;
	
	@NonNull
	private BarSpan barSpan;

	@Override
	public int compareTo(WatchlistEntry other) {
		return Objects.compare(this, other, COMPARATOR);
	}
	
}

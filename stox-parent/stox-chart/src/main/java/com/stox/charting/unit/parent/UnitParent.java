package com.stox.charting.unit.parent;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.widget.parent.Parent;

import javafx.beans.property.ObjectProperty;
import javafx.scene.paint.Color;

public interface UnitParent<C> extends Parent<C> {
	
	void unbindColorProperty();
	
	void preLayoutChartChildren(final XAxis xAxis, final YAxis yAxis);
	
	void postLayoutChartChildren(final XAxis xAxis, final YAxis yAxis);
	
	void bindColorProperty(final ObjectProperty<Color> colorProperty);
	
}

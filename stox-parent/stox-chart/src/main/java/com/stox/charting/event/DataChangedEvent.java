package com.stox.charting.event;

import java.util.List;

import com.stox.core.model.Bar;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;

public class DataChangedEvent extends Event {
	private static final long serialVersionUID = 8748545028485578771L;
	public static final EventType<DataChangedEvent> TYPE = new EventType<>("BarDataChangedEvent");

	@Getter
	private final List<Bar> bars;

	public DataChangedEvent(final List<Bar> bars) {
		super(TYPE);
		this.bars = bars;
	}

}

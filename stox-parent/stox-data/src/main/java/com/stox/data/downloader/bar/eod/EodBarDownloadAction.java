package com.stox.data.downloader.bar.eod;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Exchange;
import com.stox.core.repository.BarRepository;
import com.stox.core.repository.PropertyRepository;
import com.stox.data.DownloadContext;
import com.stox.data.StatusMessage;
import com.stox.util.Action;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EodBarDownloadAction implements Action {
	private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);

	private StatusMessage message;
	private final DownloadContext context;
	private final BarRepository barRepository = BarRepository.getInstance();
	private final PropertyRepository propertyRepository = PropertyRepository.getInstance();

	@Override
	public boolean validate() {
		final Calendar lastDownloadDate = context.getLastDownloadDate();
		return !context.isCancelled() && Calendar.SATURDAY != lastDownloadDate.get(Calendar.DAY_OF_WEEK)
				&& Calendar.SUNDAY != lastDownloadDate.get(Calendar.DAY_OF_WEEK);
	}

	@Override
	public void before() {
		final Calendar lastDownloadDate = context.getLastDownloadDate();
		message = new StatusMessage("Download EOD bars for " + DATE_FORMAT.format(lastDownloadDate.getTime()));
		context.getMessageCallback().accept(message);
	}

	@Override
	public void execute() throws Throwable {
		final Exchange exchange = context.getExchange();
		final Calendar lastDownloadDate = context.getLastDownloadDate();
		final EodBarDownloaderFactory eodBarDownloaderFactory = new EodBarDownloaderFactory();
		final EodBarDownloader eodBarDownloader = eodBarDownloaderFactory.create(exchange);
		final List<Bar> bars = eodBarDownloader.download(lastDownloadDate.getTime());
		persist(bars);
	}
	
	private void persist(List<Bar> bars) throws InterruptedException{
		final CountDownLatch latch = new CountDownLatch(bars.size());
		bars.forEach(bar -> context.getExecutorService().submit(() -> {
			try{
				barRepository.save(bar, BarSpan.D);
			}finally{
				latch.countDown();
			}
		}));
		latch.await();
	}

	@Override
	public void success() {
		final Exchange exchange = context.getExchange();
		final Calendar lastDownloadDate = context.getLastDownloadDate();
		propertyRepository.put(exchange.getKeyLastDownloadDate(), lastDownloadDate);
		message.setSuccess(true);
	}

	@Override
	public void failure(Throwable throwable) {
		message.setSuccess(false);
	}

	@Override
	public void after() {
		context.getAfterCallback().run();
		final Calendar lastDownloadDate = context.getLastDownloadDate();
		lastDownloadDate.add(Calendar.DATE, 1);
		if (context.isCancelled() || lastDownloadDate.after(Calendar.getInstance())) {
			context.getTerminationCallback().run();
		} else {
			final EodBarDownloadAction action = new EodBarDownloadAction(context);
			context.getExecutorService().submit(action);
		}
	}

}

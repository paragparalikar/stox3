package com.stox.charting.widget;

import com.stox.charting.ChartingView;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.drawing.Updatable;
import com.stox.charting.drawing.event.UpdatableRequestEvent;
import com.stox.ui.fx.fluent.scene.layout.FluentGroup;
import com.stox.util.StringUtil;
import com.stox.widget.Ui;

import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Crosshair extends FluentGroup implements Updatable {

	private final ChartingView chartingView;
	private final Label valueLabel = new Label();
	private final Line vertical = new Line();
	private final Line horizontal = new Line();
	
	public Crosshair(final ChartingView chartingView) {
		managed(false).setMouseTransparent(true);
		getStyleClass().add("crosshair");
		getChildren().addAll(vertical, horizontal, valueLabel);
		this.chartingView = chartingView;
		vertical.getStyleClass().add("vertical");
		horizontal.getStyleClass().add("horizontal");
		bind(chartingView.getContainer());
	}
	
	@Override
	protected void layoutChildren() {

	};

	private void update(final MouseEvent event) {
		vertical.setStartX(Ui.px(event.getX()));
		horizontal.setStartY(Ui.px(event.getY()));
		fireEvent(new UpdatableRequestEvent(this));
	}

	@Override
	public void update(XAxis xAxis, YAxis yAxis) {
		final double height = 16;
		final double width = xAxis.getWidth();
		valueLabel.setText(StringUtil.stringValueOf(yAxis.getValue(horizontal.getStartX())));
		valueLabel.resizeRelocate(chartingView.getContainer().getWidth() - width, horizontal.getStartY() - height / 2,
				width, height);
	}

	private void bind(final Region region) {
		bindNodes(region);
		bindHandlers(region);
	}

	private void bindNodes(final Region region) {
		vertical.endXProperty().bind(vertical.startXProperty());
		horizontal.endYProperty().bind(horizontal.startYProperty());
		vertical.startYProperty().bind(region.layoutYProperty());
		vertical.endYProperty().bind(region.heightProperty());
		horizontal.startXProperty().bind(region.layoutXProperty());
		horizontal.endXProperty().bind(region.widthProperty());

		valueLabel.setBackground(new Background(new BackgroundFill(Color.GRAY, null, null)));
		valueLabel.setTextFill(Color.WHITE);
	}

	private void bindHandlers(final Region region) {
		region.addEventHandler(MouseEvent.MOUSE_MOVED, event -> update(event));
		region.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> update(event));
	}
}

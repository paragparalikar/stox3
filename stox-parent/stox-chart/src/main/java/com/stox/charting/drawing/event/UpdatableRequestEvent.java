package com.stox.charting.drawing.event;

import com.stox.charting.drawing.Updatable;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;

public class UpdatableRequestEvent extends Event {
	private static final long serialVersionUID = -5092828484914700437L;

	public static final EventType<UpdatableRequestEvent> TYPE = new EventType<>(
			"UpdatableRequestEvent");

	@Getter
	private final Updatable updatable;

	public UpdatableRequestEvent(final Updatable updatable) {
		super(TYPE);
		this.updatable = updatable;
	}

}

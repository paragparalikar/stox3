package com.stox.screen;

import java.util.Collections;
import java.util.List;

import com.stox.core.model.Bar;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.indicator.RelativeStrengthIndex;
import com.stox.indicator.RelativeStrengthIndex.Config;

public class BullishReversalBarScreen implements Screen<RelativeStrengthIndex.Config> {

	private final Indicator<RelativeStrengthIndex.Config, Double> rsi = IndicatorFactory
			.get(RelativeStrengthIndex.class);

	@Override
	public String getCode() {
		return "screen-bullish-bar-reversal";
	}

	@Override
	public String getName() {
		return "Bullish Reversal Bar";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Config buildDefaultConfig() {
		return rsi.buildDefaultConfig();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan();
	}

	@Override
	public boolean isMatch(List<Bar> bars, Config config) {
		final Double rsiValue = rsi.compute(Collections.emptyList(), bars, config);
		if(null == rsiValue || 30 < rsiValue){
			return false;
		}
		
		final Bar bar = bars.get(0);
		final Bar previous = bars.get(1);
		return bar.getClose() >= (bar.getHigh() - (bar.getHigh() - bar.getLow())/3) 
				&& previous.getClose() <= (previous.getLow() + (previous.getHigh() - previous.getLow())/3);
	}

}

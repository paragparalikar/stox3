package com.stox.charting.indicator;

import com.stox.charting.ChartingView;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.workbench.modal.Modal;

import javafx.scene.Scene;

public class IndicatorChooserModal extends Modal{

	private final ChartingView chartingView;
	private final FluentListView<ChartIndicator<?,?,?>> listView = new FluentListView<ChartIndicator<?,?,?>>();
	private final FluentButton cancelButton = new FluentButton("Cancel").cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton("Add").classes("primary").defaultButton(true).onAction(e -> action());
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");

	public IndicatorChooserModal(ChartingView chartingView) {
		super("Indicators");
		this.chartingView = chartingView;
		getContainer().setCenter(listView);
		getContainer().setBottom(buttonBar);
		getNode().getStyleClass().add("primary");
		getTitleBar().getTitleLabel().setGraphic(new FluentLabel(Icon.LINE_CHART).focusTraversable(false).classes("primary","icon"));
		
		final ChartIndicatorFactory factory = new ChartIndicatorFactory();
		listView.getItems().addAll(factory.find());
	}
	
	@Override
	protected void resizeRelocate() {
		final double height = 500;
		final double width = 300;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}
	
	private void cancel(){
		hide();
	}
	
	private void action(){
		final ChartIndicator<?,?,?> indicator = listView.selectionModel().getSelectedItem();
		if(null != indicator){
			final IndicatorPlot<?,?,?> plot = new IndicatorPlot<>(indicator, chartingView.getConfiguration());
			chartingView.load(plot);
			chartingView.add(plot);
			hide();
		}
	}
	
}

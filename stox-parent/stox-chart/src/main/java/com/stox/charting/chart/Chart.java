package com.stox.charting.chart;

import java.util.ArrayList;
import java.util.List;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.DelegatingYAxis;
import com.stox.charting.axis.vertical.MutableYAxis;
import com.stox.charting.axis.vertical.ValueAxis;
import com.stox.charting.drawing.Drawing;
import com.stox.charting.drawing.DrawingRepository;
import com.stox.charting.drawing.event.DrawingRemoveRequestEvent;
import com.stox.charting.drawing.event.UpdatableRequestEvent;
import com.stox.charting.event.PlotRemovedEvent;
import com.stox.charting.plot.DerivativePlot;
import com.stox.charting.plot.Plot;
import com.stox.charting.plot.Underlay;
import com.stox.charting.plot.info.PlotInfoPane;
import com.stox.charting.widget.NoLayoutPane;
import com.stox.core.model.Bar;
import com.stox.core.model.Scrip;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.widget.Ui;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode(of = "content", callSuper=false)
public class Chart extends FluentBorderPane {
	private static final Color[] COLORS = { Color.BLUE, Color.BLUEVIOLET, Color.BROWN, Color.CRIMSON, Color.RED,
			Color.AQUA, Color.DARKGREEN };

	private final XAxis xAxis;
	private final MutableYAxis yAxis;
	private ValueAxis valueAxis;
	private final Configuration configuration;
	private final DelegatingYAxis volumeYAxis;
	private final VBox plotInfoContainer = new VBox();
	private final List<Drawing> drawings = new ArrayList<>();
	private final List<DerivativePlot<?>> plots = new ArrayList<>();
	private final Pane content = new NoLayoutPane();

	public Chart(final Configuration configuration, final XAxis xAxis, final DelegatingYAxis volumeYAxis) {
		center(content).classes("chart");
		content.getStyleClass().add("content");
		this.xAxis = xAxis;
		this.volumeYAxis = volumeYAxis;
		yAxis = new MutableYAxis();
		this.configuration = configuration;
		getChildren().add(plotInfoContainer);
	}

	protected void bind() {
		content.heightProperty().addListener((o, old, value) -> {
			yAxis.setHeight(value.doubleValue());
			updateValueBounds();
			layoutChartChildren();
		});
		content.addEventHandler(DrawingRemoveRequestEvent.TYPE, event -> remove(event.getDrawing()));
		addEventHandler(UpdatableRequestEvent.TYPE, event -> event.getUpdatable().update(xAxis, yAxis));
	}

	public void setValueAxis(@NonNull ValueAxis valueAxis) {
		this.valueAxis = valueAxis;
		setRight(valueAxis);
	}

	public void load(final Scrip scrip, final List<Bar> bars) {
		plots.forEach(plot -> plot.load(bars));
		if (null != scrip && drawings.isEmpty()) {
			final DrawingRepository drawingRespository = DrawingRepository.getInstance();
			final List<Drawing> drawings = drawingRespository.load(buildId(scrip));
			drawings.forEach(drawing -> add(drawing));
		}
	}

	public void unload(final Scrip scrip) {
		if (null != scrip) {
			final DrawingRepository drawingRespository = DrawingRepository.getInstance();
			drawingRespository.persist(buildId(scrip), drawings);
		}
	}

	protected String buildId(final Scrip scrip) {
		return null == scrip ? null : scrip.getIsin();
	}

	public void showIndexInfo(final int index) {
		plots.forEach(plot -> plot.showIndexInfo(index));
	}

	public void reset() {
		clearDrawings();
		plots.forEach(Plot::reset);
	}

	public boolean add(final DerivativePlot<?> plot) {
		if (!plots.contains(plot)) {
			plot.setColor(COLORS[plots.size()]);
			content.getChildren().add(plot);
			addPlotInfoPane(plot);
			return plots.add(plot);
		}
		return false;
	}

	private void addPlotInfoPane(final DerivativePlot<?> plot) {
		final PlotInfoPane plotInfoPane = plot.getPlotInfoPane();
		plotInfoPane.addVisibilityEventHandler(event -> plot.setVisible(!plot.isVisible()));
		plotInfoContainer.getChildren().add(plotInfoPane.getNode());
		plotInfoPane.addRemoveEventHandler(e -> {
			remove(plot);
			fireEvent(new PlotRemovedEvent(plot));
		});
	}

	public void remove(final DerivativePlot<?> plot) {
		plots.remove(plot);
		content.getChildren().remove(plot);
		plotInfoContainer.getChildren().remove(plot.getPlotInfoPane().getNode());
	}

	public boolean add(final Drawing drawing) {
		if (!drawings.contains(drawing)) {
			Ui.fx(() -> content.getChildren().add(drawing.getNode()));
			return drawings.add(drawing);
		}
		return false;
	}

	public void remove(final Drawing drawing) {
		drawings.remove(drawing);
		content.getChildren().remove(drawing.getNode());
	}

	public boolean contains(final Plot<?> plot) {
		return plots.contains(plot);
	}

	public boolean isEmpty() {
		return plots.isEmpty();
	}

	public void updateValueBounds() {
		yAxis.reset();
		final int start = xAxis.getClippedStartIndex(), end = xAxis.getClippedEndIndex();
		plots.forEach(plot -> {
			plot.updateValueBounds(start, end);
			yAxis.setMin(Math.min(yAxis.getMin(), plot.getMin()));
			yAxis.setMax(Math.max(yAxis.getMax(), plot.getMax()));
		});
	}

	public void layoutChartChildren() {
		synchronized (volumeYAxis) {
			if (0 < content.getWidth() || 0 < content.getHeight()) {
				volumeYAxis.setDelegate(yAxis);
				plots.forEach(plot -> plot.layoutChartChildren(xAxis,
						Underlay.VOLUME.equals(plot.getUnderlay()) ? volumeYAxis : yAxis));
				drawings.forEach(drawing -> drawing.layoutChartChildren(xAxis, yAxis));
				valueAxis.layoutChartChildren(yAxis);
			}
		}
	}

	public void clearDrawings() {
		while (0 < drawings.size()) {
			remove(drawings.get(0));
		}
	}

	protected Configuration getConfiguration() {
		return configuration;
	}

	protected MutableYAxis getMutableYAxis() {
		return yAxis;
	}
	
	public Pane getContent() {
		return content;
	}

	public VBox getPlotInfoContainer() {
		return plotInfoContainer;
	}
	
	public ReadOnlyDoubleProperty contentWidthProperty() {
		return getContent().widthProperty();
	}

	public double getContentWidth() {
		return getContent().getWidth();
	}

}

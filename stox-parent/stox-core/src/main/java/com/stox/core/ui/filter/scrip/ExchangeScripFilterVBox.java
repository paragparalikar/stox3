package com.stox.core.ui.filter.scrip;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;

public class ExchangeScripFilterVBox extends ScripFilterVBox<Exchange> {

	public ExchangeScripFilterVBox() {
		super("Exchanges");
		items(Arrays.asList(Exchange.values()));
	}

	@Override
	public void filter(Iterable<Scrip> values) {
		final List<Exchange> exchanges = selectedItems();
		if(!exchanges.isEmpty()){
			final Iterator<Scrip> iterator = values.iterator();
			while(iterator.hasNext()){
				final Scrip scrip = iterator.next();
				if(!exchanges.contains(scrip.getExchange())){
					iterator.remove();
				}
			}
		}
	}

}

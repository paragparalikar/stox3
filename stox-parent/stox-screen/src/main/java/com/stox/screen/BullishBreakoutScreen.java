package com.stox.screen;

import java.util.Collections;
import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.BarValue;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.indicator.SimpleMovingAverage;
import com.stox.screen.BullishBreakoutScreen.Config;

import lombok.Getter;
import lombok.Setter;

/**
 * Spread, Volume, Close near high, Close - Previous Close difference, Close -
 * Open difference,
 * 
 * 
 *
 */
public class BullishBreakoutScreen implements Screen<Config> {

	@Getter
	@Setter
	public static class Config {
		private int span = 14;
		private double multiple = 2;
	}

	private final Indicator<SimpleMovingAverage.Config, Double> sma = IndicatorFactory.get(SimpleMovingAverage.class);

	@Override
	public String getCode() {
		return "screen-bullish-breakout";
	}

	@Override
	public String getName() {
		return "Bullish Breakout Screen";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan();
	}

	@Override
	public boolean isMatch(List<Bar> bars, Config config) {
		final Bar bar = bars.get(0);
		final Bar previous = bars.get(1);
		
		if(BarValue.SPREAD.get(bar) <= BarValue.SPREAD.get(previous)){
			return false;
		}
		if(bar.getHigh() <= previous.getHigh()){
			return false;
		}
		if(bar.getVolume() <= previous.getVolume()){
			return false;
		}
		
		final SimpleMovingAverage.Config smaConfig = sma.buildDefaultConfig();
		smaConfig.setSpan(config.getSpan());
		smaConfig.setBarValue(BarValue.SPREAD);
		final Double averageSpread = sma.compute(Collections.emptyList(), bars, smaConfig);
		if (BarValue.SPREAD.get(bar) <= averageSpread) {
			return false;
		}
		
		smaConfig.setBarValue(BarValue.VOLUME);
		final Double averageVolume = sma.compute(Collections.emptyList(), bars, smaConfig);
		if (bar.getVolume() <= averageVolume) {
			return false;
		}
		
		double highDeltaSum = 0;
		for(int index = 0; index < config.getSpan() - 1; index++){
			final Bar indexBar = bars.get(index);
			final Bar indexPrevious = bars.get(index + 1);
			highDeltaSum += Math.abs(indexPrevious.getHigh() - indexBar.getHigh());
		}
		if(bar.getHigh() - previous.getHigh() <= highDeltaSum/config.getSpan()){
			return false;
		}
		
		
		/*
		

		if (bar.getClose() < bar.getOpen()) {
			return false;
		}
		if (bar.getClose() < previous.getClose()) {
			return false;
		}

		final SimpleMovingAverage.Config smaConfig = sma.buildDefaultConfig();
		smaConfig.setSpan(config.getSpan());
		
		smaConfig.setBarValue(BarValue.SPREAD);
		final Double averageSpread = sma.compute(Collections.emptyList(), bars, smaConfig);
		if (BarValue.SPREAD.get(bar) < averageSpread * config.getMultiple()) {
			return false;
		}

		smaConfig.setBarValue(BarValue.VOLUME);
		final Double averageVolume = sma.compute(Collections.emptyList(), bars, smaConfig);
		if (bar.getVolume() < averageVolume * config.getMultiple()) {
			return false;
		}*/

		return true;
	}

}

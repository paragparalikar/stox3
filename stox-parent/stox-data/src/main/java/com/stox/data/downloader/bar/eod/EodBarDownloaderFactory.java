package com.stox.data.downloader.bar.eod;

import com.stox.core.model.Exchange;

public class EodBarDownloaderFactory {

	public EodBarDownloader create(Exchange exchange){
		switch(exchange){
		case NSE :
			return new NseEodBarDownloader();
		default :
			throw new UnsupportedOperationException("Exchange \""+exchange.getName()+"\" is not suppoerted yet");
		}
	}
	
}

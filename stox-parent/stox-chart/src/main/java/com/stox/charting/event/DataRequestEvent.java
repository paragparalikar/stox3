package com.stox.charting.event;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.core.model.BarSpan;

import javafx.event.Event;
import javafx.event.EventType;

public class DataRequestEvent extends Event {
	private static final long serialVersionUID = 1L;
	public static final EventType<DataRequestEvent> TYPE = new EventType<DataRequestEvent>("BarDataRequestEvent");

	private final long to;
	private final BarSpan barSpan;
	private final XAxis xAxis;

	public DataRequestEvent(long to, BarSpan barSpan, XAxis xAxis) {
		super(TYPE);
		this.to = to;
		this.barSpan = barSpan;
		this.xAxis = xAxis;
	}

	public long getTo() {
		return to;
	}

	public BarSpan getBarSpan() {
		return barSpan;
	}

	public XAxis getXAxis() {
		return xAxis;
	}
	
}

package com.stox.workbench.modal;

import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;

import javafx.scene.Scene;

public class ConfirmationModal extends Modal {

	private final Runnable action;
	private final FluentButton cancelButton = new FluentButton().cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton().classes("danger").defaultButton(true).onAction(e -> action());
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");

	public ConfirmationModal(String message, String actionButtonText, Runnable action) {
		this(null, message, actionButtonText, null, action);
	}

	public ConfirmationModal(String title, String message, String actionButtonText, String cancelButtonText,
			Runnable action) {
		super(null == title ? "Please Confirm" : title, new FluentLabel(message));
		this.action = action;
		getNode().getStyleClass().add("danger");
		getContainer().setBottom(buttonBar);
		getTitleBar().getTitleLabel().setGraphic(new FluentLabel(Icon.EXCLAMATION_TRIANGLE).focusTraversable(false).classes("danger","icon"));
		actionButton.setText(null == actionButtonText ? "Confirm" : actionButtonText);
		cancelButton.setText(null == cancelButtonText ? "Cancel" : cancelButtonText);
	}

	@Override
	protected void resizeRelocate() {
		final double height = 150;
		final double width = 500;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}

	public void cancel() {
		hide();
	}

	public void action() {
		action.run();
		hide();
	}

}

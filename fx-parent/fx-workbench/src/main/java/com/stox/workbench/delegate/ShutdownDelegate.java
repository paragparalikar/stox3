package com.stox.workbench.delegate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import com.stox.workbench.layout.LayoutManager;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ShutdownDelegate extends AbstractLifecycleDelegate {

	public void shutdown(String path, Stage stage, LayoutManager layoutManager) {
		final List<String> lines = new ArrayList<>();
		lines.add(format("workbench", stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
		layoutManager.getModuleViews().forEach(view -> {
			final BorderPane pane = view.getNode();
			final String text = format(view.getId(), pane.getLayoutX(), pane.getLayoutY(), pane.getWidth(),
					pane.getHeight());
			lines.add(text);
		});
		try {
			Files.write(Paths.get(path), lines, 
					StandardOpenOption.CREATE, 
					StandardOpenOption.TRUNCATE_EXISTING,
					StandardOpenOption.WRITE);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}

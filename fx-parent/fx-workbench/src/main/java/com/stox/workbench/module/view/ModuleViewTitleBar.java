package com.stox.workbench.module.view;

import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;
import com.stox.widget.Ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Node;

public class ModuleViewTitleBar {

	private final FluentLabel title = new FluentLabel().classes("title-text").focusTraversable(false);
	private final FluentHBox left = new FluentHBox(title);
	private final FluentButton closeButton = new FluentButton(Icon.TIMES).tooltip(Ui.tooltip("Close")).classes("primary","icon","hover-danger");
	private final FluentHBox right = new FluentHBox(closeButton);
	private final FluentHBox titleContainer = new FluentHBox(left, new Spacer(), right);
	private final FluentVBox node = new FluentVBox(titleContainer).classes("primary-background","title-bar");
	
	public FluentVBox getNode() {
		return node;
	}
	
	public ModuleViewTitleBar titleIcon(Node node){
		title.setGraphic(node);
		if(null != node){
			node.setFocusTraversable(false);
		}
		return this;
	}
	
	public ModuleViewTitleBar titleText(String text){
		title.setText(text);
		return this;
	}
	
	public ModuleViewTitleBar closeEventHandler(EventHandler<ActionEvent> eventHandler){
		closeButton.onAction(eventHandler);
		return this;
	}
	
	public void add(Side side, Node node){
		switch(side){
		case BOTTOM:
			this.node.getChildren().add(node);
			break;
		case LEFT:
			left.getChildren().add(0, node);
			break;
		case RIGHT:
			right.getChildren().add(0, node);
			break;
		case TOP:
			this.node.getChildren().add(0, node);
			break;
		default:
			break;
		}
	}
	
	public void remove(Side side, Node node){
		switch(side){
		case BOTTOM:
			this.node.getChildren().remove(node);
			break;
		case LEFT:
			left.getChildren().remove(node);
			break;
		case RIGHT:
			right.getChildren().remove(node);
			break;
		case TOP:
			this.node.getChildren().remove(node);
			break;
		default:
			break;
		}
	}
	
	public void add(boolean flag, Side side, Node node){
		if(flag){
			add(side, node);
		}else{
			remove(side, node);
		}
	}
	
}

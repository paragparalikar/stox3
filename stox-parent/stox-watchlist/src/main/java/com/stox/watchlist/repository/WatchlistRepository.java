package com.stox.watchlist.repository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.util.Constant;
import com.stox.util.FileUtil;
import com.stox.watchlist.model.Watchlist;

import lombok.SneakyThrows;

public class WatchlistRepository {
	
	interface Wrapper{
		WatchlistRepository INSTANCE = new WatchlistRepository();
	}
	
	public static WatchlistRepository getInstance(){
		return Wrapper.INSTANCE;
	}
	
	private WatchlistRepository() {
	}
	
	private final WatchlistEntryRepository entryRepository = WatchlistEntryRepository.getInstance();
	
	private Path getPath(){
		return FileUtil.safeGet(Constant.HOME + "watchlist" + File.separator + "watchlists.csv");
	}
	
	private String format(Watchlist watchlist){
		return watchlist.getId() + "," + watchlist.getName();
	}
	
	private Watchlist parse(String text){
		final String[] tokens = text.split(",");
		return new Watchlist(tokens[0], tokens[1]);
	}
	
	@SneakyThrows
	public List<Watchlist> find(){
		return Files.readAllLines(getPath()).stream().map(this::parse).collect(Collectors.toList());
	}
	
	@SneakyThrows
	public Watchlist save(Watchlist watchlist){
		if(find().contains(watchlist)){
			throw new IllegalArgumentException("Watchlist with name \""+watchlist.getName()+"\" already exists");
		}
		final String text = format(watchlist);
		Files.write(getPath(), Collections.singleton(text), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
		return watchlist;
	}
	
	@SneakyThrows
	public Watchlist update(Watchlist watchlist){
		final List<Watchlist> watchlists = find();
		final Watchlist managed = watchlists.stream().filter(w -> w.getId().equals(watchlist.getId())).findFirst().orElse(null);
		if(null == managed){
			throw new IllegalArgumentException("Watchlist with id \""+watchlist.getId()+"\" does not exist");
		}
		watchlists.remove(managed);
		if(watchlists.contains(watchlist)){
			throw new IllegalArgumentException("Watchlist with name \""+watchlist.getName()+"\" already exists");
		}
		watchlists.add(watchlist);
		final List<String> lines = watchlists.stream().map(this::format).collect(Collectors.toList());
		Files.write(getPath(), lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		return watchlist;
	}
	
	@SneakyThrows
	public Watchlist delete(Watchlist watchlist){
		final List<Watchlist> watchlists = find();
		if(!watchlists.contains(watchlist)){
			throw new IllegalArgumentException("Watchlist \""+watchlist.getName()+"\" does not exist");
		}
		watchlists.remove(watchlist);
		final List<String> lines = watchlists.stream().map(this::format).collect(Collectors.toList());
		Files.write(getPath(), lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		entryRepository.delete(watchlist.getId());
		return watchlist;
	}

}

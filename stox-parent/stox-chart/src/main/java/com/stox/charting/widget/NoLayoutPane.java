package com.stox.charting.widget;

import javafx.scene.layout.Pane;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false, of = { "mutex" })
public class NoLayoutPane extends Pane {

	private final Object mutex = new Object();

	@Override
	protected void layoutChildren() {

	}

}

package com.stox.screen;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class ScreenFactory {

	private static final List<Screen<?>> ALL = new ArrayList<>();
	static {
		ServiceLoader.load(Screen.class).forEach(screen -> ALL.add(screen));
	}

	public static List<Screen<?>> find() {
		return ALL;
	}

	@SuppressWarnings("unchecked")
	public static <T> Screen<T> get(final Class<? extends Screen<T>> clazz) {
		return (Screen<T>) ALL.stream().filter(screen -> clazz.isAssignableFrom(screen.getClass())).findFirst()
				.orElse(null);
	}

}

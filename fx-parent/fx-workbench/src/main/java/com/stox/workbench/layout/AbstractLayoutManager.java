package com.stox.workbench.layout;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.stox.workbench.module.view.ModuleView;
import com.stox.workbench.module.view.ModuleViewFactory;

public abstract class AbstractLayoutManager implements LayoutManager {

	private final ModuleViewFactory moduleViewFactory;
	private final Map<String, ModuleView> moduleViews = new HashMap<>();
	
	public AbstractLayoutManager(ModuleViewFactory moduleViewFactory) {
		this.moduleViewFactory = moduleViewFactory;
	}
	
	@Override
	public ModuleView show(String id){
		final ModuleView view = moduleViewFactory.create(id);
		show(view);
		return view;
	}
	
	@Override
	public void show(ModuleView view) {
		moduleViews.put(view.getId(), view);
	}
	
	@Override
	public boolean isShowing(String id){
		return null != moduleViews.get(id);
	}
	
	@Override
	public ModuleView getShowingModuleView(String id){
		return moduleViews.get(id);
	}

	@Override
	public void hide(ModuleView view) {
		moduleViews.remove(view.getId());
	}

	@Override
	public Collection<ModuleView> getModuleViews() {
		return Collections.unmodifiableCollection(moduleViews.values());
	}
	
	protected ModuleViewFactory getModuleViewFactory() {
		return moduleViewFactory;
	}
}

package com.stox.charting.screen;

import com.stox.charting.ChartingView;
import com.stox.charting.tools.ChartingToolBox;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.widget.Icon;
import com.stox.widget.Ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;

public class ScreenToolBox extends FluentButton implements ChartingToolBox, EventHandler<ActionEvent> {

	private ChartingView chartingView;
	
	public ScreenToolBox() {
		text(Icon.FILTER).onAction(this).classes("primary","icon","small").tooltip(Ui.tooltip("Screens"));
	}
	
	@Override
	public Node getNode() {
		return this;
	}

	@Override
	public void attach(ChartingView chartingView) {
		this.chartingView = chartingView;
	}

	@Override
	public void handle(ActionEvent event) {
		final ScreenChooserModal modal = new ScreenChooserModal(chartingView);
		modal.show();
	}

}

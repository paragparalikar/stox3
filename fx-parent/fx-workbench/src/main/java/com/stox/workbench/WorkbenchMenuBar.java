package com.stox.workbench;

import javafx.scene.control.MenuBar;

public class WorkbenchMenuBar extends MenuBar {

	public WorkbenchMenuBar() {
		getStyleClass().addAll("primary", "workbench-menu-bar");
		
	}

}

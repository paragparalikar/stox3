package com.stox.list.ui.component;

import com.stox.auto.AutoUiBuilder;
import com.stox.auto.AutoVBox;
import com.stox.core.model.BarSpan;
import com.stox.list.ui.RankerModuleView;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;

import javafx.geometry.Pos;

public class RankerConfigView {

	private AutoVBox autoVBox;
	private final RankerModuleView rankerModuleView;
	private final FluentLabel barSpanLabel = new FluentLabel("Bar Span");
	private final FluentComboBox<BarSpan> barSpanComboBox = new FluentComboBox<BarSpan>().items(BarSpan.values()).classes("primary", "inverted", "auto-widget");
	private final FluentHBox barSpanContainer = new FluentHBox(barSpanLabel, new FluentHBox(barSpanComboBox).alignment(Pos.CENTER_RIGHT).fullWidth()).fullWidth().classes("spaced", "padded");
	
	private final FluentButton previousButton = new FluentButton(Icon.LONG_ARROW_LEFT).classes("primary", "icon").onAction(e -> previous());
	private final FluentButton nextButton = new FluentButton(Icon.LONG_ARROW_RIGHT).classes("primary", "icon").onAction(e -> next());
	private final FluentHBox buttonBar = new FluentHBox(previousButton, new Spacer(),nextButton).classes("padded");
	
	public RankerConfigView(RankerModuleView rankerModuleView) {
		this.rankerModuleView = rankerModuleView;
	}
	
	public void start(){
		barSpanComboBox.getSelectionModel().select(rankerModuleView.getState().getBarSpan());
		final AutoUiBuilder builder = new AutoUiBuilder();
		autoVBox = builder.build(rankerModuleView.getState().getConfig());
		autoVBox.populateView();
		final FluentVBox container = new FluentVBox(barSpanContainer, autoVBox);
		rankerModuleView.content(container).bottom(buttonBar);
	}
	
	private void stop(){
		rankerModuleView.content(null).bottom(null);
	}
	
	private void next(){
		stop();
		autoVBox.populateModel();
		final ScripSelectionView scripSelectionView = new ScripSelectionView(rankerModuleView);
		scripSelectionView.start();
	}
	
	private void previous(){
		stop();
		final RankerSelectionView rankerSelectionView = new RankerSelectionView(rankerModuleView);
		rankerSelectionView.start();
	}
	
}

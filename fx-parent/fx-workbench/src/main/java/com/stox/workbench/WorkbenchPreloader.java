package com.stox.workbench;


import javafx.animation.FadeTransition;
import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class WorkbenchPreloader extends Preloader {

	private Stage stage;
	
	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.setHeight(400);
		stage.setWidth(600);
		stage.setAlwaysOnTop(true);
		
		final BorderPane root = new BorderPane();
		root.setStyle("-fx-background-color: #292929;");
		final Scene scene = new Scene(root, 600, 400, Color.TRANSPARENT);
		stage.setScene(scene);
		stage.show();
		stage.centerOnScreen();
	}
	
	
	@Override
	public void handleApplicationNotification(PreloaderNotification info) {
		super.handleApplicationNotification(info);
		final FadeTransition transition = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
		transition.setFromValue(1);
		transition.setToValue(0);
		transition.setOnFinished(e -> stage.hide());
		transition.play();
	}
	
}

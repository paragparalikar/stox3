package com.stox.charting.indicator;

import java.util.Collections;
import java.util.List;

import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.BarUnit;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.GroupUnitParent;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.indicator.Decision;
import com.stox.indicator.Decision.Config;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.widget.parent.Parent;

import javafx.scene.Group;
import javafx.scene.Node;
import lombok.experimental.Delegate;

public class ChartDecision extends AbstractChartIndicator<Config, Double, Node> {

	@Delegate
	private final Indicator<Decision.Config, Double> indicator = IndicatorFactory.get(Decision.class);

	@Override
	public UnitParent<Node> buildParent(Group group) {
		return new GroupUnitParent(group);
	}

	@Override
	public boolean isGroupable() {
		return false;
	}

	@Override
	public Underlay getUnderlay(Config config) {
		return Underlay.NONE;
	}

	@Override
	public double getMin(Double value) {
		return null == value ? Double.MAX_VALUE : value;
	}

	@Override
	public double getMax(Double value) {
		return null == value ? Double.MIN_VALUE : value;
	}

	@Override
	public String getName() {
		return "Decision";
	}

	@Override
	public Unit<Double> buildUnit(Parent<Node> parent) {
		return new BarUnit(parent);
	}

	@Override
	public List<ChartAddIn<Double>> buildAddIns(Config config, UnitParent<Node> parent) {
		return Collections.emptyList();
	}

}

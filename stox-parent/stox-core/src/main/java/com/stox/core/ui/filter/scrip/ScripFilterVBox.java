package com.stox.core.ui.filter.scrip;

import com.stox.core.model.Scrip;

public abstract class ScripFilterVBox<T> extends FilterVBox<T, Scrip> {

	public ScripFilterVBox(String title) {
		super(title);
	}

}

package com.stox.ui.fx.fluent.scene;

import com.stox.ui.fx.fluent.scene.control.IFluentStyleable;

import javafx.css.PseudoClass;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public interface IFluentNode<T extends Node & IFluentNode<T>> extends IFluentStyleable<T>{

	default T disable(boolean value) {
		getThis().setDisable(value);
		return getThis();
	}
	
	default T managed(boolean value){
		getThis().setManaged(value);
		return getThis();
	}
	
	default T layoutX(double value){
		getThis().setLayoutX(value);
		return getThis();
	}
	
	default T layoutY(double value){
		getThis().setLayoutY(value);
		return getThis();
	}

	default <V extends Event> T eventHandler(final EventType<V> eventType, final EventHandler<? super V> eventHandler) {
		getThis().addEventHandler(eventType, eventHandler);
		return getThis();
	}

	default <V extends Event> T eventFilter(final EventType<V> eventType, final EventHandler<? super V> eventFilter) {
		getThis().addEventFilter(eventType, eventFilter);
		return getThis();
	}

	default T pseudoClassState(PseudoClass pseudoClass, boolean active) {
		getThis().pseudoClassStateChanged(pseudoClass, active);
		return getThis();
	}
	
	default T userData(Object data) {
		getThis().setUserData(data);
		return getThis();
	}

	default T visible(boolean value) {
		getThis().setVisible(value);
		return getThis();
	}

	default T cursor(Cursor cursor) {
		getThis().setCursor(cursor);
		return getThis();
	}

	default T opacity(double value) {
		getThis().setOpacity(value);
		return getThis();
	}

	default T style(String style) {
		getThis().setStyle(style);
		return getThis();
	}

	default T classes(String... classes) {
		getThis().getStyleClass().addAll(classes);
		return getThis();
	}
	
	default T classes(boolean flag, String...classes){
		if(flag){
			getThis().getStyleClass().addAll(classes);
		}else{
			getThis().getStyleClass().removeAll(classes);
		}
		return getThis();
	}
	

	default T fullHeight() {
		VBox.setVgrow(getThis(), Priority.ALWAYS);
		return getThis();
	}

	default T fullWidth() {
		HBox.setHgrow(getThis(), Priority.ALWAYS);
		return getThis();
	}

	default T fullArea() {
		fullHeight();
		return fullWidth();
	}
		
	default T focusTraversable(boolean value){
		getThis().setFocusTraversable(value);
		return getThis();
	}

}

package com.stox.charting.indicator;

import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;

public class ChartIndicatorWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		workbench.getStage().getScene().getStylesheets().add("style/charting-indicator.css");
	}
	
}

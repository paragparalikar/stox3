package com.stox.charting.plot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.plot.info.PlotInfoPane;
import com.stox.charting.plot.info.SimplePlotInfoPane;
import com.stox.charting.unit.Unit;
import com.stox.ui.fx.fluent.scene.layout.FluentGroup;
import com.stox.util.Attachable;
import com.stox.util.MathUtil;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public abstract class Plot<T> extends FluentGroup {
	
	private double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
	private final Configuration configuration;
	private final List<T> models = new ArrayList<>();
	private final List<Unit<T>> units = new ArrayList<>();
	private final PlotInfoPane plotInfoPane = buildPlotInfoPane();
	private final Supplier<Unit<T>> unitSupplier = () -> buildUnit();
	private final ObjectProperty<Color> colorProperty = new SimpleObjectProperty<>(Color.BLUE);
	
	public Plot(final Configuration configuration) {
		this.configuration = configuration;
		managed(false).autoSizeChildren(false).classes("plot");
	}

	public abstract Unit<T> buildUnit();

	public abstract double getMin(final T model);

	public abstract double getMax(final T model);
	
	public abstract void showIndexInfo(final int index);
	
	protected PlotInfoPane buildPlotInfoPane() {
		return new SimplePlotInfoPane();
	}
	
	public void updateValueBounds(final int start, final int end) {
		final List<T> models = getModels();
		synchronized(models){
			min = Double.MAX_VALUE;
			max = Double.MIN_VALUE;
			final int clippedEndIndex = MathUtil.clip(0, end, models.size());
			final int clippedStartIndex = MathUtil.clip(0, start, models.size() - 1);
			for (int index = clippedStartIndex; index < clippedEndIndex; index++) {
				final T model = models.get(index);
				min = Math.min(min, getMin(model));
				max = Math.max(max, getMax(model));
			}
		}
	}

	protected synchronized void ensureUnitsSize(final int startIndex, final int endIndex) {
		final int delta = Math.max(0, endIndex) - Math.max(0, startIndex) + 1;
		Attachable.ensureSize(units, delta, unitSupplier);
	}

	public void reset() {
		synchronized (models) {
			models.clear();
			showIndexInfo(-1);
		}
	}

	public void layoutChartChildren(final XAxis xAxis, final YAxis yAxis) {
		synchronized (models) {
			if (!models.isEmpty()) {
				int unitIndex = 0;
				final int end = xAxis.getClippedEndIndex();
				final int start = xAxis.getClippedStartIndex();
				ensureUnitsSize(start, end);
				for (int index = start; index <= end; index++) {
					final T model = models.get(index);
					if (null != model) {
						final Unit<T> unit = units.get(unitIndex++);
						final T previousModel = index < models.size() - 1 ? models.get(index + 1) : null;
						layoutUnit(unit, index, model, previousModel, xAxis, yAxis);
					}
				}
			}
		}
	}

	protected void layoutUnit(final Unit<T> unit, final int index, final T model, final T previousModel,
			final XAxis xAxis, final YAxis yAxis) {
		unit.update(index, model, previousModel, xAxis, yAxis);
	}
	
	public Underlay getUnderlay() {
		return Underlay.NONE;
	}
	
	
	
	public void setColor(final Color color) {
		colorProperty.set(color);
	}

	public Color getColor() {
		return colorProperty.get();
	}

	public ObjectProperty<Color> colorProperty() {
		return colorProperty;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
	
	public double getMin(){
		return min;
	}
	
	public double getMax(){
		return max;
	}
	
	public List<T> getModels() {
		return models;
	}
	
	public List<Unit<T>> getUnits() {
		return units;
	}
	
	public PlotInfoPane getPlotInfoPane() {
		return plotInfoPane;
	}
	
	
}

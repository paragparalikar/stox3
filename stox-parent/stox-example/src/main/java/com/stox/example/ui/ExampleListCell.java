package com.stox.example.ui;

import java.util.function.Supplier;

import com.stox.event.Event;
import com.stox.example.model.Example;
import com.stox.example.model.ExampleGroup;
import com.stox.example.repository.ExampleRepository;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.widget.Icon;

import javafx.scene.control.ListCell;

public class ExampleListCell extends ListCell<Example> {

	private final Supplier<ExampleGroup> groupSupplier;
	private final ExampleRepository exampleRepository = ExampleRepository.getInstance(); 
	private final FluentButton graphic = new FluentButton(Icon.TRASH).classes("icon", "danger", "inverted").onAction(e -> delete());
	
	public ExampleListCell(Supplier<ExampleGroup> watchlistSupplier) {
		this.groupSupplier = watchlistSupplier;
	}
	
	@Override
	protected void updateItem(Example item, boolean empty) {
		super.updateItem(item, empty);
		if(null == item || empty){
			setGraphic(null);
			setText(null);
		}else{
			setGraphic(graphic);
			setText(item.getScrip().getName());
		}
	}

	private void delete() {
		final ExampleGroup group = groupSupplier.get();
		final Example example = exampleRepository.delete(group.getId(), getItem());
		Event.fire(Example.DELETED, example);
	}
	
}

package com.stox.example.model;

import java.util.Comparator;
import java.util.Objects;

import lombok.NonNull;
import lombok.Value;

@Value
public class ExampleGroup implements Comparable<ExampleGroup>{
	public static final String CREATED = "EXAMPLE_GROUP_CREATED";
	public static final String MODIFIED = "EXAMPLE_GROUP_MODIFIED";
	public static final String DELETED = "EXAMPLE_GROUP_DELETED";
	public static final Comparator<ExampleGroup> COMPARATOR = (one, two) -> one.name.compareToIgnoreCase(two.name);  
	
	@NonNull
	private String id;
	
	@NonNull
	private String name;
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(ExampleGroup other) {
		return Objects.compare(this, other,COMPARATOR);
	}

}

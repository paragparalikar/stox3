package com.stox.list.ranker;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.Scrip;
import com.stox.list.ranker.Volatile.Config;

import lombok.Getter;
import lombok.Setter;

public class Volatile implements Ranker<Config> {

	@Getter
	@Setter
	public static class Config {
		private int span = 10;
	}

	@Override
	public String getName() {
		return "Volatile";
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan();
	}

	@Override
	public RankerResult rank(Scrip scrip, List<Bar> bars, Config config) {
		return new RankerResult(scrip, compute(config.getSpan(), bars));
	}

	private Double compute(int span, List<Bar> bars) {
		double sum = 0;
		for (int i = 0; i < span; i++) {
			sum += bars.get(i).getClose();
		}
		double deviationSum = 0;
		final double average = sum / span;
		for (int i = 0; i < span; i++) {
			double deviation = average - bars.get(i).getClose();
			deviation = deviation*100/average;
			deviationSum += Math.pow(deviation, 2);
		}
		return Math.sqrt(deviationSum / span);
	}

}

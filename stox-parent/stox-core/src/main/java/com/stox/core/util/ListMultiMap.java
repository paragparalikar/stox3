package com.stox.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

import lombok.NonNull;

public class ListMultiMap<K, V> extends HashMap<K, List<V>> {
	private static final long serialVersionUID = 2094755998485795996L;

	private final Supplier<List<V>> supplier;

	public ListMultiMap() {
		this(() -> new ArrayList<V>());
	}

	public ListMultiMap(@NonNull Supplier<List<V>> supplier) {
		this.supplier = supplier;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<V> get(Object key) {
		List<V> value = super.get(key);
		if (null == value) {
			value = supplier.get();
			super.put((K) key, value);
		}
		return value;
	}

	@Override
	public List<V> put(K key, List<V> value) {
		final List<V> val = get(key);
		val.addAll(value);
		return val;
	}

	public void put(K key, V value) {
		final List<V> val = get(key);
		val.add(value);
	}

}

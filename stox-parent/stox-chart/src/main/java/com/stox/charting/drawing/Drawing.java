package com.stox.charting.drawing;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;

import javafx.scene.Node;

public interface Drawing extends Updatable {

	int getDrawingId();
	
	void layoutChartChildren(final XAxis xAxis, final YAxis yAxis);
	
	String format();

	void parse(final String text);

	Node getNode();
}

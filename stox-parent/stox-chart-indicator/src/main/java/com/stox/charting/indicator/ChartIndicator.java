package com.stox.charting.indicator;

import java.util.List;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.indicator.Indicator;
import com.stox.widget.parent.Parent;

import javafx.scene.Group;

public interface ChartIndicator<T, V, P> extends Indicator<T, V> {
	
	String getName();

	UnitParent<P> buildParent(final Group group);

	Unit<V> buildUnit(final Parent<P> parent);

	boolean isGroupable();

	Underlay getUnderlay(final T config);

	List<ChartAddIn<V>> buildAddIns(final T config, final UnitParent<P> parent);

	double getMin(V value);

	double getMax(V value);

	void layoutChartChildren(XAxis xAxis, YAxis yAxis, List<V> models, List<Unit<V>> units, UnitParent<P> parent,
			IndicatorPlot<T, V, P> plot);

}

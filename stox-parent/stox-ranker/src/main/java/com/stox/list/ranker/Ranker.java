package com.stox.list.ranker;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.Scrip;

public interface Ranker<T> {
	
	String getName();

	T buildDefaultConfig();

	int getMinBarCount(final T config);

	RankerResult rank(Scrip scrip, final List<Bar> bars, final T config);

}

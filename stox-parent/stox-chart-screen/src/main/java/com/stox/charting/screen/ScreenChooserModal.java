package com.stox.charting.screen;

import com.stox.charting.ChartingView;
import com.stox.screen.Screen;
import com.stox.screen.ScreenFactory;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.workbench.modal.Modal;

import javafx.scene.Scene;
import javafx.scene.control.ListCell;

public class ScreenChooserModal extends Modal {

	private final ChartingView chartingView;
	private final FluentListView<Screen<?>> listView = new FluentListView<>();
	private final FluentButton cancelButton = new FluentButton("Cancel").cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton("Add").classes("primary").defaultButton(true).onAction(e -> action());
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");

	public ScreenChooserModal(ChartingView chartingView) {
		super("Screens");
		this.chartingView = chartingView;
		getContainer().setCenter(listView);
		getContainer().setBottom(buttonBar);
		getNode().getStyleClass().add("primary");
		getTitleBar().getTitleLabel().setGraphic(new FluentLabel(Icon.FILTER).focusTraversable(false).classes("primary","icon"));
		listView.getItems().addAll(ScreenFactory.find());
		listView.cellFactory(e -> new ScreenListCell());
	}
	
	@Override
	protected void resizeRelocate() {
		final double height = 500;
		final double width = 300;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}
	
	private void cancel(){
		hide();
	}
	
	private void action(){
		final Screen<?> screen = listView.selectionModel().getSelectedItem();
		if(null != screen){
			final ScreenPlot<?> plot = new ScreenPlot<>(screen, chartingView.getConfiguration());
			chartingView.load(plot);
			chartingView.add(plot);
			hide();
		}
	}
	
}

class ScreenListCell extends ListCell<Screen<?>>{
	
	private FluentLabel graphic;
	
	@Override
	protected void updateItem(Screen<?> item, boolean empty) {
		super.updateItem(item, empty);
		if(null == item || empty){
			setText(null);
			setGraphic(null);
		}else{
			setText(item.getName());
			if(null == graphic){
				graphic = new FluentLabel().focusTraversable(false).classes("icon", "tiny" , "inverted");
			}
			switch(item.getScreenType()){
			case BEARISH:
				graphic.classes("danger").getStyleClass().removeAll("primary", "success");
				graphic.setText(Icon.ARROW_DOWN);
				break;
			case BULLISH:
				graphic.classes("success").getStyleClass().removeAll("primary", "danger");
				graphic.setText(Icon.ARROW_UP);
				break;
			case NEUTRAL:
				graphic.classes("primary").getStyleClass().removeAll("success", "danger");
				graphic.setText(Icon.ARROW_UP);
			default:
				break;
			}
			setGraphic(graphic);
		}
	}
	
}

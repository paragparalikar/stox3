package com.stox.watchlist.ui.menu;

import com.stox.core.model.intf.HasBarSpan;
import com.stox.core.model.intf.HasScrip;
import com.stox.widget.menu.MenuProvider;

import javafx.scene.control.ContextMenu;

public class AddToWatchlistMenuProvider implements MenuProvider {

	@Override
	public void accept(ContextMenu contextMenu, Object target) {
		if(target instanceof HasScrip && target instanceof HasBarSpan){
			contextMenu.getItems().add(new AddToWatchlistMenu(target));
		}
	}

}

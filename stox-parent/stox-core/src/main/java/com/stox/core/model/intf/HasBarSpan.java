package com.stox.core.model.intf;

import com.stox.core.model.BarSpan;

public interface HasBarSpan {

	public BarSpan getBarSpan();

}

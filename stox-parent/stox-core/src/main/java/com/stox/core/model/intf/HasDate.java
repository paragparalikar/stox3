package com.stox.core.model.intf;

import java.util.Date;

public interface HasDate {

	Date getDate();
	
}

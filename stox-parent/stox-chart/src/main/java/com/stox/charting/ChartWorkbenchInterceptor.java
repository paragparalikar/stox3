package com.stox.charting;

import com.stox.widget.Icon;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class ChartWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		workbench.getStage().getScene().getStylesheets().add("style/chart.css");
		final ModuleViewRegistration registration = new ModuleViewRegistration(ChartingView.ID, Icon.BAR_CHART, "Chart", true, () -> new ChartingView());
		workbench.register(registration);
	}

}

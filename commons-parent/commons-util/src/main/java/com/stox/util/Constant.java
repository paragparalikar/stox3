package com.stox.util;

import java.io.File;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Constant {

	public static final String HOME = System.getProperty("com.stox.path.home.dir", System.getProperty("user.home")+File.separator+".stox"+File.separator);
	public static final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
	public static final DateFormat dateFormatFull = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
	public static final NumberFormat currencyFormat = NumberFormat.getInstance();
	
	static {
		Constant.currencyFormat.setGroupingUsed(true);
		Constant.currencyFormat.setMaximumFractionDigits(2);
		Constant.currencyFormat.setMinimumFractionDigits(0);
	}
	
}

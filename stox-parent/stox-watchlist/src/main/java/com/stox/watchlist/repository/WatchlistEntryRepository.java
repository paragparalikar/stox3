package com.stox.watchlist.repository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.repository.ScripRepository;
import com.stox.util.Constant;
import com.stox.util.FileUtil;
import com.stox.watchlist.model.WatchlistEntry;

import lombok.SneakyThrows;

public class WatchlistEntryRepository {

	interface Wrapper{
		WatchlistEntryRepository INSTANCE = new WatchlistEntryRepository();
	}
	
	public static WatchlistEntryRepository getInstance(){
		return Wrapper.INSTANCE;
	}
	
	private final ScripRepository scripRepository = ScripRepository.getInstance();
	
	
	private WatchlistEntryRepository() {
	}
	
	private Path getPath(String watchlistId, BarSpan barSpan){
		return FileUtil.safeGet(Constant.HOME + "watchlist" + File.separator + watchlistId + "-" + barSpan.getShortName());
	}
	
	@SneakyThrows
	public WatchlistEntry save(final String watchlistId, final WatchlistEntry entry){
		final Path path = getPath(watchlistId, entry.getBarSpan());
		final String text = format(entry);
		if(!Files.readAllLines(path).contains(text)){
			Files.write(path, Collections.singleton(text), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
			return entry;
		}
		return null;
	}
	
	@SneakyThrows
	public List<WatchlistEntry> find(final String watchlistId, final BarSpan barSpan){
		return Files.readAllLines(getPath(watchlistId, barSpan))
				.stream().map(line -> parse(watchlistId, line, barSpan))
				.filter(watchlist -> null != watchlist)
				.collect(Collectors.toList());
	}
	
	@SneakyThrows
	public WatchlistEntry delete(final String watchlistId, final WatchlistEntry entry){
		final Path path = getPath(watchlistId, entry.getBarSpan());
		final List<String> lines = Files.readAllLines(path);
		final String text = format(entry);
		lines.remove(text);
		Files.write(path, lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		return entry;
	}
	
	@SneakyThrows
	public void delete(String watchlistId){
		for(BarSpan barSpan : BarSpan.values()){
			delete(watchlistId, barSpan);
		}
	}
	
	@SneakyThrows
	public void delete(String watchlistId, BarSpan barSpan){
		Files.deleteIfExists(getPath(watchlistId, barSpan));
	}
	
	private String format(final WatchlistEntry entry){
		return entry.getScrip().getIsin();
	}
	
	private WatchlistEntry parse(String watchlistId, String text, BarSpan barSpan){
		final Scrip scrip = scripRepository.find(text);
		return null == scrip ? null : new WatchlistEntry(watchlistId, scrip, barSpan);
	}
	
}

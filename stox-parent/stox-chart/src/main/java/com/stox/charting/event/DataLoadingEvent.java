package com.stox.charting.event;

import javafx.event.Event;
import javafx.event.EventType;

public class DataLoadingEvent extends Event{
	private static final long serialVersionUID = -2795898858635016113L;
	
	public static final EventType<DataLoadingEvent> TYPE = new EventType<>("BarDataLoadingEvent");

	private final boolean loading;

	public DataLoadingEvent(final boolean loading) {
		super(TYPE);
		this.loading = loading;
	}
	
	public boolean isLoading() {
		return loading;
	}

}

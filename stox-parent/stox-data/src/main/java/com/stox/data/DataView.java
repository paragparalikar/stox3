package com.stox.data;

import java.util.Calendar;
import java.util.concurrent.Executors;

import com.stox.core.model.Exchange;
import com.stox.core.repository.PropertyRepository;
import com.stox.data.downloader.scrip.ScripMasterDownloadAction;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.control.FluentProgressBar;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.workbench.module.view.ModuleView;

public class DataView extends ModuleView {
	public static final String ID = "data";

	private volatile DownloadContext context;
	private final FluentListView<StatusMessage> listView = new FluentListView<StatusMessage>().cellFactory(a -> new StatusMessageListCell());
	private final FluentComboBox<Exchange> exchangeComboBox = new FluentComboBox<Exchange>().items(Exchange.values()).select(Exchange.NSE).fullArea().classes("primary","inverted", "first");
	private final FluentButton actionButton = new FluentButton(Icon.DOWNLOAD).defaultButton(true).tooltip(Ui.tooltip("Download")).classes("primary", "inverted", "icon", "last").onAction(e -> action());
	private final FluentHBox controlsBox = new FluentHBox(exchangeComboBox, actionButton).fillHeight(true).fullWidth().classes("primary-background", "box", "padded");
	private final FluentProgressBar progressBar = new FluentProgressBar(0).fullWidth().classes("primary","success");
	private final FluentHBox infoBox = new FluentHBox(progressBar).fillHeight(true).fullWidth().classes("primary-background","padded");
	private final FluentBorderPane container = new FluentBorderPane().top(controlsBox).center(listView).bottom(infoBox);

	public DataView() {
		content(container);
		updateControls();
	}

	@Override
	public String getId() {
		return ID;
	}

	private void action() {
		if (isRunning()) {
			stop();
		} else {
			final Exchange exchange = exchangeComboBox.getValue();
			final Calendar lastDownloadDate = getLastDownloadDate(exchange);
			context = DownloadContext.builder()
					.lastDownloadDate(lastDownloadDate)
					.initialLastDownloadDate(lastDownloadDate.getTimeInMillis())
					.executorService(Executors.newWorkStealingPool())
					.terminationCallback(this::terminate)
					.messageCallback(this::message)
					.afterCallback(this::after)
					.exchange(exchange)
					.build();
			progressBar.setProgress(0);
			listView.getItems().clear();
			context.getExecutorService().submit(new ScripMasterDownloadAction(context));
		}
		updateControls();
	}

	private void message(StatusMessage message) {
		Ui.fx(() -> listView.getItems().add(message));
	}
	
	private void after(){
		final double start = (double) context.getInitialLastDownloadDate();
		final double end = (double) System.currentTimeMillis();
		final double current = (double) context.getLastDownloadDate().getTimeInMillis();
		final double value = (current - start)/(end - start);
		Ui.fx(() -> progressBar.setProgress(value));
	}
	
	private void terminate(){
		updateControls();
		if(context.isCancelled()){
			Ui.fx(() -> progressBar.setProgress(1));
		}
		context = null;
	}

	private boolean isRunning() {
		return null != context && !context.isCancelled();
	}

	private void updateControls() {
		Ui.fx(() -> {
			final boolean running = isRunning();
			exchangeComboBox.disable(running);
			actionButton.text(running ? Icon.STOP : Icon.DOWNLOAD).tooltip(Ui.tooltip(running ? "Stop" : "Download"))
					.classes(running, "danger").classes(!running, "primary");
			context = running ? context : null;
		});
	}

	private Calendar getLastDownloadDate(Exchange exchange) {
		final Calendar defaultDownloadDate = getDefaultLastDownloadDate();
		final PropertyRepository propertyRepository = PropertyRepository.getInstance();
		return propertyRepository.get(exchange.getKeyLastDownloadDate(), Calendar.class, defaultDownloadDate);
	}

	private Calendar getDefaultLastDownloadDate() {
		final Calendar defaultLastDownloadDate = Calendar.getInstance();
		defaultLastDownloadDate.add(Calendar.YEAR, -20);
		return defaultLastDownloadDate;
	}

	@Override
	public void stop() {
		super.stop();
		if (null != context) {
			context.setCancelled(true);
		}
	}

}


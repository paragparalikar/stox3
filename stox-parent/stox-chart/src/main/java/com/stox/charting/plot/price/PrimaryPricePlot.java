package com.stox.charting.plot.price;

import java.util.List;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.event.DataChangedEvent;
import com.stox.charting.plot.info.SimplePlotInfoPane;
import com.stox.charting.unit.PriceUnitFactory;
import com.stox.charting.unit.PriceUnitType;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.GroupUnitParent;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.charting.widget.BarInfoPanel;
import com.stox.core.model.Bar;

import lombok.NonNull;

public class PrimaryPricePlot extends PricePlot {

	private UnitParent<?> parent;
	private PriceUnitType priceUnitType;
	private final BarInfoPanel barInfoPanel;
	private final PriceUnitFactory priceUnitFactory;

	public PrimaryPricePlot(final BarInfoPanel barInfoPanel, final Configuration configuration) {
		super(configuration);
		priceUnitType = PriceUnitType.CANDLE;
		this.barInfoPanel = barInfoPanel;
		parent = new GroupUnitParent(this);
		priceUnitFactory = new PriceUnitFactory();
	}

	@Override
	protected SimplePlotInfoPane buildPlotInfoPane() {
		return null;
	}

	@Override
	public void showIndexInfo(int index) {
		final List<Bar> bars = getModels();
		barInfoPanel.set(index >= 0 && index < bars.size() ? getModels().get(index) : null);
	}

	@Override
	public Unit<Bar> buildUnit() {
		return priceUnitFactory.build(priceUnitType, parent, getConfiguration());
	}

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis) {
		parent.preLayoutChartChildren(xAxis, yAxis);
		super.layoutChartChildren(xAxis, yAxis);
		parent.postLayoutChartChildren(xAxis, yAxis);
	}

	public void setPriceUnitType(@NonNull final PriceUnitType priceUnitType) {
		if (!priceUnitType.equals(this.priceUnitType)) {
			this.priceUnitType = priceUnitType;
			updateParent();
			getUnits().forEach(Unit::detach);
			getUnits().clear();
			final List<Bar> bars = getModels();
			synchronized(bars){
				fireEvent(new DataChangedEvent(bars));
			}
		}
	}

	private void updateParent() {
		parent.unbindColorProperty();
		getChildren().remove(parent.getNode());
		parent = priceUnitFactory.buildParent(priceUnitType, parent, this);
		parent.bindColorProperty(colorProperty());
		if (this != parent.getNode()) {
			getChildren().add(parent.getNode());
		}
	}

	public PriceUnitType getPriceUnitType() {
		return priceUnitType;
	}

}

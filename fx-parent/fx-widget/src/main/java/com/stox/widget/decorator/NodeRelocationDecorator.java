package com.stox.widget.decorator;


import com.stox.util.Attachable;

import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class NodeRelocationDecorator implements Attachable, EventHandler<MouseEvent> {

	private double offsetX;
	private double offsetY;
	private final Node node;
	private boolean relocationMode;

	private Runnable startCallback;
	private Runnable moveCallback;
	private Runnable endCallback;
	
	public NodeRelocationDecorator(final Node node) {
		this.node = node;
	}
	
	public void setEndCallback(Runnable endCallback) {
		this.endCallback = endCallback;
	}
	
	public void setMoveCallback(Runnable moveCallback) {
		this.moveCallback = moveCallback;
	}
	
	public void setStartCallback(Runnable startCallback) {
		this.startCallback = startCallback;
	}

	@Override
	public void handle(MouseEvent event) {
		if (MouseButton.PRIMARY.equals(event.getButton())) {
			final double x = event.getScreenX();
			final double y = event.getScreenY();
			final EventType<? extends MouseEvent> type = event.getEventType();
			if (MouseEvent.MOUSE_PRESSED.equals(type)) {
				process(x, y, true, startCallback, event);
			} else if (relocationMode && MouseEvent.MOUSE_DRAGGED.equals(type)) {
				node.setLayoutX(node.getLayoutX() + x - offsetX);
				node.setLayoutY(node.getLayoutY() + y - offsetY);
				process(x, y, true, moveCallback, event);
			} else if (MouseEvent.MOUSE_RELEASED.equals(type)) {
				process(0, 0, false, endCallback, event);
			}
		}
	}

	private void process(final double offsetX, final double offsetY, final boolean relocationMode,
			final Runnable callback, final MouseEvent event) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.relocationMode = relocationMode;
		event.consume();
		if (null != callback) {
			callback.run();
		}
	}

	@Override
	public void attach() {
		node.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
		node.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
		node.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
	}

	@Override
	public void detach() {
		node.removeEventHandler(MouseEvent.MOUSE_PRESSED, this);
		node.removeEventHandler(MouseEvent.MOUSE_DRAGGED, this);
		node.removeEventHandler(MouseEvent.MOUSE_RELEASED, this);
	}

}

package com.stox.charting;

public interface ModeMouseHandler {

	void attach();
	
	void detach();
	
}

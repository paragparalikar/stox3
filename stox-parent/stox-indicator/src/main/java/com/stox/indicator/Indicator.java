package com.stox.indicator;

import java.util.List;

import com.stox.core.model.Bar;


public interface Indicator<T, V> {

	T buildDefaultConfig();

	V compute(final List<Double> values, final List<Bar> bars, final T config);

	List<V> computeAll(final List<Double> values, final List<Bar> bars, final T config);

}

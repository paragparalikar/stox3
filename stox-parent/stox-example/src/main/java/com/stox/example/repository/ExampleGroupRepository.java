package com.stox.example.repository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.example.model.ExampleGroup;
import com.stox.util.Constant;
import com.stox.util.FileUtil;

import lombok.SneakyThrows;

public class ExampleGroupRepository {
	
	interface Wrapper{
		ExampleGroupRepository INSTANCE = new ExampleGroupRepository();
	}
	
	public static ExampleGroupRepository getInstance(){
		return Wrapper.INSTANCE;
	}
	
	private ExampleGroupRepository() {
	}
	
	private final ExampleRepository exampleRepository = ExampleRepository.getInstance();
	
	private Path getPath(){
		return FileUtil.safeGet(Constant.HOME + "example" + File.separator + "example-groups.csv");
	}
	
	private String format(ExampleGroup group){
		return group.getId() + "," + group.getName();
	}
	
	private ExampleGroup parse(String text){
		final String[] tokens = text.split(",");
		return new ExampleGroup(tokens[0], tokens[1]);
	}
	
	@SneakyThrows
	public List<ExampleGroup> find(){
		return Files.readAllLines(getPath()).stream().map(this::parse).collect(Collectors.toList());
	}
	
	@SneakyThrows
	public ExampleGroup save(ExampleGroup group){
		if(find().contains(group)){
			throw new IllegalArgumentException("Example Group with name \""+group.getName()+"\" already exists");
		}
		final String text = format(group);
		Files.write(getPath(), Collections.singleton(text), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
		return group;
	}
	
	@SneakyThrows
	public ExampleGroup update(ExampleGroup group){
		final List<ExampleGroup> groups = find();
		final ExampleGroup managed = groups.stream().filter(w -> w.getId().equals(group.getId())).findFirst().orElse(null);
		if(null == managed){
			throw new IllegalArgumentException("Example Group with id \""+group.getId()+"\" does not exist");
		}
		groups.remove(managed);
		if(groups.contains(group)){
			throw new IllegalArgumentException("Example Group with name \""+group.getName()+"\" already exists");
		}
		groups.add(group);
		final List<String> lines = groups.stream().map(this::format).collect(Collectors.toList());
		Files.write(getPath(), lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		return group;
	}
	
	@SneakyThrows
	public ExampleGroup delete(ExampleGroup group){
		final List<ExampleGroup> groups = find();
		if(!groups.contains(group)){
			throw new IllegalArgumentException("Example Group \""+group.getName()+"\" does not exist");
		}
		groups.remove(group);
		final List<String> lines = groups.stream().map(this::format).collect(Collectors.toList());
		Files.write(getPath(), lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		exampleRepository.delete(group.getId());
		return group;
	}

}

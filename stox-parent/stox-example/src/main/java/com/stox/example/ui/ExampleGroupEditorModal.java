package com.stox.example.ui;

import java.util.UUID;

import com.stox.event.Event;
import com.stox.example.model.ExampleGroup;
import com.stox.example.repository.ExampleGroupRepository;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;
import com.stox.util.StringUtil;
import com.stox.widget.Icon;
import com.stox.widget.form.TextFormField;
import com.stox.workbench.modal.Modal;

import javafx.scene.Scene;

public class ExampleGroupEditorModal extends Modal {

	private final ExampleGroup group;
	private final TextFormField nameFormField = new TextFormField("Name");
	private final FluentVBox content = new FluentVBox(nameFormField).classes("padded");
	private final FluentButton cancelButton = new FluentButton("Cancel").cancelButton(true).onAction(e -> cancel());
	private final FluentButton actionButton = new FluentButton().classes("success").defaultButton(true).onAction(e -> action());
	private final FluentHBox buttonBar = new FluentHBox(cancelButton, actionButton).classes("button-bar");
	private final FluentLabel graphic = new FluentLabel(Icon.BOOKMARK).classes("icon", "primary");
	
	private final ExampleGroupRepository exampleGroupRepository = ExampleGroupRepository.getInstance();

	public ExampleGroupEditorModal(ExampleGroup group) {
		this.group = group;
		getContainer().center(content).bottom(buttonBar);
		getTitleBar().getTitleLabel().graphic(graphic);
		nameFormField.getTextField().getStyleClass().add("bordered");
	}
	
	@Override
	public void show() {
		populate();
		super.show();
		nameFormField.getTextField().requestFocus();
	}
	
	@Override
	protected void resizeRelocate() {
		final double height = 160;
		final double width = 500;
		final Scene scene = getNode().getScene();
		getNode().resizeRelocate((scene.getWidth() - width) / 2, (scene.getHeight() - height) / 2, width, height);
	}
	
	private void populate(){
		if(null == group){
			getTitleBar().getTitleLabel().setText("Create Example Group");
			nameFormField.getTextField().clear();
			actionButton.setText("Create");
		}else{
			getTitleBar().getTitleLabel().setText("Edit Example Group - " + group.getName());
			nameFormField.getTextField().setText(group.getName());
			actionButton.setText("Edit");
		}
	}

	private void action() {
		final String name = nameFormField.getTextField().getText();
		if(StringUtil.hasText(name)){
			try{
				final String id = null == this.group ? UUID.randomUUID().toString() : group.getId();
				final ExampleGroup group = exampleGroupRepository.save(new ExampleGroup(id, name));
				hide();
				Event.fire(null == this.group ? ExampleGroup.CREATED : ExampleGroup.MODIFIED, group);
			}catch(Exception e){
				nameFormField.setError(e.getMessage());
			}
		}else{
			nameFormField.setError("Please provide a name for Example Group");
		}
	}
	
	private void cancel() {
		hide();
	}

}

package com.stox.workbench;

public interface WorkbenchInterceptor {
	
	default void beforeStart(Workbench workbench) throws Exception{
	}

	default void afterStop(Workbench workbench)  throws Exception{
	}

}

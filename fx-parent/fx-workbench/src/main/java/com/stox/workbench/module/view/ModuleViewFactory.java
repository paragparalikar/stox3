package com.stox.workbench.module.view;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.workbench.event.ModuleViewRegistration;

public class ModuleViewFactory {

	private final Map<String, ModuleViewRegistration> registrations = new HashMap<>();

	public ModuleView create(String id) {
		final ModuleViewRegistration registration = registrations.get(id);
		final Supplier<ModuleView> supplier = registration.getSupplier();
		final ModuleView moduleView =  supplier.get();
		init(moduleView, registration);
		return moduleView;
	}
	
	private void init(ModuleView moduleView, ModuleViewRegistration registration){
		final ModuleViewTitleBar titleBar = moduleView.getTitleBar();
		titleBar.titleIcon(new FluentLabel(registration.getIcon()).classes("icon","primary"));
		titleBar.titleText(registration.getName());
	}

	public void register(ModuleViewRegistration registration) {
		Objects.requireNonNull(registration, "registration can not be null");
		Objects.requireNonNull(registration.getId(), "registration id can not be null");
		Objects.requireNonNull(registration.getName(), "view name can not be null");
		Objects.requireNonNull(registration.getSupplier(), "supplier can not be null");
		registrations.put(registration.getId(), registration);
	}

	public Collection<ModuleViewRegistration> getRegistrations(){
		return registrations.values();
	}
	
}

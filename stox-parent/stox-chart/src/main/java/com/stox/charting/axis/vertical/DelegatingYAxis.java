package com.stox.charting.axis.vertical;

public interface DelegatingYAxis extends YAxis {
	
	void setDelegate(YAxis yAxis);

}

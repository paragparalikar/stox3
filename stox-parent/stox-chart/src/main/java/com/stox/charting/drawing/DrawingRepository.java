package com.stox.charting.drawing;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.util.Constant;
import com.stox.util.StringUtil;

import lombok.SneakyThrows;

public class DrawingRepository {

	private interface Wrapper {
		DrawingRepository INSTANCE = new DrawingRepository();
	}

	public static DrawingRepository getInstance() {
		return Wrapper.INSTANCE;
	}

	private DrawingRepository() {

	}

	private String getPath(final String scripId) {
		return Constant.HOME + "drawing" + File.separator + scripId;
	}

	@SneakyThrows
	public void persist(final String scripId, final List<Drawing> drawings) {
		if (StringUtil.hasText(scripId)) {
			final String path = getPath(scripId);
			synchronized (path) {
				final Path filePath = Paths.get(path);
				Files.createDirectories(filePath.getParent());
				final BufferedWriter writer = Files.newBufferedWriter(filePath, StandardOpenOption.CREATE,
						StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
				for (final Drawing drawing : drawings) {
					writer.write(drawing.getDrawingId() + "#" + drawing.format());
					writer.newLine();
				}
				writer.flush();
				writer.close();
			}
		}
	}

	@SneakyThrows
	public List<Drawing> load(final String scripId) {
		final String path = getPath(scripId);
		synchronized (path) {
			final Path filePath = Paths.get(path);
			if (Files.exists(filePath)) {
				final DrawingFactory drawingFactory = new DrawingFactory();
				return Files.lines(filePath).map(text -> {
					final String[] tokens = text.split("#");
					final int id = Integer.parseInt(tokens[0]);
					final Drawing drawing = drawingFactory.build(id);
					drawing.parse(tokens[1]);
					return drawing;
				}).collect(Collectors.toList());
			}
		}
		return Collections.emptyList();
	}

}

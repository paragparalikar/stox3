package com.stox.charting.tools;

import java.util.ServiceLoader;

import com.stox.charting.ChartingView;

import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ChartingToolBar extends ToolBar {

	public ChartingToolBar(ChartingView chartingView) {
		HBox.setHgrow(this, Priority.ALWAYS);
		setMaxWidth(Double.MAX_VALUE);
		getStyleClass().add("primary");
		ServiceLoader.load(ChartingToolBox.class).forEach(box -> {
			box.attach(chartingView);
			getItems().add(box.getNode());
		});
	}
	
}

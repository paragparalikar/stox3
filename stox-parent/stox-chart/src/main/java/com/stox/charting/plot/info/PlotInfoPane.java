package com.stox.charting.plot.info;

import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public interface PlotInfoPane {

	HBox getNode();
	
	void setName(final String name);
	
	void bind(final ObjectProperty<Color> colorProperty);
	
	void addRemoveEventHandler(final EventHandler<ActionEvent> eventHandler);
	
	void addVisibilityEventHandler(final EventHandler<ActionEvent> eventHandler);

}

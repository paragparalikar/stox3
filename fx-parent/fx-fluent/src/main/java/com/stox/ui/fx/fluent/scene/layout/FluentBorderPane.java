package com.stox.ui.fx.fluent.scene.layout;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

public class FluentBorderPane extends BorderPane implements IFluentPane<FluentBorderPane> {

	public FluentBorderPane() {
		super();
	}

	public FluentBorderPane(Node center, Node top, Node right, Node bottom, Node left) {
		super(center, top, right, bottom, left);
	}

	public FluentBorderPane(Node center) {
		super(center);
	}

	public FluentBorderPane clear() {
		return center(null).top(null).right(null).bottom(null).left(null);
	}

	@Override
	public FluentBorderPane getThis() {
		return this;
	}

	public Node center() {
		return getCenter();
	}

	public FluentBorderPane center(Node value) {
		setCenter(value);
		return this;
	}

	public Node top() {
		return getTop();
	}

	public FluentBorderPane top(Node value) {
		setTop(value);
		return this;
	}

	public Node bottom() {
		return getBottom();
	}

	public FluentBorderPane bottom(Node value) {
		setBottom(value);
		return this;
	}

	public Node left() {
		return getLeft();
	}

	public FluentBorderPane left(Node value) {
		setLeft(value);
		return this;
	}

	public Node right() {
		return getRight();
	}

	public FluentBorderPane right(Node value) {
		setRight(value);
		return this;
	}

}

package com.stox.screener.ui.component;

import com.stox.core.model.Scrip;
import com.stox.core.repository.BarRepository;
import com.stox.core.ui.LinkButton;
import com.stox.core.ui.model.Link;
import com.stox.screener.Screener;
import com.stox.screener.ui.ScreenerModuleView;
import com.stox.screener.ui.ScreenerState;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentListView;
import com.stox.ui.fx.fluent.scene.control.FluentProgressBar;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.util.Async;
import com.stox.widget.Icon;
import com.stox.widget.Ui;

import javafx.geometry.Pos;

public class ScreenerResultView {

	private Screener screener;
	private final ScreenerModuleView screenerModuleView;
	private final FluentListView<Scrip> listView = new FluentListView<Scrip>();
	private final FluentProgressBar progressBar = new FluentProgressBar().fullWidth().classes("success", "inverted");
	private final FluentButton previousButton = new FluentButton(Icon.LONG_ARROW_LEFT).classes("primary", "icon").onAction(e -> previous());
	private final FluentHBox buttonBar = new FluentHBox(previousButton, progressBar).classes("padded", "spaced").alignment(Pos.CENTER).fullWidth();
	
	public ScreenerResultView(ScreenerModuleView screenerModuleView) {
		this.screenerModuleView = screenerModuleView;
		listView.selectionModel().selectedItemProperty().addListener((o, old, scrip) -> updateLinkState(scrip));
	}
	
	public void start(){
		progressBar.setProgress(0);
		screenerModuleView.content(listView).bottom(buttonBar);
		final ScreenerState state = screenerModuleView.getState();
		screener = Screener.builder()
				.barProvider(BarRepository.getInstance())
				.barSpan(state.getBarSpan())
				.config(state.getConfig())
				.offset(state.getOffset())
				.progressConsumer(this::updateProgress)
				.resultConsumer(this::add)
				.screen(state.getScreen())
				.scrips(state.getScrips())
				.span(state.getSpan())
				.build();
		Async.execute(screener);
	}
	
	private void updateProgress(double progress){
		Ui.fx(() -> progressBar.setProgress(progress));
	}
	
	private void add(Scrip scrip){
		Ui.fx(() -> listView.items().add(scrip));
	}
	
	private void updateLinkState(Scrip scrip){
		final LinkButton linkButton = screenerModuleView.getLinkButton();
		final Link.State linkState = new Link.State(0, scrip, screenerModuleView.getState().getBarSpan());
		linkButton.getLink().setState(linkState);
	}
	
	private void stop(){
		screener.cancel();
		screener = null;
		screenerModuleView.content(null).bottom(null);
	}
	
	private void previous(){
		stop();
		final ScripSelectionView scripSelectionView = new ScripSelectionView(screenerModuleView);
		scripSelectionView.start();
	}

}

package com.stox.charting.indicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.indicator.unit.MoveUnit;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.PolylineUnitParent;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.core.model.Move;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.indicator.ZigZagIndicator;
import com.stox.indicator.ZigZagIndicator.Config;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.shape.Polyline;
import lombok.experimental.Delegate;

public class ChartZigZagIndicator extends AbstractChartIndicator<Config, Move, Point2D> {

	@Delegate
	private final Indicator<Config, Move> indicator = IndicatorFactory.get(ZigZagIndicator.class);

	@Override
	public UnitParent<Point2D> buildParent(Group group) {
		final Polyline line = new Polyline();
		group.getChildren().add(line);
		return new PolylineUnitParent(line);
	}

	@Override
	public Unit<Move> buildUnit(Parent<Point2D> parent) {
		return new MoveUnit(parent);
	}

	@Override
	public boolean isGroupable() {
		return false;
	}

	@Override
	public Underlay getUnderlay(Config config) {
		switch (config.getBarValue()) {
		case OPEN:
		case CLOSE:
		case HIGH:
		case LOW:
		case MID:
			return Underlay.PRICE;
		case SPREAD:
			return Underlay.NONE;
		case VOLUME:
			return Underlay.VOLUME;
		default:
			throw new IllegalArgumentException("Underlay " + config.getBarValue().name() + " is not supported.");
		}
	}

	@Override
	public List<ChartAddIn<Move>> buildAddIns(Config config, UnitParent<Point2D> parent) {
		return Collections.emptyList();
	}

	@Override
	public double getMin(Move value) {
		return null == value ? Double.MAX_VALUE : Math.min(value.getStart().getLow(), value.getEnd().getLow());
	}

	@Override
	public double getMax(Move value) {
		return null == value ? Double.MIN_VALUE : Math.max(value.getStart().getHigh(), value.getEnd().getHigh());
	}

	@Override
	public String getName() {
		return "ZigZag";
	}

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis, List<Move> models, List<Unit<Move>> units,
			UnitParent<Point2D> parent, IndicatorPlot<Config, Move, Point2D> plot) {
		final int clippedStartIndex = xAxis.getClippedStartIndex();
		final int clippedEndIndex = xAxis.getClippedEndIndex();
		int unitIndex = -1;
		for (int index = models.size() - 1; index >= 0; index--) {
			final Move move = models.get(index);
			if (clippedStartIndex <= move.getStartIndex() && move.getEndIndex() <= clippedEndIndex) {
				final Unit<Move> unit = getUnit(units, ++unitIndex, parent);
				unit.update(unitIndex, move, null, xAxis, yAxis);
			}
		}
		clear(units, Math.max(0, unitIndex));
	}

	private void clear(final List<Unit<Move>> units, final int index) {
		final List<Unit<Move>> removableUnits = new ArrayList<>();
		for (int unitIndex = index; unitIndex < units.size(); unitIndex++) {
			final Unit<Move> unit = units.get(unitIndex);
			unit.detach();
			removableUnits.add(unit);
		}
		units.removeAll(removableUnits);
	}

	private Unit<Move> getUnit(final List<Unit<Move>> units, final int index, final UnitParent<Point2D> parent) {
		if (index >= units.size()) {
			final Unit<Move> unit = buildUnit(parent);
			units.add(unit);
			unit.attach();
			return unit;
		} else {
			return units.get(index);
		}
	}

}

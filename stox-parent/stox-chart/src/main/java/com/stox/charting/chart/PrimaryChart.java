package com.stox.charting.chart;

import java.util.List;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.DateTimeAxis;
import com.stox.charting.axis.horizontal.MutableXAxis;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.DelegatingYAxis;
import com.stox.charting.axis.vertical.MutableYAxis;
import com.stox.charting.axis.vertical.PrimaryValueAxis;
import com.stox.charting.event.DataChangedEvent;
import com.stox.charting.event.DataRequestEvent;
import com.stox.charting.grid.HorizontalGrid;
import com.stox.charting.grid.VerticalGrid;
import com.stox.charting.plot.price.PrimaryPricePlot;
import com.stox.charting.unit.PriceUnitType;
import com.stox.charting.widget.BarInfoPanel;
import com.stox.charting.widget.NavigationBar;
import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;

import javafx.event.EventHandler;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class PrimaryChart extends Chart {

	private final MutableXAxis xAxis;
	private final DateTimeAxis dateTimeAxis;
	private final PrimaryPricePlot primaryPricePlot;
	private final NavigationBar chartingNavigationBar;

	public PrimaryChart(final Configuration configuration, final MutableXAxis xAxis, final DelegatingYAxis volumeYAxis, final VerticalGrid verticalGrid,
			final BarInfoPanel barInfoPanel) {
		super(configuration, xAxis, volumeYAxis);
		this.xAxis = xAxis;
		chartingNavigationBar = new NavigationBar(xAxis);
		final HorizontalGrid horizontalGrid = new HorizontalGrid();
		setValueAxis(new PrimaryValueAxis(horizontalGrid));
		setBottom((dateTimeAxis = new DateTimeAxis(verticalGrid)));
		getContent().getChildren().addAll(horizontalGrid, chartingNavigationBar);
		getPlotInfoContainer().getChildren().add(barInfoPanel);

		primaryPricePlot = new PrimaryPricePlot(barInfoPanel, configuration);
		getContent().getChildren().add(primaryPricePlot);
		primaryPricePlot.toFront();
		bind();
	}

	public PriceUnitType getUnitType() {
		return primaryPricePlot.getPriceUnitType();
	}

	public void load(final long to, final BarSpan barSpan, final XAxis xAxis) {
		primaryPricePlot.fireEvent(new DataRequestEvent(to, barSpan, xAxis));
	}

	@Override
	public void showIndexInfo(int index) {
		super.showIndexInfo(index);
		primaryPricePlot.showIndexInfo(index);
	}

	@Override
	protected void bind() {
		super.bind();
		final Pane content = getContent();
		final Region navigationBarNode = chartingNavigationBar;
		navigationBarNode.layoutXProperty()
				.bind(content.widthProperty().subtract(navigationBarNode.widthProperty()).divide(2));
		navigationBarNode.layoutYProperty()
				.bind(content.heightProperty().subtract(navigationBarNode.heightProperty()).subtract(50));

		content.widthProperty().addListener((o, old, value) -> xAxis.setWidth(value.doubleValue()));
	}

	public void setScrip(final Scrip scrip) {
		primaryPricePlot.setScrip(scrip);
	}

	@Override
	public void reset() {
		primaryPricePlot.reset();
		super.reset();
	}

	@Override
	public void updateValueBounds() {
		primaryPricePlot.updateValueBounds(xAxis.getClippedStartIndex(), xAxis.getClippedEndIndex());
		super.updateValueBounds();
		final MutableYAxis yAxis = getMutableYAxis();
		yAxis.setMin(Math.min(yAxis.getMin(), primaryPricePlot.getMin()));
		yAxis.setMax(Math.max(yAxis.getMax(), primaryPricePlot.getMax()));
	}

	@Override
	public void layoutChartChildren() {
		primaryPricePlot.layoutChartChildren(xAxis, getMutableYAxis());
		super.layoutChartChildren();
		dateTimeAxis.layoutChartChildren(xAxis, getBars());
		chartingNavigationBar.toFront();
	}

	public Scrip getScrip() {
		return primaryPricePlot.getScrip();
	}

	public List<Bar> getBars() {
		return primaryPricePlot.getModels();
	}

	public void setUnitType(final PriceUnitType unitType) {
		primaryPricePlot.setPriceUnitType(unitType);
	}

	public void addPrimaryPlotDataChangedEventHandler(EventHandler<DataChangedEvent> eventHandler){
		primaryPricePlot.addEventHandler(DataChangedEvent.TYPE, eventHandler);
	}
}

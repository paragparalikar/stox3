package com.stox.charting.screen;

import java.util.ArrayList;
import java.util.List;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.plot.DerivativePlot;
import com.stox.charting.plot.Underlay;
import com.stox.charting.plot.info.EditablePlotInfoPane;
import com.stox.charting.plot.info.PlotInfoPane;
import com.stox.charting.screen.unit.ScreenUnitFactory;
import com.stox.charting.unit.Unit;
import com.stox.core.model.Bar;
import com.stox.screen.Screen;

import lombok.NonNull;

public class ScreenPlot<T> extends DerivativePlot<ScreenMatch> {

	private final Screen<T> screen;
	private final T configuration;
	private EditablePlotInfoPane editablePlotInfoPane;

	public ScreenPlot(@NonNull final Screen<T> screen, final Configuration chartConfiguration) {
		super(chartConfiguration);
		this.screen = screen;
		configuration = screen.buildDefaultConfig();
		getPlotInfoPane().setName(screen.getName());
		editablePlotInfoPane.addEditEventHandler(event -> edit());
	}

	private void edit() {/*
		final ModalDialogView view = new ModalDialogView();
		view.setTitleIcon(Icon.FILTER);
		view.setTitleText("Edit " + screen.getName());
		view.setPseudoClassState(UiConstant.PSEUDO_CLASS_PRIMARY, true);
		final AutoView autoView = new AutoView(configuration);
		view.setContent(autoView);
		view.setActionButtonText("Edit");
		final ModalDialogPresenter presenter = new ModalDialogPresenter(view);
		view.addActionEventHandler(event -> {
			autoView.updateModel();
			presenter.hide();
			getNode().fireEvent(new ConfigChangedEvent(this));
		});

		presenter.show();
		presenter.center();
	*/}

	@Override
	public Underlay getUnderlay() {
		return Underlay.PRICE;
	}

	@Override
	public void load(final List<Bar> bars) {
		final List<ScreenMatch> models = getModels();
		models.clear();
		final int minBarCount = screen.getMinBarCount(configuration);
		if (minBarCount + 1 < bars.size()) {
			for (int index = bars.size() - minBarCount - 1; index >= 0; index--) {
				if (screen.isMatch(bars.subList(index, index + minBarCount + 1), configuration)) {
					models.add(0, new ScreenMatch(bars.get(index), index));
				}
			}
		}
	}

	@Override
	public void showIndexInfo(int index) {

	}

	@Override
	public Unit<ScreenMatch> buildUnit() {
		return ScreenUnitFactory.getInstance().build(screen.getScreenType(), this);
	}

	private final List<ScreenMatch> visibleModels = new ArrayList<>();

	@Override
	public void updateValueBounds(int start, int end) {
	}

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis) {
		populateVisibleModels(xAxis);
		ensureUnitsSize(0, visibleModels.size() - 1);
		for (int index = 0; index < visibleModels.size(); index++) {
			final ScreenMatch model = visibleModels.get(index);
			final Unit<ScreenMatch> unit = getUnits().get(index);
			unit.update(index, model, null, xAxis, yAxis);
		}
	}

	private void populateVisibleModels(XAxis xAxis) {
		visibleModels.clear();
		final List<ScreenMatch> models = getModels();
		final int layoutEndIndex = Math.max(0, xAxis.getEndIndex());
		final int layoutStartIndex = Math.max(0, xAxis.getStartIndex());
		for (int index = 0; index < models.size(); index++) {
			final ScreenMatch model = models.get(index);
			final int modelIndex = model.getIndex();
			if (modelIndex >= layoutStartIndex && modelIndex <= layoutEndIndex) {
				visibleModels.add(model);
			}
		}
	}

	@Override
	public double getMin(ScreenMatch model) {
		return Double.MAX_VALUE;
	}

	@Override
	public double getMax(ScreenMatch model) {
		return Double.MIN_VALUE;
	}

	@Override
	protected PlotInfoPane buildPlotInfoPane() {
		if (null == editablePlotInfoPane) {
			final PlotInfoPane plotInfoPane = super.buildPlotInfoPane();
			editablePlotInfoPane = new EditablePlotInfoPane(plotInfoPane);
		}
		return editablePlotInfoPane;
	}

}

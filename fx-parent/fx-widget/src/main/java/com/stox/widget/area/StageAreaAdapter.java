package com.stox.widget.area;

import java.util.List;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class StageAreaAdapter implements Area {

	private final Stage stage;

	public StageAreaAdapter(final Stage stage) {
		this.stage = stage;
		final Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		setBounds(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight());
	}

	@Override
	public Node getNode() {
		return stage.getScene().getRoot();
	}

	@Override
	public void setBounds(double x, double y, double width, double height) {
		stage.setX(x);
		stage.setY(y);
		stage.setWidth(width);
		stage.setHeight(height);
	}

	@Override
	public Rectangle2D getMaxBounds() {
		final List<Screen> screens = Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(),
				stage.getHeight());
		final Screen screen = screens.stream().filter(s -> s.getVisualBounds()
				.contains(stage.getX() + stage.getWidth() / 2, stage.getY() + stage.getHeight() / 2)).findFirst().get();
		return screen.getVisualBounds();
	}

	@Override
	public Cursor getCursor() {
		return stage.getScene().getCursor();
	}

	@Override
	public void setCursor(final Cursor cursor) {
		stage.getScene().setCursor(cursor);
	}

	@Override
	public double getX() {
		return stage.getX();
	}

	@Override
	public double getY() {
		return stage.getY();
	}

	@Override
	public double getHeight() {
		return stage.getHeight();
	}

	@Override
	public double getWidth() {
		return stage.getWidth();
	}

	@Override
	public <T extends Event> void addEventHandler(EventType<T> eventType, EventHandler<? super T> eventHandler) {
		stage.addEventHandler(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void addEventFilter(EventType<T> eventType, EventHandler<? super T> eventHandler) {
		stage.addEventFilter(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void removeEventHandler(EventType<T> eventType, EventHandler<T> eventHandler) {
		stage.removeEventHandler(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void removeEventFilter(EventType<T> eventType, EventHandler<T> eventHandler) {
		stage.removeEventFilter(eventType, eventHandler);
	}

}

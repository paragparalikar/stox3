package com.stox.widget.decorator;

import com.stox.widget.area.Area;
import com.stox.widget.area.RegionAreaAdapter;

import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class DockableDecorator {

	private final Node knob;
	private final Region target;
	private final Pane container;
	private final MovableDecorator movableDecorator;
	private final ResizableDecorator resizableDecorator;

	public DockableDecorator(final Node knob, final BorderPane target, final Pane container) {
		this.knob = knob;
		this.target = target;
		this.container = container;
		
		target.setManaged(false);
		final Area area = new RegionAreaAdapter(target);
		movableDecorator = new MovableDecorator(knob, area);
		resizableDecorator = new ResizableDecorator(area, target);
		target.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> target.toFront());
	}

	protected Node getKnob() {
		return knob;
	}

	protected Region getTarget() {
		return target;
	}

	protected Pane getContainer() {
		return container;
	}

	protected ResizableDecorator getResizableDecorator() {
		return resizableDecorator;
	}
	
	protected MovableDecorator getMovableDecorator() {
		return movableDecorator;
	}

}

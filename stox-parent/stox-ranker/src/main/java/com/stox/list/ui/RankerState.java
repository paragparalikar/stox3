package com.stox.list.ui;

import java.util.ArrayList;
import java.util.List;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.list.ranker.Ranker;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RankerState {

	private Object config;
	
	private Ranker<?> ranker;
	
	private int offset = 0;
	
	private volatile boolean stopped;
	
	private BarSpan barSpan = BarSpan.D;
	
	private final List<Scrip> scrips = new ArrayList<>();
	
}

package com.stox.charting.drawing.text;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.drawing.AbstractDrawing;
import com.stox.charting.drawing.Location;
import com.stox.charting.drawing.event.UpdatableRequestEvent;
import com.stox.ui.fx.fluent.scene.layout.FluentGroup;
import com.stox.util.StringUtil;
import com.stox.widget.decorator.NodeRelocationDecorator;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class ChartText extends AbstractDrawing implements EventHandler<MouseEvent> {
	public static final int ID = 5;
	private static final String DELIMITER = "::";

	private volatile TextField textField;
	private final Text text = new Text();
	private final Location location = new Location();
	private final Group group = new FluentGroup().classes("drawing");
	private final EventHandler<KeyEvent> textFieldEventHandler = event -> {
		if (KeyCode.ENTER.equals(event.getCode())) {
			commit();
			event.consume();
		} else if (KeyCode.ESCAPE.equals(event.getCode())) {
			rollback();
			event.consume();
		}
	};

	public ChartText() {
		text.setManaged(false);
		group.setManaged(false);
		text.setCursor(Cursor.MOVE);
		text.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
		final NodeRelocationDecorator nodeRelocationDecorator = new NodeRelocationDecorator(group);
		nodeRelocationDecorator.setMoveCallback(() -> group.fireEvent(new UpdatableRequestEvent(this)));
		nodeRelocationDecorator.attach();
		bind();
	}

	public void edit() {
		if (null == textField) {
			textField = new TextField();
			textField.setText(text.getText());
			textField.addEventHandler(KeyEvent.KEY_PRESSED, textFieldEventHandler);
			textField.requestFocus();
			group.getChildren().setAll(textField);
		}
	}

	public void commit() {
		if (null != textField) {
			text.setText(textField.getText());
			group.getChildren().setAll(text);
			textField = null;
		}
	}

	public void rollback() {
		group.getChildren().setAll(text);
		textField = null;
	}

	@Override
	public Node getNode() {
		return group;
	}

	@Override
	public int getDrawingId() {
		return ID;
	}
	
	@Override
	public void update(XAxis xAxis, YAxis yAxis) {
		location.setDate(xAxis.getDate(group.getLayoutX()));
		location.setValue(yAxis.getValue(group.getLayoutY()));
	}

	public void move(final double screenX, final double screenY) {
		final Parent parent = group.getParent();
		if (null != parent) {
			final Point2D point = parent.screenToLocal(screenX, screenY);
			group.setLayoutX(point.getX());
			group.setLayoutY(point.getY());
			group.fireEvent(new UpdatableRequestEvent(this));
		}
	}

	@Override
	public void handle(MouseEvent event) {
		if (MouseButton.PRIMARY.equals(event.getButton()) && !event.isConsumed()) {
			if (MouseEvent.MOUSE_CLICKED.equals(event.getEventType()) && 2 == event.getClickCount()) {
				edit();
			}
		}
	}

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis) {
		group.setLayoutX(xAxis.getX(location.getDate()));
		group.setLayoutY(yAxis.getY(location.getValue()));
	}

	@Override
	public String format() {
		return text.getText() + DELIMITER + location.format();
	}

	@Override
	public void parse(String text) {
		final String[] tokens = text.split(DELIMITER);
		StringUtil.ifHasText(tokens, 0, token -> ChartText.this.text.setText(token));
		StringUtil.ifHasText(tokens, 1, token -> location.parse(token));
	}

}

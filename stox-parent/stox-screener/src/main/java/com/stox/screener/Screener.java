package com.stox.screener;

import java.util.List;
import java.util.function.Consumer;

import com.stox.core.model.Bar;
import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.BarProvider;
import com.stox.screen.Screen;

import lombok.Builder;
import lombok.NonNull;

@Builder
@SuppressWarnings("rawtypes")
public class Screener implements Runnable{
	
	private final int span, offset;
	
	private final Object config;
	
	@NonNull
	private final Screen screen;
	
	@NonNull
	private final BarSpan barSpan;
	
	@NonNull
	private final List<Scrip> scrips;
	
	@NonNull
	private final Consumer<Scrip> resultConsumer;
	
	private final Consumer<Double> progressConsumer;
	
	@NonNull
	private final BarProvider barProvider;
	
	@Builder.Default
	private boolean cancelled = false;
	
	public void cancel(){
		this.cancelled = true;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		final int minBarCount = screen.getMinBarCount(config) + span + offset;
		for(int index = 0; index < scrips.size(); index++){
			try{
				if(cancelled) break;
				final Scrip scrip = scrips.get(index);
				final List<Bar> bars = barProvider.get(scrip.getIsin(), barSpan, minBarCount);
				if(bars.size() >= minBarCount){
					final List<Bar> offsetBars = bars.subList(offset, bars.size());
					for(int spanIndex = 0; spanIndex < span; spanIndex++){
						final List<Bar> data = offsetBars.subList(spanIndex, offsetBars.size());
						if(screen.isMatch(data, config)){
							resultConsumer.accept(scrip);
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				try{
					if(null != progressConsumer){
						final double progress = ((double)index)/((double)scrips.size()-1);
						progressConsumer.accept(progress);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		try{
			progressConsumer.accept(1d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}

package com.stox.charting.event;

import com.stox.charting.plot.DerivativePlot;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;

public class UnderlayChangedEvent extends Event {
	private static final long serialVersionUID = -4661085740139090637L;

	public static final EventType<UnderlayChangedEvent> TYPE = new EventType<>("UnderlayChangedEvent");

	@Getter
	private final DerivativePlot<?> plot;
	
	public UnderlayChangedEvent(final DerivativePlot<?> plot) {
		super(TYPE);
		this.plot = plot;
	}

}

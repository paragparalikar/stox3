package com.stox.charting.event;

import com.stox.charting.plot.DerivativePlot;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;

@Getter
public class PlotRemovedEvent extends Event {
	private static final long serialVersionUID = 732857006798695256L;

	public static final EventType<PlotRemovedEvent> TYPE = new EventType<>("PlotRemovedEvent");

	private final DerivativePlot<?> plot;

	public PlotRemovedEvent(final DerivativePlot<?> plot) {
		super(TYPE);
		this.plot = plot;
	}

}

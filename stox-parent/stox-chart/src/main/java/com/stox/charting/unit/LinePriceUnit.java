package com.stox.charting.unit;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.core.model.Bar;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LinePriceUnit implements PriceUnit {

	private final Parent<Point2D> parent;
	private final Configuration configuration;

	@Override
	public void update(int index, Bar model, Bar previousModel, XAxis xAxis, YAxis yAxis) {
		parent.addAll(new Point2D(xAxis.getX(index) + xAxis.getUnitWidth(), yAxis.getY(model.getClose())));
	}
	
	
	@Override
	public void attach() {

	}

	@Override
	public void detach() {

	}

}

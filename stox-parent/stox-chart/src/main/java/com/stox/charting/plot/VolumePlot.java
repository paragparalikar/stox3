package com.stox.charting.plot;

import java.util.List;
import java.util.stream.Collectors;

import com.stox.charting.Configuration;
import com.stox.charting.axis.vertical.TransformerYAxis;
import com.stox.charting.plot.info.EditablePlotInfoPane;
import com.stox.charting.plot.info.PlotInfoPane;
import com.stox.charting.plot.info.ValuePlotInfoPane;
import com.stox.charting.unit.BarUnit;
import com.stox.charting.unit.Unit;
import com.stox.core.model.Bar;
import com.stox.util.StringUtil;
import com.stox.widget.parent.GroupParentAdapter;
import com.stox.widget.parent.Parent;

import javafx.scene.Node;

public class VolumePlot extends DerivativePlot<Double> {

	private ValuePlotInfoPane valuePlotInfoPane;
	private EditablePlotInfoPane editablePlotInfoPane;
	private final Parent<Node> parent = new GroupParentAdapter(this);
	private final TransformerYAxis transformerYAxis;

	public VolumePlot(final Configuration configuration, final TransformerYAxis yAxis) {
		super(configuration);
		transformerYAxis = yAxis;
	}

	@Override
	public void showIndexInfo(int index) {
		final List<Double> values = getModels();
		valuePlotInfoPane
				.setValue(index >= 0 && index < values.size() ? StringUtil.stringValueOf(values.get(index)) : null);
	}

	@Override
	protected PlotInfoPane buildPlotInfoPane() {
		if (null == valuePlotInfoPane) {
			final PlotInfoPane plotInfoPane = super.buildPlotInfoPane();
			plotInfoPane.setName("Volume");
			editablePlotInfoPane = new EditablePlotInfoPane(plotInfoPane);
			valuePlotInfoPane = new ValuePlotInfoPane(editablePlotInfoPane);
		}
		return valuePlotInfoPane;
	}

	@Override
	public void updateValueBounds(int start, int end) {
		super.updateValueBounds(start, end);
		transformerYAxis.setMin(super.getMin());
		transformerYAxis.setMax(super.getMax());
	}

	@Override
	public Unit<Double> buildUnit() {
		final BarUnit barUnit = new BarUnit(parent);
		barUnit.setOpacity(0.25);
		return barUnit;
	}

	@Override
	public void load(List<Bar> bars) {
		final List<Double> models = getModels();
		synchronized (models) {
			models.clear();
			models.addAll(bars.stream().map(bar -> bar.getVolume()).collect(Collectors.toList()));
		}
	}

	@Override
	public double getMin(Double model) {
		return model;
	}

	@Override
	public double getMax(Double model) {
		return model;
	}

	@Override
	public double getMin() {
		return Double.MAX_VALUE;
	}

	@Override
	public double getMax() {
		return Double.MIN_VALUE;
	}

	@Override
	public Underlay getUnderlay() {
		return Underlay.VOLUME;
	}
}

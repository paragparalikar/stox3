package com.stox.list.ranker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;

import lombok.Getter;

public class RankerFactory {

	private interface Wrapper {
		RankerFactory INSTANCE = new RankerFactory();
	}

	public static RankerFactory instance() {
		return Wrapper.INSTANCE;
	}

	@Getter
	private final List<Ranker<?>> all = new ArrayList<>();

	private RankerFactory() {
		ServiceLoader.load(Ranker.class).forEach(ranker -> all.add(ranker));
		Collections.sort(all, (one, two) -> one.getName().compareToIgnoreCase(two.getName()));
	}

}

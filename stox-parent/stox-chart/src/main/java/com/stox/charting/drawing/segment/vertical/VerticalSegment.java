package com.stox.charting.drawing.segment.vertical;

import com.stox.charting.drawing.ControlPoint;
import com.stox.charting.drawing.segment.Segment;

public class VerticalSegment extends Segment {
	public static final int ID = 3;

	@Override
	public int getDrawingId() {
		return ID;
	}

	protected void bind() {
		super.bind();
		final ControlPoint one = getOne();
		final ControlPoint two = getTwo();
		one.centerXProperty().bindBidirectional(two.centerXProperty());
	}

	public void move(final double deltaX, final double deltaY) {
		final ControlPoint one = getOne();
		final ControlPoint two = getTwo();
		move(one.getCenterX() + deltaX, one.getCenterY() + deltaY, two.getCenterY() + deltaY);
	}

	public void move(final double x, final double startY, final double endY) {
		final ControlPoint one = getOne();
		final ControlPoint two = getTwo();
		one.setCenterX(x);
		one.setCenterY(startY);
		two.setCenterX(x);
		two.setCenterY(endY);
	}

}

package com.stox.data;

import java.util.ArrayList;

public class StatusMessage extends ArrayList<Runnable>{
	private static final long serialVersionUID = 1L;
	
	private Boolean success;
	private String message;
	
	public StatusMessage(String message) {
		super(3);
		this.message = message;
	}

	public void setSuccess(boolean success) {
		this.success = success;
		forEach(Runnable::run);
	}
	
	public Boolean isSuccess() {
		return success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
}
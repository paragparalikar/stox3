package com.stox.workbench;

import java.util.List;

import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.widget.Icon;
import com.stox.widget.Spacer;
import com.stox.widget.Ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class WorkbenchTitleBar {

	private double x, y, width, height;
	private final WorkbenchMenuBar menuBar = new WorkbenchMenuBar();
	private final FluentButton minimizeButton = new FluentButton(Icon.WINDOW_MINIMIZE).classes("primary", "icon","first").tooltip(Ui.tooltip("Minimize"));
	private final FluentButton restoreButton = new FluentButton(Icon.WINDOW_RESTORE).classes("primary", "icon","middle").tooltip(Ui.tooltip("Restore"));
	private final FluentButton exitButton = new FluentButton(Icon.TIMES).classes("primary", "icon", "last", "hover-danger").tooltip(Ui.tooltip("Exit"));
	private final FluentHBox windowControls = new FluentHBox(minimizeButton, restoreButton, exitButton).classes("container","box");
	private final FluentHBox node = new FluentHBox(menuBar, new Spacer(), new AuthorContact(), windowControls).classes("primary-background","title-bar");
	
	public void bind(Stage stage){
		minimizeButton.onAction(e -> stage.setIconified(true));
		restoreButton.onAction(e -> toggleMaximized(stage));
		height = stage.getHeight();
		width = stage.getWidth();
		stage.addEventHandler(WindowEvent.WINDOW_SHOWN, e -> {
			height = stage.getHeight();
			width = stage.getWidth();
		});
		node.eventHandler(MouseEvent.MOUSE_CLICKED, e -> click(e, stage));
	}
	
	private void click(MouseEvent e, Stage stage){
		if(!e.isConsumed() && MouseButton.PRIMARY.equals(e.getButton()) && 2 == e.getClickCount()){
			toggleMaximized(stage);
		}
	}
	
	private void toggleMaximized(Stage stage){
		if(isMaximized()){
			restore(stage);
		}else{
			maximize(stage);
		}
	}
	
	private void restore(Stage stage){
		if(0 < height && 0 < width){
			stage.setX(x);
			stage.setY(y);
			stage.setWidth(width);
			stage.setHeight(height);
			restoreButton.setText(Icon.WINDOW_MAXIMIZE);
		}
	}
	
	private void maximize(Stage stage){
		x = stage.getX();
		y = stage.getY();
		width = stage.getWidth();
		height = stage.getHeight();
		restoreButton.setText(Icon.WINDOW_RESTORE);
		final Screen screen = getScreen(stage);
		final Rectangle2D rectangle = screen.getVisualBounds();
		stage.setX(rectangle.getMinX());
		stage.setY(rectangle.getMinY());
		stage.setWidth(rectangle.getWidth());
		stage.setHeight(rectangle.getHeight());
	}
	
	private boolean isMaximized(){
		return Icon.WINDOW_RESTORE.equals(restoreButton.getText());
	}
	
	private Screen getScreen(Stage stage){
		final List<Screen> screens = Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
		Screen result = Screen.getPrimary();
		double maxArea = 0;
		for(Screen screen : screens){
			final Rectangle2D rectangle = screen.getBounds();
			if(rectangle.intersects(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight())){
				final double minX = Math.max(rectangle.getMinX(), stage.getX());
				final double minY = Math.max(rectangle.getMinY(), stage.getY());
				final double maxX = Math.min(rectangle.getMinX() + rectangle.getWidth(), stage.getX() + stage.getWidth());
				final double maxY = Math.min(rectangle.getMinY() + rectangle.getHeight(), stage.getY() + stage.getHeight());
				final double area = (maxX - minX)*(maxY - minY);
				if(area > maxArea){
					maxArea = area;
					result = screen;
				}
			}
		}
		return result;
	}
	
	public Node getNode(){
		return node;
	}
	
	public WorkbenchMenuBar getMenuBar() {
		return menuBar;
	}

	public WorkbenchTitleBar exitEventHandler(EventHandler<ActionEvent> eventHandler){
		exitButton.setOnAction(eventHandler);
		return this;	
	}
	
}

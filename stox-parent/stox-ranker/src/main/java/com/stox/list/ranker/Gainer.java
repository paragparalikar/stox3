package com.stox.list.ranker;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.Scrip;
import com.stox.list.ranker.Gainer.Config;

import lombok.Getter;
import lombok.Setter;

public class Gainer implements Ranker<Config> {

	@Getter
	@Setter
	public static class Config {
		private int span = 10;
	}

	@Override
	public String getName() {
		return "Gainer";
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan();
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public RankerResult rank(Scrip scrip, List<Bar> bars, Config config) {
		int minIndex = 0, maxIndex = 0;
		double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
		for (int index = 0; index < bars.size() && index < config.getSpan(); index++) {
			final Bar bar = bars.get(index);
			if (min > bar.getLow()) {
				min = bar.getLow();
				minIndex = index;
			}
			if (max < bar.getHigh()) {
				max = bar.getHigh();
				maxIndex = index;
			}
		}
		return new RankerResult(scrip, ((max - min)*100/min) * (minIndex < maxIndex ? -1 : 1));
	}

}

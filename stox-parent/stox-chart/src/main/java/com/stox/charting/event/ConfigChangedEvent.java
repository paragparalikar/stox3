package com.stox.charting.event;

import com.stox.charting.plot.DerivativePlot;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;

public class ConfigChangedEvent extends Event {
	private static final long serialVersionUID = 8908887112476536911L;

	public static final EventType<ConfigChangedEvent> TYPE = new EventType<>("ConfigChangedEvent");
	
	@Getter
	private final DerivativePlot<?> plot;
	
	public ConfigChangedEvent(final DerivativePlot<?> plot) {
		super(TYPE);
		this.plot = plot;
	}
	
}

package com.stox.workbench.event;

import java.util.function.Supplier;

import com.stox.workbench.module.view.ModuleView;

public class ModuleViewRegistration {
	public static final String TYPE = "ModuleViewRegistrationEvent";
	
	private final String id;
	private final String icon;
	private final String name;
	private final boolean multipleAllowed;
	private final Supplier<ModuleView> supplier;

	public ModuleViewRegistration(String id, String icon, String name, boolean multipleAllowed, Supplier<ModuleView> supplier) {
		super();
		this.id = id;
		this.icon = icon;
		this.name = name;
		this.supplier = supplier;
		this.multipleAllowed = multipleAllowed;
	}

	public String getId() {
		return id;
	}
	
	public String getIcon() {
		return icon;
	}

	public String getName() {
		return name;
	}

	public Supplier<ModuleView> getSupplier() {
		return supplier;
	}
	
	public boolean isMultipleAllowed() {
		return multipleAllowed;
	}

}

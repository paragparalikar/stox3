package com.stox.core.ui.filter.scrip;

import java.util.ArrayList;
import java.util.List;

import com.stox.ui.fx.fluent.scene.control.FluentCheckBox;
import com.stox.ui.fx.fluent.scene.control.FluentLabel;
import com.stox.ui.fx.fluent.scene.layout.FluentBorderPane;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public abstract class FilterVBox<T, V> extends FluentBorderPane {

	private List<T> items;
	private final FluentVBox container = new FluentVBox();
	private final FluentLabel titleLabel = new FluentLabel().fullWidth().classes("primary", "padded");
	
	public abstract void filter(Iterable<V> values);

	public FilterVBox(String title) {
		top(titleLabel.text(title)).center(container);
	}
	
	protected FilterVBox<T, V> items(List<T> value){
		items = value;
		updateItemsDisplay();
		return this;
	}
	
	private void updateItemsDisplay(){
		container.children().clear();
		items.forEach(item -> {
			final FluentLabel label = new FluentLabel(item.toString(), new FluentCheckBox().classes("primary", "inverted"))
					.userData(item)
					.classes("primary", "inverted", "padded");
			container.children().add(label);
		});
	}
	
	@SuppressWarnings("unchecked")
	public List<T> selectedItems(){
		final List<T> selectedItems = new ArrayList<>();
		for(Node node : container.children()){
			if(node instanceof Label){
				final Label label = (Label) node;
				final Node graphic = label.getGraphic();
				if(graphic instanceof CheckBox){
					final CheckBox checkBox = (CheckBox)graphic;
					if(checkBox.isSelected()){
						final T item = (T) label.getUserData();
						selectedItems.add(item);
					}
				}
			}
		}
		return selectedItems;
	}

}

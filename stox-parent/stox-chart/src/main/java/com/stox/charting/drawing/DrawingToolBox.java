package com.stox.charting.drawing;

import com.stox.charting.ChartingView;
import com.stox.charting.drawing.region.ChartRegionToggleButton;
import com.stox.charting.drawing.segment.horizontal.HorizontalSegmentToggleButton;
import com.stox.charting.drawing.segment.trend.TrendSegmentToggleButton;
import com.stox.charting.drawing.segment.vertical.VerticalSegmentToggleButton;
import com.stox.charting.drawing.text.ChartTextToggleButton;
import com.stox.charting.tools.ChartingToolBox;
import com.stox.widget.Ui;

import javafx.scene.Node;
import javafx.scene.layout.HBox;

public class DrawingToolBox extends HBox implements ChartingToolBox {

	@Override
	public Node getNode() {
		return this;
	}

	@Override
	public void attach(ChartingView chartingView) {
		getChildren().addAll(
				new TrendSegmentToggleButton(chartingView),
				new HorizontalSegmentToggleButton(chartingView),
				new VerticalSegmentToggleButton(chartingView),
				new ChartRegionToggleButton(chartingView),
				new ChartTextToggleButton(chartingView),
				new ClearDrawingsButton(chartingView));
		Ui.box(this);
	}

}

package com.stox.workbench.delegate;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.stox.event.Event;
import com.stox.util.FileUtil;
import com.stox.workbench.module.view.ModuleView;
import com.stox.workbench.module.view.ModuleViewFactory;

import javafx.stage.Stage;

public class StartupDelegate extends AbstractLifecycleDelegate {

	public void startup(String path, Stage stage, ModuleViewFactory moduleViewFactory) {
		try {
			final List<String> lines = Files.readAllLines(FileUtil.safeGet(path));
			if (null != lines && !lines.isEmpty()) {
				final String workbenchLine = lines.get(0);
				parse(workbenchLine, stage);
				if (1 <= lines.size()) {
					for (int index = 1; index < lines.size(); index++) {
						final String line = lines.get(index);
						final ModuleView moduleView = parse(line, moduleViewFactory);
						Event.fire(ModuleView.SHOW, moduleView);
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}

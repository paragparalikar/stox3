package com.stox.core.util;

import com.stox.core.model.Bar;
import com.stox.core.model.BarValue;

public abstract class BarUtil {

	private BarUtil() {
	}

	public static boolean isBullishPinBar(final Bar bar) {
		final double body = BarValue.BODY.get(bar);
		final double spread = BarValue.SPREAD.get(bar);
		final double upperWick = BarValue.UWICK.get(bar);
		final double lowerWick = BarValue.LWICK.get(bar);
		return lowerWick > spread * 2 / 3 || (lowerWick > 2 * body && lowerWick > 2 * upperWick);
	}

	public static boolean isReversalBar(final Bar bar, final Bar previous) {
		return bar.getLow() < previous.getLow() && bar.getClose() > previous.getClose()
				&& bar.getClose() > BarValue.MID.get(previous) && bar.getClose() > BarValue.MID.get(bar);
	}

}

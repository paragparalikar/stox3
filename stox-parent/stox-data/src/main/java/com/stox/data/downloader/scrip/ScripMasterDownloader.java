package com.stox.data.downloader.scrip;

import java.io.IOException;
import java.util.List;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;

public interface ScripMasterDownloader {

	List<Scrip> download(Exchange exchange) throws IOException;
	
}

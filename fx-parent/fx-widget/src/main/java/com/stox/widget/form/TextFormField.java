package com.stox.widget.form;

import com.stox.ui.fx.fluent.scene.control.FluentTextField;

public class TextFormField extends FormField {
	
	private final FluentTextField textField = new FluentTextField().fullArea();
	
	public TextFormField(String name) {
		super(name);
		getChildren().add(textField);
	}
	
	public FluentTextField getTextField() {
		return textField;
	}

}

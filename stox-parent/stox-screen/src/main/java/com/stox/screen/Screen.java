package com.stox.screen;

import java.util.List;

import com.stox.core.model.Bar;


public interface Screen<T> {

	String getCode();
	
	String getName();
	
	ScreenType getScreenType();
	
	T buildDefaultConfig();
	
	int getMinBarCount(final T config);
	
	boolean isMatch(final List<Bar> bars, final T config);
	
}

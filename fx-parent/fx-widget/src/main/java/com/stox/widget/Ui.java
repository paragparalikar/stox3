package com.stox.widget;

import java.util.List;

import com.stox.util.WeakKeyValueMap;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class Ui {
	
	private static final WeakKeyValueMap<String, Tooltip> tooltips = new WeakKeyValueMap<String, Tooltip>();
	
	public static Tooltip tooltip(String text){
		Tooltip tooltip = tooltips.getValue(text);
		if(null == tooltip){
			tooltip = new Tooltip(text);
			tooltips.putValue(text, tooltip);
		}
		return tooltip;
	}

	public static void fx(Runnable runnable) {
		if (Platform.isFxApplicationThread()) {
			runnable.run();
		} else {
			Platform.runLater(runnable);
		}
	}
	
	public static String web(final Color color) {
		return String.format("#%02X%02X%02X", (int) (color.getRed() * 255), (int) (color.getGreen() * 255),
				(int) (color.getBlue() * 255));
	}
	
	public static double px(final double value) {
		return Math.floor(value) + 0.5;
	}
	
	public static <T extends Parent> T box(T parent){
		parent.getStyleClass().add("box");
		final List<Node> children = parent.getChildrenUnmodifiable();
		for(int index = 0; index < children.size(); index++){
			final Node child = children.get(index);
			if(0 == index){
				child.getStyleClass().add("first");
			}else if(index == children.size() - 1){
				child.getStyleClass().add("last");
			}else{
				child.getStyleClass().add("middle");
			}
		}
		return parent;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getParentOfType(final Class<T> type, final Node node) {
		final javafx.scene.Parent parent = null == node ? null : node.getParent();
		return null == parent ? null
				: type.isAssignableFrom(parent.getClass()) ? (T) parent : getParentOfType(type, parent);
	}

	
	public static void fade(Node node, double from, double to, Runnable onFinishedCallback){
		final FadeTransition transition = new FadeTransition(Duration.millis(150), node);
		transition.setFromValue(from);
		transition.setToValue(to);
		if(null != onFinishedCallback) transition.setOnFinished(e -> onFinishedCallback.run());
		transition.play();
	}
	
}

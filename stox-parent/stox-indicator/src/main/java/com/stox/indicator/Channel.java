package com.stox.indicator;

import com.stox.util.Constant;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Channel {
	private double upper;
	private double middle;
	private double lower;

	@Override
	public String toString() {
		return Constant.currencyFormat.format(upper) + ", " + Constant.currencyFormat.format(middle) + ", "
				+ Constant.currencyFormat.format(lower);
	}
}
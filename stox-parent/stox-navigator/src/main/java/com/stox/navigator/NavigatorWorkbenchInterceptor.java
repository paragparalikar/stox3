package com.stox.navigator;

import com.stox.widget.Icon;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class NavigatorWorkbenchInterceptor implements WorkbenchInterceptor {

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(NavigatorView.ID, Icon.LIST, "Navigator", true, () -> new NavigatorView());
		workbench.register(registration);
	}

}

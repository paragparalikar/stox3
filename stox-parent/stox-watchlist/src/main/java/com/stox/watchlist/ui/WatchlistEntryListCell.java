package com.stox.watchlist.ui;

import java.util.function.Supplier;

import com.stox.event.Event;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.watchlist.model.Watchlist;
import com.stox.watchlist.model.WatchlistEntry;
import com.stox.watchlist.repository.WatchlistEntryRepository;
import com.stox.widget.Icon;

import javafx.scene.control.ListCell;

public class WatchlistEntryListCell extends ListCell<WatchlistEntry> {

	private final Supplier<Watchlist> watchlistSupplier;
	private final WatchlistEntryRepository watchlistEntryRepository = WatchlistEntryRepository.getInstance(); 
	private final FluentButton graphic = new FluentButton(Icon.TRASH).classes("icon", "danger", "inverted").onAction(e -> delete());
	
	public WatchlistEntryListCell(Supplier<Watchlist> watchlistSupplier) {
		this.watchlistSupplier = watchlistSupplier;
	}
	
	@Override
	protected void updateItem(WatchlistEntry item, boolean empty) {
		super.updateItem(item, empty);
		if(null == item || empty){
			setGraphic(null);
			setText(null);
		}else{
			setGraphic(graphic);
			setText(item.getScrip().getName());
		}
	}

	private void delete() {
		final Watchlist watchlist = watchlistSupplier.get();
		final WatchlistEntry entry = watchlistEntryRepository.delete(watchlist.getId(), getItem());
		Event.fire(WatchlistEntry.DELETED, entry);
	}
	
}

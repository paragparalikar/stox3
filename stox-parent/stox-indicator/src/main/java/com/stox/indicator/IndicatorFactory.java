package com.stox.indicator;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class IndicatorFactory {

	private static final List<Indicator<?, ?>> ALL = new ArrayList<>();
	static {
		ServiceLoader.load(Indicator.class).forEach(indicator -> ALL.add(indicator));
	}
	
	@SuppressWarnings("unchecked")
	public static <T, V> Indicator<T, V> get(final Class<? extends Indicator<T, V>> clazz) {
		return (Indicator<T, V>) ALL.stream().filter(indicator -> clazz.isAssignableFrom(indicator.getClass()))
				.findFirst().orElse(null);
	}

}

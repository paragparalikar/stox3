package com.stox.list;

import com.stox.list.ui.RankerModuleView;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class RankerWorkbenchInterceptor implements WorkbenchInterceptor{

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(RankerModuleView.ID, RankerModuleView.ICON, "Ranker", true, () -> new RankerModuleView());
		workbench.register(registration);
	}
	
}

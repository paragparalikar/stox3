package com.stox.example.ui;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.example.model.ExampleGroup;
import com.stox.example.repository.ExampleGroupRepository;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.layout.FluentHBox;
import com.stox.util.Async;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.workbench.modal.ConfirmationModal;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;

public class ExampleGroupBox extends FluentHBox implements Supplier<ExampleGroup>{
	
	private final FluentComboBox<ExampleGroup> comboBox = new FluentComboBox<ExampleGroup>().fullArea().classes("primary","inverted", "first");
	private final FluentButton createButton = new FluentButton(Icon.PLUS).onAction(e -> create()).classes("icon","primary","inverted","middle");
	private final FluentButton editButton = new FluentButton(Icon.EDIT).onAction(e -> edit()).disable(true).classes("icon","primary","inverted","middle");
	private final FluentButton deleteButton = new FluentButton(Icon.TRASH).onAction(e -> delete()).disable(true).classes("icon","primary","inverted","last");
	
	private final ExampleGroupRepository exampleGroupRepository = ExampleGroupRepository.getInstance();
	
	public ExampleGroupBox() {
		children(comboBox, createButton, editButton, deleteButton).fullArea().classes("box");
		comboBox.selectionModel().selectedIndexProperty().addListener((o,old,value) -> {
			editButton.disable(null == value);
			deleteButton.disable(null == value);
		});
	}
	
	public void start(){
		Event.register(this);
		disable(true);
		Async.execute(()->{
			final List<ExampleGroup> watchlists = exampleGroupRepository.find();
			Collections.sort(watchlists);
			Ui.fx(() -> {
				comboBox.getItems().setAll(watchlists);
				if(!watchlists.isEmpty()){
					comboBox.getSelectionModel().select(0);
				}
				disable(false);
			});
		});
	}
	
	public void stop(){
		Event.unregister(this);
	}
	
	@Override
	protected void finalize() throws Throwable {
		stop();
		super.finalize();
	}
	
	private void create(){
		final ExampleGroupEditorModal modal = new ExampleGroupEditorModal(null);
		modal.show();
	}
	
	private void edit(){
		final ExampleGroupEditorModal modal = new ExampleGroupEditorModal(comboBox.getValue());
		modal.show();
	}
	
	private void delete(){
		final ExampleGroup watchlist = comboBox.getValue();
		new ConfirmationModal(
				"Are you sure you want to delete example group \""+watchlist.getName()+"\"?", 
				"Delete",
				() -> {
					try{
						exampleGroupRepository.delete(watchlist);
						Event.fire(ExampleGroup.DELETED, watchlist);
						// TODO Toast here 
					}catch(Exception e){
						e.printStackTrace();
						// TODO Toast here 
					}
				}).show();
	}
	
	
	@EventHandler(ExampleGroup.CREATED)
	public void onWatchlistCreated(ExampleGroup watchlist){
		Ui.fx(() -> {
			comboBox.items().add(watchlist);
			FXCollections.sort(comboBox.items());
			comboBox.select(watchlist);
		});
	}
	
	@EventHandler(ExampleGroup.MODIFIED)
	public void onWatchlistModified(ExampleGroup watchlist){
		Ui.fx(() -> {
			final Iterator<ExampleGroup> iterator = comboBox.items().iterator();
			while(iterator.hasNext()){
				final ExampleGroup item = iterator.next();
				if(item.getId().equals(watchlist.getId())){
					iterator.remove();
					break;
				}
			}
			comboBox.items().add(watchlist);
			FXCollections.sort(comboBox.items());
			comboBox.setValue(watchlist);
		});
	}
	
	@EventHandler(ExampleGroup.DELETED)
	public void onWatchlistDeleted(ExampleGroup watchlist){
		Ui.fx(() -> {
			comboBox.items().remove(watchlist);
			if(!comboBox.items().isEmpty()){
				comboBox.getSelectionModel().select(0);
			}
		});
	}
			
	@Override
	public ExampleGroup get() {
		return comboBox.getValue();
	}
	
	public void addChangeListener(ChangeListener<? super ExampleGroup> listener){
		comboBox.getSelectionModel().selectedItemProperty().addListener(listener);
	}

}

package com.stox.data;

import com.stox.widget.Icon;
import com.stox.workbench.Workbench;
import com.stox.workbench.WorkbenchInterceptor;
import com.stox.workbench.event.ModuleViewRegistration;

public class DataWorkbenchInterceptor implements WorkbenchInterceptor{

	@Override
	public void beforeStart(Workbench workbench) throws Exception {
		final ModuleViewRegistration registration = new ModuleViewRegistration(DataView.ID, Icon.DATABASE,"Data", false, () -> new DataView());
		workbench.register(registration);
	}

}

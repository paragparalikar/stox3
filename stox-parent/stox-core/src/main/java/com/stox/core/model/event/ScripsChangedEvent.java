package com.stox.core.model.event;

import java.util.List;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;

import lombok.Value;

@Value
public class ScripsChangedEvent {
	public static final String TYPE = "ScripsChangedEvent";

	private Exchange exchange;
	
	private List<Scrip> scrips;
	
}

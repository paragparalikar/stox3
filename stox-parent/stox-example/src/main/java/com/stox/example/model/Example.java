package com.stox.example.model;

import java.util.Comparator;
import java.util.Objects;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.model.intf.HasScrip;

import lombok.NonNull;
import lombok.Value;

@Value
public class Example implements HasScrip, Comparable<Example>{
	public static final String CREATED = "EXAMPLE_CREATED";
	public static final String DELETED = "EXAMPLE_DELETED";
	public static final Comparator<Example> COMPARATOR = (one, two) -> one.scrip.getName().compareToIgnoreCase(two.getScrip().getName());

	@NonNull
	private String groupId;
	
	@NonNull
	private Scrip scrip;
	
	@NonNull
	private BarSpan barSpan;
	
	private long date;

	@Override
	public int compareTo(Example other) {
		return Objects.compare(this, other, COMPARATOR);
	}
	
}

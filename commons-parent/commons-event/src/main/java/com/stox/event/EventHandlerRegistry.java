package com.stox.event;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class EventHandlerRegistry {

	private final Map<String, Set<EventRegistration>> registrations = new HashMap<>();

	public void register(final Object object) {
		if (null != object) {
			register(object.getClass(), object);
		}
	}

	public void unregister(final Object object) {
		for (final Set<EventRegistration> eventRegistrations : registrations.values()) {
			if (null != eventRegistrations) {
				final Iterator<EventRegistration> iterator = eventRegistrations.iterator();
				while (iterator.hasNext()) {
					final EventRegistration eventRegistration = iterator.next();
					if (null == eventRegistration || null == eventRegistration.getTarget()
							|| object == eventRegistration.getTarget()) {
						iterator.remove();
					}
				}
			}
		}
		cleanup();
	}

	private void cleanup() {
		final Iterator<Map.Entry<String, Set<EventRegistration>>> iterator = registrations.entrySet().iterator();
		while (iterator.hasNext()) {
			final Map.Entry<String, Set<EventRegistration>> entry = iterator.next();
			final Set<EventRegistration> eventRegistrations = entry.getValue();
			if (null == eventRegistrations || eventRegistrations.isEmpty()) {
				iterator.remove();
			}
		}
	}

	private void register(final Class<?> clazz, final Object target) {
		if (!Object.class.equals(clazz)) {
			final Method[] methods = clazz.getDeclaredMethods();
			if (null != methods && 0 < methods.length) {
				for (final Method method : methods) {
					method.setAccessible(true);
					final EventHandler eventHandler = method.getAnnotation(EventHandler.class);
					if (null != eventHandler) {
						validate(method);
						final String topic = eventHandler.value();
						Set<EventRegistration> topicRegistrations = registrations.get(topic);
						if (null == topicRegistrations) {
							topicRegistrations = new HashSet<>();
							registrations.put(topic, topicRegistrations);
						}
						topicRegistrations.add(new EventRegistration(target, method));
					}
				}
			}
			register(clazz.getSuperclass(), target);
		}
	}

	private void validate(final Method method) {
		if (1 < method.getParameterCount()) {
			throw new RuntimeException("@EventHandler method can have at most one parameter");
		}
	}

	public void invoke(final String topic, final Object payload) {
		final Set<EventRegistration> topicRegistrations = registrations.get(topic);
		if (null != topicRegistrations) {
			for (final EventRegistration eventRegistration : topicRegistrations) {
				eventRegistration.invoke(payload);
			}
		}
	}

}

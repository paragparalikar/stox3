package com.stox.charting.chart;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.DelegatingYAxis;
import com.stox.charting.axis.vertical.ValueAxis;

public class SecondaryChart extends Chart {

	public SecondaryChart(Configuration configuration, XAxis xAxis, final DelegatingYAxis volumeYAxis) {
		super(configuration, xAxis, volumeYAxis);
		setValueAxis(new ValueAxis(5));
		getMutableYAxis().setTop(0);
		getMutableYAxis().setBottom(0);
		bind();
	}

}

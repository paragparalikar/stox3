package com.stox.event;

public class Event {

	private static final EventHandlerRegistry eventHandlerRegistry = new EventHandlerRegistry();

	public static void register(final Object object) {
		eventHandlerRegistry.register(object);
	}
	
	public static void fire(final String topic) {
		fire(topic, null);
	}

	public static void fire(final String topic, final Object payload) {
		eventHandlerRegistry.invoke(topic, payload);
	}

	public static void unregister(final Object object){
		eventHandlerRegistry.unregister(object);
	}
	
}

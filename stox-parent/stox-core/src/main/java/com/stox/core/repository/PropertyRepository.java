package com.stox.core.repository;

import java.io.File;
import java.io.RandomAccessFile;
import java.sql.Date;
import java.util.Calendar;

import com.stox.util.Constant;
import com.stox.util.FileUtil;

import lombok.SneakyThrows;

public class PropertyRepository {

	private static final PropertyRepository INSTANCE = new PropertyRepository();
	
	public static PropertyRepository getInstance(){
		return INSTANCE;
	}
	
	private PropertyRepository() {
		
	}
	
	private String getPath(String key){
		return Constant.HOME + "properties" + File.separator + key; 
	}
	
	@SneakyThrows
	@SuppressWarnings("unchecked")
	public <T> T get(String key, Class<T> type, T defaultValue){
		try(final RandomAccessFile file = new RandomAccessFile(FileUtil.safeGet(getPath(key)).toFile(), "r")){
			if(type == Long.class){
				return Long.BYTES <= file.length() ? (T)(Object)file.readLong() : defaultValue;
			}else if(type == Date.class){
				return Long.BYTES <= file.length() ? (T)(Object)new Date(file.readLong()) : defaultValue;
			}else if(type == Calendar.class){
				if(Long.BYTES <= file.length()){
					final Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis((Long)(Object)file.readLong());
					return (T)(Object)calendar;
				}
				return defaultValue;
			}
			throw new IllegalArgumentException("Property of type \""+type.getCanonicalName()+"\" is not suppoerted");
		}
	}
	
	@SneakyThrows
	public void put(String key, Object value){
		try(final RandomAccessFile file = new RandomAccessFile(FileUtil.safeGet(getPath(key)).toFile(), "rw")){
			file.setLength(0);
			if(value instanceof Long){
				file.writeLong((Long)value);
			}else if(value instanceof Date){
				file.writeLong(((Date)value).getTime());
			}else if(Calendar.class.isAssignableFrom(value.getClass())){
				file.writeLong(((Calendar)value).getTimeInMillis());
			}else{
				throw new IllegalArgumentException("Property of type \""+value.getClass().getCanonicalName()+"\" is not suppoerted");
			}
		}
	}
	
}

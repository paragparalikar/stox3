package com.stox.example.repository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.stox.core.model.BarSpan;
import com.stox.core.model.Scrip;
import com.stox.core.repository.ScripRepository;
import com.stox.example.model.Example;
import com.stox.util.Constant;
import com.stox.util.FileUtil;

import lombok.SneakyThrows;

public class ExampleRepository {

	interface Wrapper{
		ExampleRepository INSTANCE = new ExampleRepository();
	}
	
	public static ExampleRepository getInstance(){
		return Wrapper.INSTANCE;
	}
	
	private final ScripRepository scripRepository = ScripRepository.getInstance();
	
	
	private ExampleRepository() {
	}
	
	private Path getPath(String watchlistId, BarSpan barSpan){
		return FileUtil.safeGet(Constant.HOME + "example" + File.separator + watchlistId + "-" + barSpan.getShortName());
	}
	
	@SneakyThrows
	public Example save(final String groupId, final Example example){
		final Path path = getPath(groupId, example.getBarSpan());
		final String text = format(example);
		if(!Files.readAllLines(path).contains(text)){
			Files.write(path, Collections.singleton(text), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
			return example;
		}
		return null;
	}
	
	@SneakyThrows
	public List<Example> find(final String groupId, final BarSpan barSpan){
		return Files.readAllLines(getPath(groupId, barSpan))
				.stream().map(line -> parse(groupId, line, barSpan))
				.filter(example -> null != example)
				.collect(Collectors.toList());
	}
	
	@SneakyThrows
	public Example delete(final String groupId, final Example example){
		final Path path = getPath(groupId, example.getBarSpan());
		final List<String> lines = Files.readAllLines(path);
		final String text = format(example);
		lines.remove(text);
		Files.write(path, lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		return example;
	}
	
	@SneakyThrows
	public void delete(String groupId){
		for(BarSpan barSpan : BarSpan.values()){
			Files.deleteIfExists(getPath(groupId, barSpan));
		}
	}
	
	private String format(final Example example){
		return example.getScrip().getIsin() + "," + example.getDate();
	}
	
	private Example parse(String groupId, String text, BarSpan barSpan){
		final String[] tokens = text.split(",");
		final Scrip scrip = scripRepository.find(tokens[0]);
		final long date = Long.parseLong(tokens[1]);
		return null == scrip ? null : new Example(groupId, scrip, barSpan, date);
	}
	
}

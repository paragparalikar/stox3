package com.stox.data.downloader.scrip;

import java.io.IOException;
import java.util.List;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;

public class NseIndicesMasterDownloader implements ScripMasterDownloader {

	private static final String URL_TEMPLATE = "https://www.nseindia.com/content/indices/ind_nifty%slist.csv";
	private static final String[] INDICES = {"50","next50","100","200","500","midcap150","midcap50","fullmidcap100",
			"freefloatmidcap100","smallcap250","smallcap50","fullsmallcap100","freefloatsmallcap_100","midsmallcap100",
			"auto","bank","finance","fmcg","it","media","pharma","_privatebank","psubank","realty","industry",
			"commodities","cpse","energy","consumption","infra","mnc","pse","service"};
	
	@Override
	public List<Scrip> download(Exchange exchange) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {
		for(String index : INDICES){
			System.out.println(String.format(URL_TEMPLATE, index));
		}
	}
	
}

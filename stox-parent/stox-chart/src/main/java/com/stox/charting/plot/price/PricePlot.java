package com.stox.charting.plot.price;

import com.stox.charting.Configuration;
import com.stox.charting.plot.Plot;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.Unit;
import com.stox.core.model.Bar;
import com.stox.core.model.Scrip;

public class PricePlot extends Plot<Bar> {
	
	private Scrip scrip;
	private final PricePlotDataDecorator dataDecorator = new PricePlotDataDecorator(this);

	public PricePlot(final Configuration configuration) {
		super(configuration);
	}
	
	public Scrip getScrip() {
		return scrip;
	}
	
	public void setScrip(Scrip scrip) {
		this.scrip = scrip;
	}

	@Override
	public Unit<Bar> buildUnit() {
		return null;
	}

	@Override
	public void showIndexInfo(int index) {

	}

	@Override
	public double getMax(Bar bar) {
		return bar.getHigh();
	}

	@Override
	public double getMin(Bar bar) {
		return bar.getLow();
	}

	@Override
	public Underlay getUnderlay() {
		return Underlay.PRICE;
	}

}

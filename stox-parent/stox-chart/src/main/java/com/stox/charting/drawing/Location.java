package com.stox.charting.drawing;

import lombok.Data;

@Data
public class Location {

	private long date;
	private double value;

	public String format() {
		return String.valueOf(date) + "," + String.valueOf(value);
	}

	public void parse(final String text) {
		final String[] tokens = text.split(",");
		date = Long.parseLong(tokens[0]);
		value = Double.parseDouble(tokens[1]);
	}

}

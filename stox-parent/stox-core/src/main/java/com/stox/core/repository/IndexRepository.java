package com.stox.core.repository;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.stox.core.model.Exchange;
import com.stox.core.model.Index;
import com.stox.core.util.ListMultiMap;

import lombok.SneakyThrows;

public class IndexRepository {

	private static final IndexRepository INSTANCE = new IndexRepository();

	public static IndexRepository getInstance(){
		return INSTANCE;
	}
	
	private final ListMultiMap<Exchange, Index> exchangeIndexCache = new ListMultiMap<>();
	
	private IndexRepository() {
		for(Exchange exchange : Exchange.values()){
			load(exchange);
		}
	}
	
	
	public static void main(String[] args) {
		
	}
	
	@SneakyThrows
	private void load(Exchange exchange){
		final String name = exchange.getCode().toLowerCase()+"_indices.csv";
		final URL url = IndexRepository.class.getClassLoader().getResource(name);
		exchangeIndexCache.clear();
		Files.readAllLines(Paths.get(url.toURI())).forEach(line -> {
		});
	}
	
	private Index parse(String line, Exchange exchange){
		final Index index = new Index();
		final String[] tokens = line.split(",");
		
		return index;
	}
	
}

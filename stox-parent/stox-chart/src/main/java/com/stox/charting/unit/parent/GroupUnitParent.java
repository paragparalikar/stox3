package com.stox.charting.unit.parent;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.widget.parent.GroupParentAdapter;

import javafx.beans.property.ObjectProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;

public class GroupUnitParent extends GroupParentAdapter implements UnitParent<Node> {

	public GroupUnitParent(Group group) {
		super(group);
	}

	@Override
	public void unbindColorProperty() {
		
	}

	@Override
	public void bindColorProperty(ObjectProperty<Color> colorProperty) {
		
	}

	@Override
	public void preLayoutChartChildren(final XAxis xAxis, final YAxis yAxis) {
		
	}

	@Override
	public void postLayoutChartChildren(final XAxis xAxis, final YAxis yAxis) {
		
	}

}

package com.stox.charting.indicator;


import java.util.ArrayList;
import java.util.List;

import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.indicator.addin.LevelAddIn;
import com.stox.charting.indicator.addin.OverboughtAddIn;
import com.stox.charting.indicator.addin.OversoldAddIn;
import com.stox.charting.indicator.unit.PointUnit;
import com.stox.charting.plot.Underlay;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.PolylineUnitParent;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.indicator.Indicator;
import com.stox.indicator.IndicatorFactory;
import com.stox.indicator.RateOfChange;
import com.stox.indicator.RateOfChange.Config;
import com.stox.widget.parent.Parent;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.shape.Polyline;
import lombok.experimental.Delegate;

public class ChartRateOfChange  extends AbstractChartIndicator<Config, Double, Point2D> {

	@Delegate
	private final Indicator<Config, Double> indicator = IndicatorFactory.get(RateOfChange.class);

	@Override
	public UnitParent<Point2D> buildParent(Group group) {
		final Polyline line = new Polyline();
		group.getChildren().add(line);
		return new PolylineUnitParent(line);
	}

	@Override
	public String getName() {
		return "Rate Of Change";
	}

	@Override
	public Unit<Double> buildUnit(Parent<Point2D> parent) {
		return new PointUnit(parent);
	}

	@Override
	public List<ChartAddIn<Double>> buildAddIns(final Config config, final UnitParent<Point2D> parent) {
		final PolylineUnitParent polylineUnitParent = (PolylineUnitParent) parent;
		final ArrayList<ChartAddIn<Double>> addIns = new ArrayList<>(3);
		addIns.add(new LevelAddIn<>(() -> 0d));
		addIns.add(new OverboughtAddIn<>(polylineUnitParent.getNode(), () -> 50d));
		addIns.add(new OversoldAddIn<>(polylineUnitParent.getNode(), () -> -50d));
		return addIns;
	}

	@Override
	public boolean isGroupable() {
		return true;
	}

	@Override
	public Underlay getUnderlay(Config config) {
		return Underlay.NONE;
	}

	@Override
	public double getMin(Double value) {
		return null == value ? Double.MAX_VALUE : value;
	}

	@Override
	public double getMax(Double value) {
		return null == value ? Double.MIN_VALUE : value;
	}

}

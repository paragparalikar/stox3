package com.stox.workbench.delegate;

import com.stox.util.StringUtil;
import com.stox.workbench.module.view.ModuleView;
import com.stox.workbench.module.view.ModuleViewFactory;

import javafx.scene.layout.Region;
import javafx.stage.Window;

public abstract class AbstractLifecycleDelegate {
	
	protected String format(String id, double x, double y, double width, double height) {
		return id + "-" + x + "," + y + "," + width + "," + height;
	}

	protected void parse(final String line, final Window window) {
		if (StringUtil.hasText(line)) {
			final String[] tokens = line.split("-")[1].split(",");
			StringUtil.ifHasText(tokens, 0, x -> window.setX(Double.parseDouble(x)));
			StringUtil.ifHasText(tokens, 1, y -> window.setY(Double.parseDouble(y)));
			StringUtil.ifHasText(tokens, 2, width -> window.setWidth(Double.parseDouble(width)));
			StringUtil.ifHasText(tokens, 3, height -> window.setHeight(Double.parseDouble(height)));
		}
	}

	protected ModuleView parse(String line, ModuleViewFactory moduleViewFactory) {
		final String[] tokens = line.split("-");
		final String id = tokens[0];
		final ModuleView moduleView = moduleViewFactory.create(id);
		final String[] subTokens = tokens[1].split(",");
		final Region node = moduleView.getNode();
		node.setLayoutX(Double.parseDouble(subTokens[0]));
		node.setLayoutY(Double.parseDouble(subTokens[1]));
		node.setPrefWidth(Double.parseDouble(subTokens[2]));
		node.setPrefHeight(Double.parseDouble(subTokens[3]));
		return moduleView;
	}
	
}

package com.stox.charting.indicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.stox.charting.Configuration;
import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;
import com.stox.charting.event.ConfigChangedEvent;
import com.stox.charting.event.UnderlayChangedEvent;
import com.stox.charting.indicator.addin.ChartAddIn;
import com.stox.charting.plot.DerivativePlot;
import com.stox.charting.plot.Underlay;
import com.stox.charting.plot.info.EditablePlotInfoPane;
import com.stox.charting.plot.info.PlotInfoPane;
import com.stox.charting.plot.info.ValuePlotInfoPane;
import com.stox.charting.unit.Unit;
import com.stox.charting.unit.parent.UnitParent;
import com.stox.core.model.Bar;
import com.stox.util.Constant;

public class IndicatorPlot<T, V, P> extends DerivativePlot<V> {

	private final T configuration;
	private final UnitParent<P> parent;
	private ValuePlotInfoPane valuePlotInfoPane;
	private EditablePlotInfoPane editablePlotInfoPane;
	private final ChartIndicator<T, V, P> indicator;
	private final List<ChartAddIn<V>> addIns = new ArrayList<>();

	public IndicatorPlot(final ChartIndicator<T, V, P> indicator, final Configuration configuration) {
		super(configuration);
		this.indicator = indicator;
		this.parent = indicator.buildParent(this);
		this.configuration = indicator.buildDefaultConfig();
		build();
	}

	private void build() {
		addIns.addAll(indicator.buildAddIns(configuration, parent));
		addIns.forEach(addIn -> getChildren().add(addIn.getNode()));
		addIns.forEach(ChartAddIn::update);
		parent.bindColorProperty(colorProperty());
		valuePlotInfoPane.setName(indicator.getName());
		valuePlotInfoPane.bind(colorProperty());
		editablePlotInfoPane.addEditEventHandler(event -> edit());
	}

	private void edit() {
		final Underlay oldUnderlay = getUnderlay();
		final IndicatorEditorModal modal = new IndicatorEditorModal(configuration, indicator);
		modal.setOnAction(e -> {
			fireEvent(oldUnderlay.equals(getUnderlay()) ? new ConfigChangedEvent(this) : new UnderlayChangedEvent(this));
		});
		modal.show();
	}

	@Override
	public Underlay getUnderlay() {
		return indicator.getUnderlay(configuration);
	}

	@Override
	public void layoutChartChildren(XAxis xAxis, YAxis yAxis) {
		parent.preLayoutChartChildren(xAxis, yAxis);
		addIns.forEach(addIn -> addIn.preLayoutChartChildren(xAxis, yAxis));
		indicator.layoutChartChildren(xAxis, yAxis, getModels(), getUnits(), parent, this);
		parent.postLayoutChartChildren(xAxis, yAxis);
		addIns.forEach(addIn -> addIn.postLayoutChartChildren(xAxis, yAxis));
	}

	@Override
	protected void layoutUnit(Unit<V> unit, int index, V model, V previousModel, XAxis xAxis, YAxis yAxis) {
		super.layoutUnit(unit, index, model, previousModel, xAxis, yAxis);
		addIns.forEach(addIn -> addIn.layoutUnit(model, unit, xAxis, yAxis));
	}

	void doLayoutChartChildren(XAxis xAxis, YAxis yAxis) {
		super.layoutChartChildren(xAxis, yAxis);
	}

	@Override
	public void load(List<Bar> bars) {
		final List<V> models = getModels();
		synchronized (models) {
			models.clear();
			final List<V> values = indicator.computeAll(Collections.emptyList(), bars, configuration);
			models.addAll(values);
		}
	}

	@Override
	public Unit<V> buildUnit() {
		return indicator.buildUnit(parent);
	}

	@Override
	protected PlotInfoPane buildPlotInfoPane() {
		if (null == valuePlotInfoPane) {
			final PlotInfoPane plotInfoPane = super.buildPlotInfoPane();
			editablePlotInfoPane = new EditablePlotInfoPane(plotInfoPane);
			valuePlotInfoPane = new ValuePlotInfoPane(editablePlotInfoPane);
		}
		return valuePlotInfoPane;
	}

	@Override
	public void showIndexInfo(int index) {
		final V value = 0 <= index && index < getModels().size() ? getModels().get(index) : null;
		valuePlotInfoPane.setValue(null == value ? ""
				: (value instanceof Number ? Constant.currencyFormat.format(value) : value.toString()));
	}

	@Override
	public double getMin() {
		return Underlay.NONE.equals(getUnderlay()) ? super.getMin() : Double.MAX_VALUE;
	}

	@Override
	public double getMax() {
		return Underlay.NONE.equals(getUnderlay()) ? super.getMax() : Double.MIN_VALUE;
	}

	@Override
	public double getMin(V model) {
		return indicator.getMin(model);
	}

	@Override
	public double getMax(V model) {
		return indicator.getMax(model);
	}

}

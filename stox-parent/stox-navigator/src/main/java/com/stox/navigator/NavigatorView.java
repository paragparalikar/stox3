package com.stox.navigator;

import java.util.List;
import java.util.Objects;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;
import com.stox.core.model.event.ScripsChangedEvent;
import com.stox.core.repository.ScripRepository;
import com.stox.core.ui.LinkButton;
import com.stox.core.ui.model.Link;
import com.stox.core.ui.model.Link.State;
import com.stox.event.Event;
import com.stox.event.EventHandler;
import com.stox.ui.fx.fluent.scene.control.FluentComboBox;
import com.stox.ui.fx.fluent.scene.control.FluentToggleButton;
import com.stox.widget.Icon;
import com.stox.widget.Ui;
import com.stox.widget.search.SearchBox;
import com.stox.widget.search.SearchableListView;
import com.stox.workbench.module.view.ModuleView;

import javafx.geometry.Side;

public class NavigatorView extends ModuleView{
	public static final String ID = "navigator";
	
	private final SearchableListView<Scrip> listView = new SearchableListView<>();
	private final FluentToggleButton filterButton = new FluentToggleButton(Icon.FILTER).classes("primary","icon").onAction(e -> toggleFilter()).tooltip(Ui.tooltip("Filter"));
	private final FluentComboBox<Exchange> exchangeComboBox = new FluentComboBox<Exchange>().items(Exchange.values()).select(Exchange.NSE).classes("primary","inverted").fullWidth();
	private final FluentToggleButton searchButton = new FluentToggleButton(Icon.SEARCH).classes("primary","icon").onAction(e -> toggleSearch());
	private final SearchBox<Scrip> searchBox = new SearchBox<Scrip>(listView, this::test);
	private final LinkButton linkButton = new LinkButton();
	
	public NavigatorView() {
		content(listView);
		searchBox.classes("primary").fullWidth();
		getTitleBar().add(Side.RIGHT, filterButton);
		getTitleBar().add(Side.RIGHT, searchButton);
		getTitleBar().add(Side.RIGHT, linkButton);
		exchangeComboBox.selectionModel().selectedItemProperty().addListener((o, old, exchange) -> refresh());
		listView.getSelectionModel().selectedItemProperty().addListener((o, old, scrip) -> updateLinkState(scrip));
	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	public boolean test(final Scrip scrip, String text) {
		text = text.trim().toLowerCase();
		return null != scrip && (
				scrip.getName().trim().toLowerCase().contains(text) || 
				scrip.getCode().trim().toLowerCase().contains(text) ||
				scrip.getIsin().trim().toLowerCase().contains(text));
	}
	
	private void toggleFilter(){
		getTitleBar().add(filterButton.isSelected(), Side.BOTTOM, exchangeComboBox);
	}
	
	private void toggleSearch(){
		getTitleBar().add(searchButton.isSelected(), Side.BOTTOM, searchBox);
		if(searchButton.isSelected()){
			searchBox.getTextField().requestFocus();
		}
	}
	
	private void updateLinkState(Scrip scrip){
		final Link link = linkButton.getLink();
		link.setState(new State(0, scrip, null));
	}
	
	@Override
	public void start() {
		super.start();
		refresh();
		Event.register(this);
	}
	
	@Override
	public void stop() {
		Event.unregister(this);
		super.stop();
	}
	
	private void refresh(){
		final Exchange exchange = exchangeComboBox.getValue();
		final ScripRepository scripRepository = ScripRepository.getInstance();
		final List<Scrip> scrips = scripRepository.find(exchange);
		listView.getItems().setAll(scrips);
	}
	
	@EventHandler(ScripsChangedEvent.TYPE)
	public void onScripsChanged(ScripsChangedEvent event){
		if(Objects.equals(event.getExchange(), exchangeComboBox.getValue())){
			Ui.fx(() -> listView.getItems().setAll(event.getScrips()));
		}
	}
	
	
}

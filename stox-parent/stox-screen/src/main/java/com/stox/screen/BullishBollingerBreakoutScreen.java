package com.stox.screen;

import java.util.List;

import com.stox.core.model.Bar;
import com.stox.core.model.BarValue;
import com.stox.screen.BullishBollingerBreakoutScreen.Config;

import lombok.Getter;
import lombok.Setter;

public class BullishBollingerBreakoutScreen implements Screen<Config>{

	@Getter
	@Setter
	public static class Config{
		private int span = 14;
		private double multiple = 2;
		private double maximumStandardDeviation = 2;
		private BarValue bandBarValue = BarValue.MID;
		private BarValue breakoutBarValue = BarValue.CLOSE;
	}
	
	@Override
	public String getCode() {
		return "screen-bullish-bollinger-breakout";
	}

	@Override
	public String getName() {
		return "Bollinger Breakout";
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.BULLISH;
	}

	@Override
	public Config buildDefaultConfig() {
		return new Config();
	}

	@Override
	public int getMinBarCount(Config config) {
		return config.getSpan() + 1;
	}

	@Override
	public boolean isMatch(List<Bar> bars, Config config) {
		if(null == bars || bars.size() < config.getSpan()){
			return false;
		}
		
		double sum = 0;
		for (int i = 1; i < 1 + config.getSpan(); i++) {
			sum += config.getBandBarValue().get(bars.get(i));
		}
		double deviationSum = 0;
		final double average = sum / config.getSpan();
		for (int i = 1; i < 1 + config.getSpan(); i++) {
			double deviation = average - config.getBandBarValue().get(bars.get(i));
			deviationSum += Math.pow(deviation, 2);
		}
		final double stdDevValue = Math.sqrt(deviationSum / config.getSpan());
		System.out.println("stdDevValue:"+stdDevValue+", average:"+average);
		return stdDevValue*100/average <= config.getMaximumStandardDeviation() && stdDevValue*config.getMultiple()+average <= config.getBreakoutBarValue().get(bars.get(0));
	}
	
}

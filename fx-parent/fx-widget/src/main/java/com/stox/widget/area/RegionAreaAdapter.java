package com.stox.widget.area;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.layout.Region;

public class RegionAreaAdapter implements Area {

	private final Region region;

	public RegionAreaAdapter(final Region region) {
		this.region = region;
	}

	@Override
	public Node getNode() {
		return region;
	}

	@Override
	public double getX() {
		return region.getLayoutX();
	}

	@Override
	public double getY() {
		return region.getLayoutY();
	}

	@Override
	public double getHeight() {
		return region.getHeight();
	}

	@Override
	public double getWidth() {
		return region.getWidth();
	}

	@Override
	public Cursor getCursor() {
		return region.getCursor();
	}

	@Override
	public void setCursor(final Cursor cursor) {
		region.setCursor(cursor);
	}

	@Override
	public Rectangle2D getMaxBounds() {
		final Bounds bounds = region.getParent().getLayoutBounds();
		return new Rectangle2D(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight());
	}

	@Override
	public void setBounds(final double x, final double y, final double width, final double height) {
		region.setLayoutX(x);
		region.setLayoutY(y);
		region.setMinWidth(width);
		region.setPrefWidth(width);
		region.setMaxWidth(width);
		region.setMinHeight(height);
		region.setPrefHeight(height);
		region.setMaxHeight(height);
		region.resizeRelocate(x, y, width, height);
		region.autosize();
	}

	@Override
	public <T extends Event> void addEventHandler(final EventType<T> eventType,
			final EventHandler<? super T> eventHandler) {
		region.addEventHandler(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void addEventFilter(final EventType<T> eventType,
			final EventHandler<? super T> eventHandler) {
		region.addEventFilter(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void removeEventHandler(EventType<T> eventType, EventHandler<T> eventHandler) {
		region.removeEventHandler(eventType, eventHandler);
	}

	@Override
	public <T extends Event> void removeEventFilter(EventType<T> eventType, EventHandler<T> eventHandler) {
		region.removeEventFilter(eventType, eventHandler);
	}

}

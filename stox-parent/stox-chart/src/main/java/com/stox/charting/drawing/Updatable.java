package com.stox.charting.drawing;

import com.stox.charting.axis.horizontal.XAxis;
import com.stox.charting.axis.vertical.YAxis;

public interface Updatable {

	void update(final XAxis xAxis, final YAxis yAxis);
	
}

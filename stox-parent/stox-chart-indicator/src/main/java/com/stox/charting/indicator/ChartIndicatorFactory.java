package com.stox.charting.indicator;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class ChartIndicatorFactory {

	private static final List<ChartIndicator<?, ?, ?>> ALL = new ArrayList<>();
	static {
		ServiceLoader.load(ChartIndicator.class).forEach(indicator -> ALL.add(indicator));
	}

	public List<ChartIndicator<?, ?, ?>> find() {
		return ALL;
	}

}

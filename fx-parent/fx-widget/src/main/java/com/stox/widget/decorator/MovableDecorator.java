package com.stox.widget.decorator;

import com.stox.widget.area.Area;
import com.stox.widget.area.RegionAreaAdapter;
import com.stox.widget.area.StageAreaAdapter;

import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class MovableDecorator {

	private double offsetX;
	private double offsetY;
	private final Area area;
	private boolean relocationMode;
	private final Node knob;
	private final EventHandler<MouseEvent> eventHandler = event -> relocate(event);

	public MovableDecorator(Node knob, Stage stage){
		this(knob, new StageAreaAdapter(stage));
	}
	
	public MovableDecorator(Node knob, Region target) {
		this(knob, new RegionAreaAdapter(target));
	}
	
	public MovableDecorator(Node knob, Area area) {
		this.area = area;
		this.knob = knob;
		attach();
	}

	public void attach() {
		knob.addEventHandler(MouseEvent.MOUSE_PRESSED, eventHandler);
		knob.addEventHandler(MouseEvent.MOUSE_DRAGGED, eventHandler);
		knob.addEventHandler(MouseEvent.MOUSE_RELEASED, eventHandler);
	}

	public void detach() {
		knob.removeEventHandler(MouseEvent.MOUSE_PRESSED, eventHandler);
		knob.removeEventHandler(MouseEvent.MOUSE_DRAGGED, eventHandler);
		knob.removeEventHandler(MouseEvent.MOUSE_RELEASED, eventHandler);
	}

	private void relocate(final MouseEvent event) {
		final double x = event.getScreenX();
		final double y = event.getScreenY();
		final EventType<? extends MouseEvent> type = event.getEventType();
		if (MouseEvent.MOUSE_PRESSED.equals(type)) {
			offsetX = x;
			offsetY = y;
			relocationMode = true;
		} else if (relocationMode && MouseEvent.MOUSE_DRAGGED.equals(type)) {
			area.setBounds(area.getX() + x - offsetX, area.getY() + y - offsetY, area.getWidth(), area.getHeight());
			offsetX = x;
			offsetY = y;
		} else if (MouseEvent.MOUSE_RELEASED.equals(type)) {
			offsetX = 0;
			offsetY = 0;
			relocationMode = false;
		}
	}

}

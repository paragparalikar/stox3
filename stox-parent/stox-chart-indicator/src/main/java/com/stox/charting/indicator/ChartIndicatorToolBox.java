package com.stox.charting.indicator;

import com.stox.charting.ChartingView;
import com.stox.charting.tools.ChartingToolBox;
import com.stox.ui.fx.fluent.scene.control.FluentButton;
import com.stox.widget.Icon;
import com.stox.widget.Ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;

public class ChartIndicatorToolBox extends FluentButton implements ChartingToolBox, EventHandler<ActionEvent> {

	private ChartingView chartingView;
	
	public ChartIndicatorToolBox() {
		super(Icon.LINE_CHART);
		classes("primary","icon","small").tooltip(Ui.tooltip("Indicators")).onAction(this);
	}
	
	@Override
	public Node getNode() {
		return this;
	}

	@Override
	public void attach(ChartingView chartingView) {
		this.chartingView = chartingView;
	}

	@Override
	public void handle(ActionEvent event) {
		if(null != chartingView){
			final IndicatorChooserModal modal = new IndicatorChooserModal(chartingView);
			modal.show();
		}
	}

}

package com.stox.core.ui.filter.scrip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;
import java.util.function.Supplier;

import com.stox.core.model.Exchange;
import com.stox.core.model.Scrip;
import com.stox.core.repository.ScripRepository;
import com.stox.ui.fx.fluent.scene.layout.FluentVBox;

import javafx.scene.Node;

public class ScripFilter extends FluentVBox implements Supplier<List<Scrip>>{

	public ScripFilter() {
		classes("spaced", "padded");
		ServiceLoader.load(ScripFilterVBox.class).forEach(getChildren()::add);
	}
	
	@Override
	public List<Scrip> get() {
		final ScripRepository scripRepository = ScripRepository.getInstance();
		final List<Scrip> scrips = new ArrayList<>();
		for (Exchange exchange : Exchange.values()) {
			scrips.addAll(scripRepository.find(exchange));
		}
		for(Node node : children()){
			if(node instanceof ScripFilterVBox){
				final ScripFilterVBox<?> filter = (ScripFilterVBox<?>)node;
				filter.filter(scrips);
			}
		}
		Collections.sort(scrips);
		return scrips;
	}
}
